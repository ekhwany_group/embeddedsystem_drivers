

/*********************************************************/
/* Author : Islam Gamal Abdel Nasser 					 */
/* Date   : 28th Mar 2019								 */
/* Driver : Timer Driver 							     */
/* Layer  : MCAL 										 */
/*********************************************************/

/* Include LIBs */
#include "STD_TYPES.h"
#include "BIT_CALC.h"
/* Include The Driver headers */
#include "Timer.h"


/***************************************************/
/* 				Timer Private Section	 		   */
/***************************************************/
/* Timer Counter Register */
#define TCNT0	(u8*)0x52
#define TCNT1H	(u8*)0x4D
#define TCNT1L	(u8*)0x4C
#define TCNT2	(u8*)0x44
/* Timer/Counter Control Register */
#define TCCR0	(u8*)0x53
#define TCCR1B	(u8*)0x4E
#define TCCR1A	(u8*)0x4F
#define TCCR2	(u8*)0x45
/* Output Compare Register */
#define OCR0	(u8*)0x5C
#define OCR1AH	(u8*)0x4B
#define OCR1AL	(u8*)0x4A
#define OCR1BH	(u8*)0x49
#define OCR1BL	(u8*)0x48
#define OCR2	(u8*)0x43
/* Timer Interrupt Mask Register */
#define TIMSK	(u8*)0x59
/* Timer Interrupt Flag Register */
#define TIFR	(u8*)0x58


static u16 Locale_u8COunter_OVF  = 0;


/********************************************************************/
/* 					Timer APIs			                   			*/
/********************************************************************/

/* API : TIMER0_u8Enable							   */
/* Description: Enable Timer 0 with no prescaler mode  */
/*******************************************************/
/* Inputs  : No Inputs					 			   */
/* Outputs : Error State Level						   */
/*******************************************************/
void TIMER0_VidEnable(void){

	/* Initialize Timer at No Prescaler Clock Frequency */
	*(TCCR0) &= ~(TIMER_MASK_PRS);
	*(TCCR0) |= TIMER_NO_PRSCLR;
	return;
}

/* API : TIMER1_VidEnable							   */
/* Description: Enable Timer1 with no prescaler mode  */
/*******************************************************/
/* Inputs  : No Inputs					 			   */
/* Outputs : Error State Level						   */
/*******************************************************/
void TIMER1_VidEnable(void){

	/* Initialize Timer1 at Normal Mode */
	*(TCCR1A) &= ~(TIMER_MASK_TIMER1_MODE);
	*(TCCR1B) &= ~(TIMER_MASK_TIMER1_MODE<<3);

	*(TCCR1A) |= ((TIMER_NORMAL>>0)&1);
	*(TCCR1A) |= (((TIMER_NORMAL>>1)&1) << 1);
	*(TCCR1B) |= (((TIMER_NORMAL>>2)&1) << 3);
	*(TCCR1B) |= (((TIMER_NORMAL>>3)&1) << 4);

	return;
}
/* API : TIMER0_VidDisable							   */
/* Description: Used To Set  Timer Peripheral Off      */
/*******************************************************/
/* Inputs  : No Inputs					 			   */
/* Outputs : Error State Level						   */
/*******************************************************/
void TIMER0_VidDisable(void){

	*(TCCR0) = TIMER_DISABLED;
	return;
}


/* API : TIMER0_u8SetMode						 	   */
/* Description: Used To set Mode For Timer0            */
/*******************************************************/
/* Inputs  : Copy_u8SetMode				 			   */
/* Outputs : Error State Level						   */
/*******************************************************/
u8 TIMER0_u8SetMode(u8 Copy_u8SetMode){
	/* Define Local Variables */
	u8 Locale_u8ErrorState = ERROR_OK;

	if ( 	(TIMER_NORMAL == Copy_u8SetMode) ||
			(TIMER_PWM    == Copy_u8SetMode) ||
			(TIMER_CTC    == Copy_u8SetMode) ||
			(TIMER_FPWM   == Copy_u8SetMode)
			){
		*(TCCR0) &=~(1<<6);
		*(TCCR0) &=~(1<<3);
		*(TCCR0) |= ( GET_BIT(Copy_u8SetMode,0)	<< 6);
		*(TCCR0) |= ( GET_BIT(Copy_u8SetMode,1)	<< 3);
	}

	else {
		Locale_u8ErrorState = ERROR_NOK;
	}

	return Locale_u8ErrorState;
}


/* API : TIMER0_u8SetMode						 	   */
/* Description: Used To set Mode For Timer0            */
/*******************************************************/
/* Inputs  : Copy_u8SetMode				 			   */
/* Outputs : Error State Level						   */
/*******************************************************/
u8 TIMER0_u8SetSenseLevel(u8 Copy_u8SenseLevel){
	/* Define Local Variables */
	u8 Locale_u8ErrorState = ERROR_OK;

	if ( 	    (TIMER_NO_OC     == Copy_u8SenseLevel) ||
				(TIMER_TOGGLE_OC == Copy_u8SenseLevel) ||
				(TIMER_CLR_OC    == Copy_u8SenseLevel) ||
				(TIMER_SET_OC    == Copy_u8SenseLevel)
				){
		*(TCCR0) &=	~(0b11<<4);
		*(TCCR0) |=	 (Copy_u8SenseLevel<<4);

	}
	else {
			Locale_u8ErrorState = ERROR_NOK;
		}

		return Locale_u8ErrorState;
	}


/* API : TIMER0_u8SetPreScaler						   */
/* Description: Used To set a prescaler for the Timer  */
/*******************************************************/
/* Inputs  : Copy_u8Prescaler			 			   */
/* Outputs : Error State Level						   */
/*******************************************************/
u8 TIMER0_u8SetPreScaler(u8 Copy_u8Prescaler){

	/* Define Local Variables */
	u8 Locale_u8ErrorState = ERROR_OK;

	if ( 	(TIMER_8PRSCLR    == Copy_u8Prescaler) ||
			(TIMER_64PRSCLR   == Copy_u8Prescaler) ||
			(TIMER_256PRSCLR  == Copy_u8Prescaler) ||
			(TIMER_1024PRSCLR == Copy_u8Prescaler)
			){
		*(TCCR0) |= (Copy_u8Prescaler);
	}
	else {
		Locale_u8ErrorState = ERROR_NOK;
	}
	return Locale_u8ErrorState;
}

/* API : TIMER0_u8SetDutyCycle					 	   */
/* Description: Used To set specific Duty Cycle.       */
/*******************************************************/
/* Inputs  : Copy_u8SetMode				 			   */
/* Outputs : Error State Level						   */
/*******************************************************/
u8 TIMER0_u8SetDutyCycle(u8 Copy_u8DutyCycle){
	/* Define Local Variables */
	u8 Locale_u8ErrorState = ERROR_OK;

	if ( TIMER0_MAX_DUTY_CYCLE >= Copy_u8DutyCycle){
		*(OCR0) = (254 * Copy_u8DutyCycle)/100;
	}
	else {
		Locale_u8ErrorState = ERROR_NOK;
	}
	return Locale_u8ErrorState;
}


/* API : TIMER0_u8SetDesiredTime_Ms					   */
/* Description: Used To Configure Timer to meet        */
/*				A desired Time in Ms as a delay		   */
/*******************************************************/
/* Inputs  : Copy_u8Prescaler			 			   */
/* Outputs : Error State Level						   */
/*******************************************************/

 void TIMER0_u8SetDesiredTime_Ms(u8 Copy_u8DesiredTime_Ms){

	/* Define Local Variables */
	u16 Locale_u8Time_OVF      = 0;
	u8  Locale_u8Preload       = 0;
	u32 Locale_TimerResolution = 1;
	u8  Locale_u8Power         = 0;
	/* Calculate The Timer Resolution :
	 * 		Resolution = 2^TIMER_RESOLUTION
	 */
	for (Locale_u8Power=1; Locale_u8Power<=TIMER_RESOLUTION; Locale_u8Power++){
		Locale_TimerResolution *=2;
	}

	Locale_u8COunter_OVF = (Copy_u8DesiredTime_Ms*1000) / ((Locale_TimerResolution * TIMER_PRESCALER)/TIMER_FREQUENCY);
	/* Calculate Preload Using :
	 * Fraction Count = Fraction * Timer Resolution
	 * Preload 		  = Timer Resolution - Fraction Count
	 */
	Locale_u8Preload	= ((Copy_u8DesiredTime_Ms)% Locale_u8Time_OVF) * Locale_TimerResolution;
	Locale_u8Preload    = Locale_TimerResolution - Locale_u8Preload;

	*(TCNT0) = Locale_u8Preload;

	return;
}


/* API : TIMER0_u8SetDesiredTime_Us					   */
/* Description: Used To Configure Timer to meet        */
/*				A desired Time in Ms as a delay		   */
/*******************************************************/
/* Inputs  : Copy_u8Prescaler			 			   */
/* Outputs : Error State Level						   */
/*******************************************************/
void TIMER0_u8SetDesiredTime_Us(u8 Copy_u8DesiredTime_Us){
	/* Define Local Variables */
	u16 Locale_u8Time_OVF      = 0;
	u8  Locale_u8Preload       = 0;
	u32 Locale_TimerResolution = 1;
	u8  Locale_u8Power         = 0;
	/* Calculate The Timer Resolution :
	 * 		Resolution = 2^TIMER_RESOLUTION
	 */
	for (Locale_u8Power=1; Locale_u8Power<=TIMER_RESOLUTION; Locale_u8Power++){
		Locale_TimerResolution *=2;
	}

	Locale_u8COunter_OVF = (Copy_u8DesiredTime_Us) / ((Locale_TimerResolution * TIMER_PRESCALER)/TIMER_FREQUENCY);
	/* Calculate Preload Using :
	 * Fraction Count = Fraction * Timer Resolution
	 * Preload 		  = Timer Resolution - Fraction Count
	 */
	Locale_u8Preload	= ((Copy_u8DesiredTime_Us)% Locale_u8Time_OVF) * Locale_TimerResolution;
	Locale_u8Preload    = Locale_TimerResolution - Locale_u8Preload;

	*(TCNT0) = Locale_u8Preload;
	return;
}
