/*
 * Timer.h
 *
 *  Created on: Mar 31, 2019
 *      Author: EsSss
 */

#ifndef TIMER_H_
#define TIMER_H_

/*********************************************************/
/* Author : Islam Gamal Abdel Nasser 					 */
/* Date   : 28th Mar 2019								 */
/* Driver : Timer Driver 							     */
/* Layer  : MCAL 										 */
/*********************************************************/





/****************************************************/
/* 				Timer PreCONFIGURATIO	  	 		*/
/****************************************************/
/* Configure Timer Resolution: */
#define TIMER_RESOLUTION	(u8)8
/* Configure System Clock Frequency: */
#define TIMER_FREQUENCY		(u8)16
/* Timer Prescaler */
#define TIMER_PRESCALER		(u8)8

/*****************************************************/




/***************************************************/
/* 				Timer INTERFACES			 	   */
/***************************************************/
/***** Timer0 Modes Of Operation: */
#define TIMER_NORMAL	(u8)0b00
#define TIMER_PWM		(u8)0b01
#define TIMER_CTC		(u8)0b10
#define TIMER_FPWM		(u8)0b11

/***** Timer1 Modes Of Operation: */
#define TIMER_NORMAL	(u8)0b0000
#define TIMER_PWM		(u8)0b1011
#define TIMER_CTC		(u8)0b0100
#define TIMER_FPWM		(u8)0b1111

/* Maximum Duty Cycle */
#define TIMER0_MAX_DUTY_CYCLE	(u8)100

/* MASK Timer Values */
#define TIMER_MASK_PRS		   (u8)0b111
#define TIMER_MASK_SENSE_LEVEL (u8)0b11
#define TIMER_MASK_TIMER1_MODE (u8)0b11
/* Compare Output Modes(OC):   */
#define TIMER_NO_OC		(u8)0b00
#define TIMER_TOGGLE_OC	(u8)0b01
#define TIMER_CLR_OC	(u8)0b10
#define TIMER_SET_OC	(u8)0b11

/* Prescaler States For Timer: */
#define TIMER_DISABLED		(u8)0b000
#define TIMER_NO_PRSCLR		(u8)0b001
#define TIMER_8PRSCLR		(u8)0b010
#define TIMER_64PRSCLR		(u8)0b100
#define TIMER_256PRSCLR		(u8)0b100
#define TIMER_1024PRSCLR	(u8)0b101

/* Timer Interrupt States: 	*/
#define TIMER_OC_INT_ENABLE		(u8)1
#define TIMER_OC_INT_DISABLE	(u8)0
#define TIMER_OV_INT_ENABLE		(u8)1
#define TIMER_OV_INT_DISABLE	(u8)0
/****************************************************/



/********************************************************************/
/* 					Timer APIs			                   			*/
/********************************************************************/

/* API : TIMER0_u8Enable							   */
/* Description: Enable Timer 0 with no prescaler mode  */
/*******************************************************/
/* Inputs  : No Inputs					 			   */
/* Outputs : Error State Level						   */
/*******************************************************/
void TIMER0_VidEnable(void);

/* API : TIMER1_VidEnable							   */
/* Description: Enable Timer1 with no prescaler mode  */
/*******************************************************/
/* Inputs  : No Inputs					 			   */
/* Outputs : Error State Level						   */
/*******************************************************/
void TIMER1_VidEnable(void);
/* API : TIMER0_VidDisable							   */
/* Description: Used To Set  Timer Peripheral Off      */
/*******************************************************/
/* Inputs  : No Inputs					 			   */
/* Outputs : Error State Level						   */
/*******************************************************/
void TIMER0_VidDisable(void);

/* API : TIMER0_u8SetMode						 	   */
/* Description: Used To set Mode For Timer0            */
/*******************************************************/
/* Inputs  : Copy_u8SetMode				 			   */
/* Outputs : Error State Level						   */
/*******************************************************/
u8 TIMER0_u8SetMode(u8 Copy_u8SetMode);

/* API : TIMER0_u8SetMode						 	   */
/* Description: Used To set Mode For Timer0            */
/*******************************************************/
/* Inputs  : Copy_u8SetMode				 			   */
/* Outputs : Error State Level						   */
/*******************************************************/
u8 TIMER0_u8SetSenseLevel(u8 Copy_u8SenseLevel);


/* API : TIMER0_u8SetDutyCycle					 	   */
/* Description: Used To set specific Duty Cycle.       */
/*******************************************************/
/* Inputs  : Copy_u8SetMode				 			   */
/* Outputs : Error State Level						   */
/*******************************************************/
u8 TIMER0_u8SetDutyCycle(u8 Copy_u8DutyCycle);

/* API : TIMER0_u8SetPreScaler						   */
/* Description: Used To set a prescaler for the Timer  */
/*******************************************************/
/* Inputs  : Copy_u8Prescaler			 			   */
/* Outputs : Error State Level						   */
/*******************************************************/
u8 TIMER0_u8SetPreScaler(u8 Copy_u8Prescaler);

/* API : TIMER0_u8SetDesiredTime_Ms					   */
/* Description: Used To Configure Timer to meet        */
/*				A desired Time in Ms as a delay		   */
/*******************************************************/
/* Inputs  : Copy_u8Prescaler			 			   */
/* Outputs : Error State Level						   */
/*******************************************************/
void TIMER0_u8SetDesiredTime_Ms(u8 Copy_u8DesiredTime_Ms);

/* API : TIMER0_u8SetDesiredTime_Us					   */
/* Description: Used To Configure Timer to meet        */
/*				A desired Time in Ms as a delay		   */
/*******************************************************/
/* Inputs  : Copy_u8Prescaler			 			   */
/* Outputs : Error State Level						   */
/*******************************************************/
void TIMER0_u8SetDesiredTime_Us(u8 Copy_u8DesiredTime_Us);


#endif /* TIMER_H_ */
