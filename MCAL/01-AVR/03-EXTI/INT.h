/*
 * INT.h
 *
 *  Created on: Mar 20, 2019
 *      Author: EsSss
 */

#ifndef INT_H_
#define INT_H_


/*************** PRIVATE Configuration ****************/
/* 		MCU Control Register 						  */
#define INT_MCUCR 	((Register*) 0x55)->ByteAccess
/* 		MCU Control & Reset Register 			      */
#define INT_MCUCSR 	((Register*) 0x54)->ByteAccess
/* 		General Control Interrupt Register			  */
#define INT_GICR 	((Register*) 0x5B)->ByteAccess
/* 		General Interrupt Flag		  		          */
#define INT_GIFR 	((Register*) 0x5A)->ByteAccess
/******************************************************/


/*************** Public Configuration ****************/
/* Default Interrupt Level 							 */
#define INT_Default_Level	INT_RISING_EDGE

/******************************************************/


/*************** Public Interfaces    ****************/
/* Interrupts Levels 								 */
#define INT_LOW_LEVEL		(u8)0b00
#define INT_LOGICAL_CHANGE	(u8)0b01
#define INT_FALLING_EDGE	(u8)0b10
#define INT_RISING_EDGE		(u8)0b11

/* Available Interrupts Indexes in the MCU			*/
#define INT0	(u8)6
#define INT1	(u8)7
#define INT2	(u8)5

/* Interrupt Modes 									*/
#define INT_ENABLE		(u8)1
#define INT_DISABLE		(u8)0
/******************************************************/




/****************** 	Public APIs    *******************/


/* API: INT_VidInit				    				*/
/* Description: This Function Implemented to setup  */
/* 				the INT Registers					*/
/****************************************************/
/* Inputs:  No Inputs 								*/
/* Outputs: No Outputs								*/
/****************************************************/
void INT_VidInit(void);

/* API: INT_u8EnableINT				    			*/
/* Description: Enable Any INT by Setting its BIT	*/
/* 				in the External GICR				*/
/****************************************************/
/* Inputs:  No Inputs 								*/
/* Outputs: Error State								*/
/****************************************************/
u8 INT_u8EnableINT(u8 INT_u8NB);

/* API: INT_u8DisableINT				    		*/
/* Description: Disable Any INT by Clearing its BIT	*/
/* 				in the External GICR				*/
/****************************************************/
/* Inputs: INT_u8NB 								*/
/* Outputs: Error State								*/
/****************************************************/
u8 INT_u8DisableINT(u8 INT_u8NB);

/* API: INT_VidInit				    				*/
/* Description: Setup The Sense level of Interrupts */
/****************************************************/
/* Inputs:  INT_u8NB 								*/
/* Outputs: Error State								*/
/****************************************************/
u8 INT_u8SenseLevel(u8 INT_u8NB, u8 INT_u8Level);

/* API: INT_VidInit				    				*/
/* Description: This Function Implemented to setup  */
/* 				the INT Registers					*/
/****************************************************/
/* Inputs: u8 INT_u8NB, u8 INT_u8Level				*/
/* Outputs: Error State								*/
/****************************************************/
void INT_u8SetCallBack(void (*Copy_u8FuncAddr)(void), u8 INT_u8Index);


#endif /* INT_H_ */
