/*
 * INT.c
 *
 *  Created on: Mar 20, 2019
 *      Author: EsSss
 */
/* LIBs Include */
#include "STD_TYPES.h"
#include "BIT_CALC.h"
#include "util/delay.h"
/* MCAL Includes */
#include "DIO_interface.h"
#include "INT.h"

static void (*Arr_VidPtrFuncCallBack[3])(void)={NULL,NULL,NULL};


/* API: INT_VidInit				    				*/
/* Description: This Function Implemented to setup  */
/* 				the INT Registers					*/
/****************************************************/
/* Inputs:  No Inputs 								*/
/* Outputs: No Outputs								*/
/****************************************************/
void INT_VidInit(void)
{

	/* Configure INT0 on RISING Edge */
	SET_BIT(INT_MCUCR,BIT0);
	SET_BIT(INT_MCUCR,BIT1);
	/* Enable General Interrupt Control of INT0 */
	SET_BIT(INT_GICR,BIT6);

	return;
}

/* API: INT_u8EnableINT				    			*/
/* Description: Enable Any INT by Setting its BIT	*/
/* 				in the External GICR				*/
/****************************************************/
/* Inputs:  No Inputs 								*/
/* Outputs: Error State								*/
/****************************************************/
u8 INT_u8EnableINT(u8 INT_u8NB){
	/* Local Variable Definition */
	u8 Locale_ErrorState = ERROR_OK;

	if (	(GET_BIT(INT_GICR,INT_u8NB) == INT_DISABLE) &&
			((INT_u8NB = INT0) || (INT_u8NB = INT1) || (INT_u8NB = INT2)) ){
		/* Enable The Corresponding INT in the GICR */
		SET_BIT(INT_GICR,INT_u8NB);
	}
	else {
		Locale_ErrorState = ERROR_NOK;
	}
	/* Return Error Level State */
	return Locale_ErrorState;
}


/* API: INT_u8DisableINT				    		*/
/* Description: Disable Any INT by Clearing its BIT	*/
/* 				in the External GICR				*/
/****************************************************/
/* Inputs: INT_u8NB 								*/
/* Outputs: Error State								*/
/******************************* *********************/
u8 INT_u8DisableINT(u8 INT_u8NB){
	/* Local Variable Definition */
	u8 Locale_ErrorState = ERROR_OK;

	if (GET_BIT(INT_GICR,INT_u8NB) == INT_ENABLE &&
		((INT_u8NB == INT0) || (INT_u8NB == INT1)|| (INT_u8NB == INT2)) ){
		/* Enable The Corresponding INT in the GICR */
		CLR_BIT(INT_GICR,INT_u8NB);
	}
	else {
		Locale_ErrorState = ERROR_NOK;
	}
	/* Return Error Level State */
	return Locale_ErrorState;
}


/* API: INT_u8SenseLevel		    				*/
/* Description: Setup The Sense level of Interrupts */
/****************************************************/
/* Inputs:  INT_u8NB 								*/
/* Outputs: Error State								*/
/****************************************************/
u8 INT_u8SenseLevel(u8 INT_u8NB, u8 INT_u8Level){
	/* Local Variable Definition */
	u8 Locale_ErrorState = ERROR_OK;

	if  ( ((INT_u8Level == INT_LOW_LEVEL) || (INT_u8Level == INT_LOGICAL_CHANGE)||
			(INT_u8Level == INT_FALLING_EDGE) || (INT_u8Level == INT_RISING_EDGE)) &&
		((INT_u8NB == INT0) || (INT_u8NB == INT1)|| (INT_u8NB == INT2)) ){
		/* Configure The INT with specific Sense Level */
		switch (INT_u8NB){
			case INT0: 	ASSIGN_BIT(INT_MCUCR,0,((Register)INT_u8Level).BitAccess.Bit0);
						ASSIGN_BIT(INT_MCUCR,1,((Register)INT_u8Level).BitAccess.Bit1);
						break;
			case INT1:	ASSIGN_BIT(INT_MCUCR,2,((Register)INT_u8Level).BitAccess.Bit0);
						ASSIGN_BIT(INT_MCUCR,3,((Register)INT_u8Level).BitAccess.Bit1);
						break;
			case INT2:	if (INT_u8Level == INT_FALLING_EDGE){
							CLR_BIT(INT_MCUCSR,BIT6);
						}
						else if (INT_u8Level == INT_RISING_EDGE){
							SET_BIT(INT_MCUCSR,BIT6);
						}
						else {
							Locale_ErrorState = ERROR_NOK;
						}
						break;
		}
	}
	else {
		Locale_ErrorState = ERROR_NOK;
	}
	/* Return Error Level State */
	return Locale_ErrorState;
}

/* API: INT_u8SetCallBack				    		*/
/* Description: This Function Implemented to setup  */
/* 				the INT Registers					*/
/****************************************************/
/* Inputs: u8 INT_u8NB, u8 INT_u8Level				*/
/* Outputs: Error State								*/
/****************************************************/
void INT_u8SetCallBack(void (*Copy_u8FuncAddr)(void), u8 INT_u8Index){

	switch(INT_u8Index){
	case INT0 : Arr_VidPtrFuncCallBack[0]=Copy_u8FuncAddr;break;
	case INT1 : Arr_VidPtrFuncCallBack[1]=Copy_u8FuncAddr;break;
	case INT2 : Arr_VidPtrFuncCallBack[2]=Copy_u8FuncAddr;break;
	}
	return;
}

void __vector_1 (void) __attribute__ ((signal)) ;

void __vector_1 (void) {

	if ( NULL != Arr_VidPtrFuncCallBack[0]){

		Arr_VidPtrFuncCallBack[0]();
	}
	else {
		/* Do Nothing ^_^ */
	}

	return;
}

void __vector_2 (void)	__attribute__((signal));

void __vector_2 (void){

	if ( NULL != Arr_VidPtrFuncCallBack[1]){

		Arr_VidPtrFuncCallBack[1]();
	}
	else {
		/* Do Nothing ^_^ */
	}

	return;
}

void __vector_3 (void)	__attribute__((signal));

void __vector_3 (void){

	if ( NULL != Arr_VidPtrFuncCallBack[2]){

		Arr_VidPtrFuncCallBack[2]();
	}
	else {
		/* Do Nothing ^_^ */
	}
	return;
}
