
#ifndef USART_H_
#define USART_H_


/*********************************************************/
/* Author : Islam Gamal Abdel Nasser 					 */
/* Date   : 1st May 2019								 */
/* Driver : USART Driver 							     */
/* Layer  : MCAL 										 */
/*********************************************************/




/****************************************************/
/* 				USART PreCONFIGURATIO	  	 		*/
/****************************************************/

/* USART MODE : Asynchronous */
#define USART_MODE			(u8)0

/* Parity Status: Np Parity */
#define USART_PARITY_STATUS	(u8)0b00

/* Data Size : 8 Bit		*/
#define USART_DATA_SIZE		(u8)0b011

/* Stop Bit : 1 bit 		*/
#define USART_STOP_BITS		(u8)0

/****************************************************/




/***************************************************/
/* 				USART INTERFACES			 	   */
/***************************************************/
/* UCSRA Regiser Bits */
#define USART_UCSRA_RXC		(u8)7
#define USART_UCSRA_TXC   	(u8)6
#define USART_UCSRA_UDRE  	(u8)5
#define USART_UCSRA_FE   	(u8)4
#define USART_UCSRA_DOR  	(u8)3
#define USART_UCSRA_PE   	(u8)2
#define USART_UCSRA_U2X   	(u8)1
#define USART_UCSRA_MPCM  	(u8)0
/* UCSRB Register Bits */
#define USART_UCSRB_RXCIE    (u8)7
#define USART_UCSRB_TXCIE    (u8)6
#define USART_UCSRB_UDRIE    (u8)5
#define USART_UCSRB_RXEN     (u8)4
#define USART_UCSRB_TXEN     (u8)3
#define USART_UCSRB_UCSZ2    (u8)2
#define USART_UCSRB_RXB8     (u8)1
#define USART_UCSRB_TXB8     (u8)0
/* UCSRA Register Bits */
#define USART_UCSRC_URSEL    (u8)7
#define USART_UCSRC_UMSEL    (u8)6
#define USART_UCSRC_UPM1     (u8)5
#define USART_UCSRC_UPM0     (u8)4
#define USART_UCSRC_USBS     (u8)3
#define USART_UCSRC_UCSZ1    (u8)2
#define USART_UCSRC_UCSZ0    (u8)1
#define USART_UCSRC_UCPOL    (u8)0


#define USART_UBRRL_REGISTER_VALUE		(u8)255
#define USART_UBRRH_BITS				(u8)8
#define USART_MASK_BIT					(u8)1
/****************************************************/


/********************************************************************/
/* 					USART APIs			                   			*/
/********************************************************************/

/* API : USART_VidInit								   */
/* Description: Initialize The USART with: 			   */
/*				- Setting Baud Rate					   */
/*				- Setting Frame Formate				   */
/* 						 No Parity					   */
/* 						 8 Bit Data					   */
/*						 1 bit Stop					   */
/*				- Enable Transmitter and Reciever 	   */
/*******************************************************/
/* Inputs  : BAUD Rate 					 			   */
/* Outputs : NO Outputs								   */
/*******************************************************/
void USART_VidInit(u32 u32BaudRate);

/* API : USART_u8TransmitData						   */
/* Description: Transmitting Data through USART		   */
/*******************************************************/
/* Inputs  : Data To transmit			 			   */
/* Outputs : No Return Value						   */
/*******************************************************/
void USART_VidTransmitByte(u8 u8Data);

/* API : USART_u8ReceiveData						   */
/* Description: Recieving Data through USART		   */
/*******************************************************/
/* Inputs  : Addrebss To store Data in 	 			   */
/* Outputs : No return Value					       */
/*******************************************************/
void USART_VidReceiveByte(u8* u8Data);

u8   USART_u8TransmitBuffer(u8* Buffer, u8* Size);
u8   USART_u8ReceiveBuffer(u8* Buffer, u8* Size);

#endif /* USART_H_ */
