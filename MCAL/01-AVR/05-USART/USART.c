


/*********************************************************/
/* Author : Islam Gamal Abdel Nasser 					 */
/* Date   : 1st May 2019								 */
/* Driver : USART Driver 							     */
/* Layer  : MCAL 										 */
/*********************************************************/

/* LIBs Include */
#include "STD_TYPES.h"
#include "BIT_CALC.h"

/* Header File Include */
#include "USART.h"


// Define CPU clock Frequency , here its 16MHz
#define F_CPU 12000000UL




/***************************************************/
/* 				Timer Private Section	 		   */
/***************************************************/

/* USART Data Register */
#define UDR		(volatile u8*)0x2C

/* USART Control and status Register A,B,C  */
#define UCSRA	(volatile u8*)0x2B
#define UCSRB	(volatile u8*)0x2A
#define UCSRC	(volatile u8*)0x40

#define UBRRH	(volatile u8*)0x40
#define UBRRL	(volatile u8*)0x29

/* Private Macros */
#define	ENABLE_UCSRC_REGISTER		*(UCSRC)|=  (1 << USART_UCSRC_URSEL)
#define ENABLE_UBRRL_REGISTER		*(UCSRC)&= ~(1 << USART_UCSRC_URSEL)

#define CLK_FREQ					(u32)12000000
#define NORMAL_MODE_BAUD_VALUE 		(( CLK_FREQ / (16*u16BaudRate)) -1)
/****************************************************/


/********************************************************************/
/* 					USART APIs			                   			*/
/********************************************************************/

/* API : USART_VidInit								   */
/* Description: Initialize The USART with: 			   */
/*				- Setting USART Mode 				   */
/*				- Setting Baud Rate					   */
/*				- Setting Frame Formate				   */
/* 						 No Parity					   */
/* 						 8 Bit Data					   */
/*						 1 bit Stop					   */
/*				- Enable Transmitter and Reciever 	   */
/*******************************************************/
/* Inputs  : BAUD Rate 					 			   */
/* Outputs : NO Outputs								   */
/*******************************************************/
void USART_VidInit(u32 u32BaudRate){

	u16 UBRRL_Value = 0;
	/* Setting BaudRate */
	/*	Normal Mode      UBRRL =( (Fclk)/(16*BAUD) ) -1  */
	/*	Duble Speed Mode UBRRL =( (Fclk)/(8*BAUD) ) -1  */
	/*	Synchronous Mode UBRRL =( (Fclk)/(2*BAUD) ) -1  */
	UBRRL_Value = (( CLK_FREQ / (16 * u32BaudRate)) -1);

	*(UBRRH) = UBRRL_Value >> 8;
	*(UBRRL) = UBRRL_Value;

	*(UCSRB) = (1<<USART_UCSRB_TXEN) | (1<<USART_UCSRB_RXEN);
	*(UCSRC) = (1<<USART_UCSRC_URSEL) | (1<<USART_UCSRC_USBS) | (3<<USART_UCSRC_UCSZ0);





//	/* Enable TX & RX */
//		*(UCSRB) |= BIT_HIGH << USART_UCSRB_TXEN;
//		*(UCSRB) |= BIT_HIGH << USART_UCSRB_RXEN;

	/* Enable UCSRC Register */
//	ENABLE_UCSRC_REGISTER;
//	/* Selecting USART MODE */
//	*(UCSRC) &= ~(USART_MASK_BIT << USART_UCSRC_UMSEL);
//	*(UCSRC) |=  (USART_MODE     << USART_UCSRC_UMSEL);

//	/* Setting Frame Formate */
//		/* Parity Status */
//			/* Clear Bits First */
//			*(UCSRC) &= ~(USART_MASK_BIT << USART_UCSRC_UPM0);
//			*(UCSRC) &= ~(USART_MASK_BIT << USART_UCSRC_UPM1);
//			/* Then Assign The Desired Values */
//			*(UCSRC) |= ( (	(USART_PARITY_STATUS >> BIT0) & BIT_HIGH) << USART_UCSRC_UPM0);
//			*(UCSRC) |= ( (	(USART_PARITY_STATUS >> BIT1) & BIT_HIGH) << USART_UCSRC_UPM1);

//	   /* Number Of Stop Bits */
//			/* Clear Bits First */
//			*(UCSRC) &= ~(USART_MASK_BIT << USART_UCSRC_USBS);
//			/* Then Assign The Desired Values */
//			*(UCSRC) |=  (USART_STOP_BITS << USART_UCSRC_USBS);
//
//		/* Determine Data Size */
//			/* Clear Bits First */
//			*(UCSRC) &= ~(USART_MASK_BIT << USART_UCSRC_UCSZ0);
//			*(UCSRC) &= ~(USART_MASK_BIT << USART_UCSRC_UCSZ1);
//			*(UCSRB) &= ~(USART_MASK_BIT << USART_UCSRB_UCSZ2);
//			/* Then Assign The Desired Values */
//			*(UCSRC) |= ( USART_DATA_SIZE << USART_UCSRC_UCSZ1);
//			*(UCSRB) |= ( (	(USART_DATA_SIZE >> BIT2) & BIT_HIGH) << USART_UCSRB_UCSZ2);


}

/* API : USART_u8TransmitData						   */
/* Description: Transmitting Data through USART		   */
/*******************************************************/
/* Inputs  : Data To transmit			 			   */
/* Outputs : Error Level							   */
/*******************************************************/
void USART_VidTransmitByte(u8 u8Data){

	/* Wait Until The Empty flag is high */
	while(	!( *(UCSRA) & (BIT_HIGH << USART_UCSRA_UDRE) )	);

	/* Sending Data */
	*(UDR) = u8Data;

	/* Wait Until finshing the Transmission */
	while(	!(*(UCSRA) & (BIT_HIGH << USART_UCSRA_TXC) )	);

	/* Clear TX Flag */
	*(UCSRA) |= (BIT_HIGH << USART_UCSRA_TXC);
}

/* API : USART_u8ReceiveData						   */
/* Description: Recieving Data through USART		   */
/*******************************************************/
/* Inputs  : Address To store Data in 	 			   */
/* Outputs : Error Level							   */
/*******************************************************/
void USART_VidReceiveByte(u8* u8Data){

	/* Wait Until The Recieve Flag Rised */
	while(	!( *(UCSRA) & (BIT_HIGH << USART_UCSRA_RXC) ) );

	/* Store The data in the UDR to the Data Location */
	*(u8Data) = *(UDR);

}

/* Struct Of Trasmittion Data */
typedef struct{
	u8 *Data;
	u8 Size;
	u8 Index;
	u8 IsSending;
	void (*Tx_Notify)(void);
}TX_Buffer;
/* Creation of TX Struct Object */
static TX_Buffer Transmission_Buffer;



/* Struct of Receiving Data */
typedef struct{
	u8 *Data;
	u8 Size;
	u8 Index;
	u8 IsReceiving;
	void (*Rx_Notify)(void);
}RX_Buffer;
/* Definition of Received Data Struct */
RX_Buffer Receiving_Buffer;


u8   USART_u8TransmitBuffer(u8* Buffer, u8* Size){
	u8 Locale_ErrorState = ERROR_OK;
	if( (Buffer != NULL) && (Size !=0) ){
		if( Transmission_Buffer.IsSending == 0){
			Transmission_Buffer.IsSending = 1;
			*(Transmission_Buffer.Data) = *(Buffer);
			Transmission_Buffer.Size = Size;
			Transmission_Buffer.Index = 0;
			*(UDR) = Transmission_Buffer.Data[Transmission_Buffer.Index];
			Transmission_Buffer.Index++;
		}
		else{
			Locale_ErrorState = ERROR_NOK;
		}
	}
	else {
		Locale_ErrorState = ERROR_NOK;
	}

	return Locale_ErrorState;
}

u8   USART_u8ReceiveBuffer(u8* Buffer, u8* Size){
	u8 Locale_ErrorState = ERROR_OK;
	if( (Buffer != NULL) && (Size !=0) ){



	}
	else {
		Locale_ErrorState = ERROR_NOK;
	}

	return Locale_ErrorState;




}






