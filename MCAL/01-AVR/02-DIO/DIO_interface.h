/*
 * DIO_interface.h
 *
 *  Created on: Jan 29, 2019
 *      Author: EsSss
 */




/********************************************************/
/* Author: ISLAM GMAL ABDEL NASSER						*/
/* DATE: 31 JAN 2019									*/
/* VERSION : V1.0										*/
/********************************************************/
/* DESCRIPTION: 										*/
/* 	This file is used to Define the Interfaces 			*/
/* 	Of The software Component							*/
/********************************************************/


#ifndef DIO_INTERFACE_H_
#define DIO_INTERFACE_H_


/***********************************************************
 DESCRIPTION: DIO PINS DEFINATION						***/
#define DIO_PIN0   (u8)0 
#define DIO_PIN1   (u8)1 
#define DIO_PIN2   (u8)2 
#define DIO_PIN3   (u8)3 
#define DIO_PIN4   (u8)4 
#define DIO_PIN5   (u8)5 
#define DIO_PIN6   (u8)6 
#define DIO_PIN7   (u8)7 
#define DIO_PIN8   (u8)8 
#define DIO_PIN9   (u8)9 
#define DIO_PIN10  (u8)10
#define DIO_PIN11  (u8)11
#define DIO_PIN12  (u8)12
#define DIO_PIN13  (u8)13
#define DIO_PIN14  (u8)14
#define DIO_PIN15  (u8)15
#define DIO_PIN16  (u8)16
#define DIO_PIN17  (u8)17
#define DIO_PIN18  (u8)18
#define DIO_PIN19  (u8)19
#define DIO_PIN20  (u8)20
#define DIO_PIN21  (u8)21
#define DIO_PIN22  (u8)22
#define DIO_PIN23  (u8)23
#define DIO_PIN24  (u8)24
#define DIO_PIN25  (u8)25
#define DIO_PIN26  (u8)26
#define DIO_PIN27  (u8)27
#define DIO_PIN28  (u8)28
#define DIO_PIN29  (u8)29
#define DIO_PIN30  (u8)30
#define DIO_PIN31  (u8)31


/**********************************************************/
/* DESCRIPTION: DIO PORTS DEFINATION	   			   	  */
#define DIO_PORTA 	(u8)0
#define DIO_PORTB 	(u8)1
#define DIO_PORTC 	(u8)2
#define DIO_PORTD 	(u8)3


/***********************************************************
 DESCRIPTION: The COMMON USED ASSIGNMENT OF PORTS 	****/
#define DIO_U8_PORT_HIGH 	(u8)0Xff
#define DIO_U8_PORT_LOW		(u8)0X00
#define DIO_U8_PORT_OUTPUT	(u8)0xff
#define DIO_U8_PORT_INPUT	(u8)0x00



/***********************************************************
 DESCRIPTION: The COMMON USED ASSIGNMENT OF PINS 	****/
#define DIO_U8_PIN_OUTPUT   (u8)1
#define DIO_U8_PIN_INPUT    (u8)0
#define DIO_U8_PIN_HIGH 	(u8)1
#define DIO_U8_PIN_LOW  	(u8)0
#define DIO_INPUT_PIN  		(u8)0
#define DIO_OUTPUT_PIN 		(u8)1
#define DIO_INPUT_PULLUP    (u8)1
#define DIO_INPUT_PULLDOWN  (u8)0

/***********************************************************
* DESCRIPTION: Initialize The DIO Driver				****/
#define DIO_MAXNB       (u8)32
#define DIO_u8MaxPortNB (u8)4
/***********************************************************
* DESCRIPTION: Initialize The DIO Driver				****
* Input1 :	NA											****
* Outputs:	NA											****
***********************************************************/
void DIO_VidInit(void);
/***********************************************************
* DESCRIPTION: Set Pin Direction						****
* Input1 :	u8PinNB     -> The Number Of The Pin		****
* Input2 :	u8Direction -> The Direction Of The Pin		****
* Outputs:												****
***********************************************************/
u8 DIO_u8SetPinDirection(u8 Copy_u8PinNb, u8 Copy_u8Direction);

/***********************************************************
* DESCRIPTION: Set Pin Value.							****
* Input1 :	u8PinNB     -> The Number Of The Pin		****
* Input2 :	Copy_u8Value -> The Value Of The Pin		****
* Outputs:												****
***********************************************************/
u8 DIO_u8SetPinValue(u8 Copy_u8PinNB, u8 Copy_u8Value);

/***********************************************************
* DESCRIPTION: Get Pin Diretion							****
* Input1 :	Copy_u8PinNB     -> The Number Of The Pin	****
* Input2 :	Copy_u8PinValue -> The Address of DIO Pin	****
* Outputs:												****
***********************************************************/
u8 DIO_u8GetPinValue(u8 Copy_u8PinNB, u8* Copy_u8PinValue);

/***********************************************************
* DESCRIPTION: Set PORT Diretion						****
* Input1 :	Copy_u8PortNB     -> The Number Of The PORT	****
* Input2 :	Copy_u8PortDirection1 -> The Direction Of The PORT****
* Outputs:												****
***********************************************************/
u8 DIO_u8SetPortDirection(u8 Copy_u8PortNB, u8 Copy_u8PortDirection1);

/***********************************************************
 DESCRIPTION: Set Pin Direction							****
* Input1 :	Copy_u8PortNB     -> The Number Of The PORT	****
* Input2 :	Copy_PortValue -> The Value Of The PORT		****
* Outputs:												****
***********************************************************/
u8 DIO_u8SetPortValue(u8 Copy_u8PortNB, u8 Copy_PortValue);

/***********************************************************
* DESCRIPTION: Set Pin Diretion							****
* Input1 :	Copy_u8PortNB     -> The Number Of The PORT	****
* Input2 :	Copy_PortValue -> The Value Of The PORT		****
* Outputs:												****
***********************************************************/
u8 DIO_u8GetPortValue(u8 Copy_u8PortNB, u8* Copy_PortValue);



#endif /* DIO_INTERFACE_H_ */
