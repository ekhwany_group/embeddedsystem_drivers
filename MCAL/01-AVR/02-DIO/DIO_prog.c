/*
 * DIO_prog.c
 *
 *  Created on: Jan 29, 2019
 *      Author: EsSss
 */




/********************************************************/
/* Author: ISLAM GMAL ABDEL NASSER						*/
/* DATE: 31 JAN 2019									*/
/* VERSION : V1.0										*/
/********************************************************/
/* DESCRIPTION: 										*/
/*This file is used to Contain the implementation 		*/
/* of the SWC APIs 										*/
/********************************************************/


#include "STD_TYPES.h"
#include "BIT_CALC.h"
#include "DIO_private.h"
#include "DIO_interface.h"
#include "DIO_config.h"


/* DESCRIPTION: Set Pin Direction
 * Input1 :	u8PinNB     -> The Number Of The Pin
 * Input2 :	u8Direction -> The Direction Of The Pin
 * Outputs:
 */


/* Description : Initialization of the DIO Component */
void DIO_VidInit(void)
{

  /* Configuration of DDRA Register from Config.h */
		DIO_DDRA_Register = CONC_8BIT(DIO_PIN0_INIT_DIRECTION,
                                  DIO_PIN1_INIT_DIRECTION,
                                  DIO_PIN2_INIT_DIRECTION,
                                  DIO_PIN3_INIT_DIRECTION,
                                  DIO_PIN4_INIT_DIRECTION,
                                  DIO_PIN5_INIT_DIRECTION,
                                  DIO_PIN6_INIT_DIRECTION,
                                  DIO_PIN7_INIT_DIRECTION
                                  );
  /* Configuration of DDRB Register from Config.h */
		DIO_DDRB_Register = CONC_8BIT(DIO_PIN8_INIT_DIRECTION,
                                  DIO_PIN9_INIT_DIRECTION,
                                  DIO_PIN10_INIT_DIRECTION,
                                  DIO_PIN11_INIT_DIRECTION,
                                  DIO_PIN12_INIT_DIRECTION,
                                  DIO_PIN13_INIT_DIRECTION,
                                  DIO_PIN14_INIT_DIRECTION,
                                  DIO_PIN15_INIT_DIRECTION
                                  );
  /* Configuration of DDRC Register from Config.h */
		DIO_DDRC_Register = CONC_8BIT(DIO_PIN16_INIT_DIRECTION,
                                  DIO_PIN17_INIT_DIRECTION,
                                  DIO_PIN18_INIT_DIRECTION,
                                  DIO_PIN19_INIT_DIRECTION,
                                  DIO_PIN20_INIT_DIRECTION,
                                  DIO_PIN21_INIT_DIRECTION,
                                  DIO_PIN22_INIT_DIRECTION,
                                  DIO_PIN23_INIT_DIRECTION
                                  );
  /* Configuration of DDRD Register from Config.h */
		DIO_DDRD_Register = CONC_8BIT(DIO_PIN24_INIT_DIRECTION,
                                  DIO_PIN25_INIT_DIRECTION,
                                  DIO_PIN26_INIT_DIRECTION,
                                  DIO_PIN27_INIT_DIRECTION,
                                  DIO_PIN28_INIT_DIRECTION,
                                  DIO_PIN29_INIT_DIRECTION,
                                  DIO_PIN30_INIT_DIRECTION,
                                  DIO_PIN31_INIT_DIRECTION
                                  );
                                  
  /**********************************************************/                               
  /* Configuration of PORTA Register from Config.h */            
    DIO_PORTA_Register = CONC_8BIT(DIO_PIN0_INIT_VALUE,
                                   DIO_PIN1_INIT_VALUE,
                                   DIO_PIN2_INIT_VALUE,
                                   DIO_PIN3_INIT_VALUE,
                                   DIO_PIN4_INIT_VALUE,
                                   DIO_PIN5_INIT_VALUE,
                                   DIO_PIN6_INIT_VALUE,
                                   DIO_PIN7_INIT_VALUE
                                  );
  /* Configuration of PORTB Register from Config.h */                        
   DIO_PORTB_Register = CONC_8BIT(DIO_PIN8_INIT_VALUE,
                                  DIO_PIN9_INIT_VALUE,
                                  DIO_PIN10_INIT_VALUE,
                                  DIO_PIN11_INIT_VALUE,
                                  DIO_PIN12_INIT_VALUE,
                                  DIO_PIN13_INIT_VALUE,
                                  DIO_PIN14_INIT_VALUE,
                                  DIO_PIN15_INIT_VALUE
                                  );             
  /* Configuration of PORTC Register from Config.VALUEh */                
    DIO_PORTC_Register = CONC_8BIT(DIO_PIN16_INIT_VALUE,
                                   DIO_PIN17_INIT_VALUE,
                                   DIO_PIN18_INIT_VALUE,
                                   DIO_PIN19_INIT_VALUE,
                                   DIO_PIN20_INIT_VALUE,
                                   DIO_PIN21_INIT_VALUE,
                                   DIO_PIN22_INIT_VALUE,
                                   DIO_PIN23_INIT_VALUE
                                  );
  /* Configuration of PORTD Register from Config.h */                
    DIO_PORTD_Register = CONC_8BIT(DIO_PIN24_INIT_VALUE,
                                   DIO_PIN25_INIT_VALUE,
                                   DIO_PIN26_INIT_VALUE,
                                   DIO_PIN27_INIT_VALUE,
                                   DIO_PIN28_INIT_VALUE,
                                   DIO_PIN29_INIT_VALUE,
                                   DIO_PIN30_INIT_VALUE,
                                   DIO_PIN31_INIT_VALUE
                                  );  
        #if (DIO_PIN0_INIT_DIRECTION == DIO_PIN0_INIT_DIRECTION_INPUT) && (DIO_PIN0_INIT_VALUE == DIO_PIN_INIT_VALUE_HIGH)
          #warning "PIN 0 is configured as input and defualt value is high, Corrected to LOW"
          #undef  DIO_PIN0_INIT_VALUE
          #define DIO_PIN0_INIT_VALUE   DIO_PIN_INIT_PIN_DEF
        #endif

		#if (DIO_PIN1_INIT_DIRECTION == DIO_PIN1_INIT_DIRECTION_INPUT) && (DIO_PIN1_INIT_VALUE == DIO_PIN_INIT_VALUE_HIGH)
			 #warning "PIN 0 is configured as input and defualt value is high, Corrected to LOW"
			 #undef  DIO_PIN0_INIT_VALUE
			#define DIO_PIN0_INIT_VALUE   DIO_PIN_INIT_PIN_DEF
       #endif

       #if (DIO_PIN2_INIT_DIRECTION == DIO_PIN2_INIT_DIRECTION_INPUT) && (DIO_PIN2_INIT_VALUE == DIO_PIN_INIT_VALUE_HIGH)
         #warning "PIN 2 is configured as input and defualt value is high, Corrected to LOW"
          #undef  DIO_PIN2_INIT_VALUE
          #define DIO_PIN2_INIT_VALUE   DIO_PIN_INIT_PIN_DEF
        #endif

       #if (DIO_PIN3_INIT_DIRECTION == DIO_PIN3_INIT_DIRECTION_INPUT) && (DIO_PIN3_INIT_VALUE == DIO_PIN_INIT_VALUE_HIGH)
          #warning "PIN 3 is configured as input and defualt value is high, Corrected to LOW"
          #undef  DIO_PIN3_INIT_VALUE
          #define DIO_PIN3_INIT_VALUE   DIO_PIN_INIT_PIN_DEF
        #endif

     #if (DIO_PIN4_INIT_DIRECTION == DIO_PIN4_INIT_DIRECTION_INPUT) && (DIO_PIN4_INIT_VALUE == DIO_PIN_INIT_VALUE_HIGH)
			#warning "PIN 4 is configured as input and defualt value is high, Corrected to LOW"
          #undef  DIO_PIN4_INIT_VALUE
          #define DIO_PIN4_INIT_VALUE   DIO_PIN_INIT_PIN_DEF
        #endif
  
     #if (DIO_PIN5_INIT_DIRECTION == DIO_PIN5_INIT_DIRECTION_INPUT) && (DIO_PIN5_INIT_VALUE == DIO_PIN_INIT_VALUE_HIGH)
			#warning "PIN 5 is configured as input and defualt value is high, Corrected to LOW"
          #undef  DIO_PIN5_INIT_VALUE
          #define DIO_PIN5_INIT_VALUE   DIO_PIN_INIT_PIN_DEF
        #endif

    #if (DIO_PIN6_INIT_DIRECTION == DIO_PIN6_INIT_DIRECTION_INPUT) && (DIO_PIN6_INIT_VALUE == DIO_PIN_INIT_VALUE_HIGH)
			#warning "PIN 6 is configured as input and defualt value is high, Corrected to LOW"
          #undef  DIO_PIN6_INIT_VALUE
          #define DIO_PIN6_INIT_VALUE   DIO_PIN_INIT_PIN_DEF
        #endif

     #if (DIO_PIN7_INIT_DIRECTION == DIO_PIN7_INIT_DIRECTION_INPUT) && (DIO_PIN7_INIT_VALUE == DIO_PIN_INIT_VALUE_HIGH)
			#warning "PIN 7 is configured as input and defualt value is high, Corrected to LOW"
          #undef  DIO_PIN7_INIT_VALUE
          #define DIO_PIN7_INIT_VALUE   DIO_PIN_INIT_PIN_DEF
        #endif

    #if (DIO_PIN8_INIT_DIRECTION == DIO_PIN8_INIT_DIRECTION_INPUT) && (DIO_PIN8_INIT_VALUE == DIO_PIN_INIT_VALUE_HIGH)
			#warning "PIN 8 is configured as input and defualt value is high, Corrected to LOW"
          #undef  DIO_PIN8_INIT_VALUE
          #define DIO_PIN8_INIT_VALUE   DIO_PIN_INIT_PIN_DEF
        #endif

    #if (DIO_PIN9_INIT_DIRECTION == DIO_PIN0_INIT_DIRECTION_INPUT) && (DIO_PIN9_INIT_VALUE == DIO_PIN_INIT_VALUE_HIGH)
		#warning "PIN 9 is configured as input and defualt value is high, Corrected to LOW"
          #undef  DIO_PIN9_INIT_VALUE
          #define DIO_PIN9_INIT_VALUE   DIO_PIN_INIT_PIN_DEF
        #endif

     #if (DIO_PIN10_INIT_DIRECTION == DIO_PIN10_INIT_DIRECTION_INPUT) && (DIO_PIN10_INIT_VALUE == DIO_PIN_INIT_VALUE_HIGH)
		#warning "PIN 10 is configured as input and defualt value is high, Corrected to LOW"
          #undef  DIO_PIN10_INIT_VALUE
          #define DIO_PIN10_INIT_VALUE   DIO_PIN_INIT_PIN_DEF
        #endif

        #if (DIO_PIN11_INIT_DIRECTION == DIO_PIN0_INIT_DIRECTION_INPUT) && (DIO_PIN11_INIT_VALUE == DIO_PIN_INIT_VALUE_HIGH)
            #warning "PIN 3 is configured as input and defualt value is high, Corrected to LOW"
                      #undef  DIO_PIN11_INIT_VALUE
                      #define DIO_PIN11_INIT_VALUE   DIO_PIN_INIT_PIN_DEF
                    #endif
            
        #if (DIO_PIN12_INIT_DIRECTION == DIO_PIN12_INIT_DIRECTION_INPUT) && (DIO_PIN12_INIT_VALUE == DIO_PIN_INIT_VALUE_HIGH)
            #warning "PIN 3 is configured as input and defualt value is high, Corrected to LOW"
                      #undef  DIO_PIN12_INIT_VALUE
                      #define DIO_PIN12_INIT_VALUE   DIO_PIN_INIT_PIN_DEF
                    #endif
        
        #if (DIO_PIN13_INIT_DIRECTION == DIO_PIN13_INIT_DIRECTION_INPUT) && (DIO_PIN13_INIT_VALUE == DIO_PIN_INIT_VALUE_HIGH)
            #warning "PIN 3 is configured as input and defualt value is high, Corrected to LOW"
                      #undef  DIO_PIN13_INIT_VALUE
                      #define DIO_PIN13_INIT_VALUE   DIO_PIN_INIT_PIN_DEF
                    #endif
            
      #if (DIO_PIN14_INIT_DIRECTION == DIO_PIN14_INIT_DIRECTION_INPUT) && (DIO_PIN14_INIT_VALUE == DIO_PIN_INIT_VALUE_HIGH)
            #warning "PIN 3 is configured as input and defualt value is high, Corrected to LOW"
                      #undef  DIO_PIN14_INIT_VALUE
                      #define DIO_PIN14_INIT_VALUE   DIO_PIN_INIT_PIN_DEF
                    #endif
            
          #if (DIO_PIN15_INIT_DIRECTION == DIO_PIN15_INIT_DIRECTION_INPUT) && (DIO_PIN15_INIT_VALUE == DIO_PIN_INIT_VALUE_HIGH)
            #warning "PIN 3 is configured as input and defualt value is high, Corrected to LOW"
                      #undef  DIO_PIN15_INIT_VALUE
                      #define DIO_PIN15_INIT_VALUE   DIO_PIN_INIT_PIN_DEF
                    #endif
            
          #if (DIO_PIN16_INIT_DIRECTION == DIO_PIN16_INIT_DIRECTION_INPUT) && (DIO_PIN16_INIT_VALUE == DIO_PIN_INIT_VALUE_HIGH)
            #warning "PIN 3 is configured as input and defualt value is high, Corrected to LOW"
                      #undef  DIO_PIN16_INIT_VALUE
                      #define DIO_PIN16_INIT_VALUE   DIO_PIN_INIT_PIN_DEF
                    #endif
            
         #if (DIO_PIN17_INIT_DIRECTION == DIO_PIN0_INIT_DIRECTION_INPUT) && (DIO_PIN17_INIT_VALUE == DIO_PIN_INIT_VALUE_HIGH)
            #warning "PIN 3 is configured as input and defualt value is high, Corrected to LOW"
                      #undef  DIO_PIN17_INIT_VALUE
                      #define DIO_PIN17_INIT_VALUE   DIO_PIN_INIT_PIN_DEF
                    #endif
            
         #if (DIO_PIN18_INIT_DIRECTION == DIO_PIN18_INIT_DIRECTION_INPUT) && (DIO_PIN18_INIT_VALUE == DIO_PIN_INIT_VALUE_HIGH)
            #warning "PIN 3 is configured as input and defualt value is high, Corrected to LOW"
                      #undef  DIO_PIN18_INIT_VALUE
                      #define DIO_PIN18_INIT_VALUE   DIO_PIN_INIT_PIN_DEF
                    #endif
            
         #if (DIO_PIN19_INIT_DIRECTION == DIO_PIN19_INIT_DIRECTION_INPUT) && (DIO_PIN19_INIT_VALUE == DIO_PIN_INIT_VALUE_HIGH)
            #warning "PIN 3 is configured as input and defualt value is high, Corrected to LOW"
                      #undef  DIO_PIN19_INIT_VALUE
                      #define DIO_PIN19_INIT_VALUE   DIO_PIN_INIT_PIN_DEF
                    #endif
            
         #if (DIO_PIN20_INIT_DIRECTION == DIO_PIN20_INIT_DIRECTION_INPUT) && (DIO_PIN20_INIT_VALUE == DIO_PIN_INIT_VALUE_HIGH)
            #warning "PIN 3 is configured as input and defualt value is high, Corrected to LOW"
                      #undef  DIO_PIN20_INIT_VALUE
                      #define DIO_PIN20_INIT_VALUE   DIO_PIN_INIT_PIN_DEF
                    #endif
            
          #if (DIO_PIN0_INIT_DIRECTION == DIO_PIN21_INIT_DIRECTION_INPUT) && (DIO_PIN21_INIT_VALUE == DIO_PIN_INIT_VALUE_HIGH)
            #warning "PIN 3 is configured as input and defualt value is high, Corrected to LOW"
                      #undef  DIO_PIN21_INIT_VALUE
                      #define DIO_PIN21_INIT_VALUE   DIO_PIN_INIT_PIN_DEF
                    #endif
            
         #if (DIO_PIN22_INIT_DIRECTION == DIO_PIN22_INIT_DIRECTION_INPUT) && (DIO_PIN22_INIT_VALUE == DIO_PIN_INIT_VALUE_HIGH)
            #warning "PIN 3 is configured as input and defualt value is high, Corrected to LOW"
                      #undef  DIO_PIN22_INIT_VALUE
                      #define DIO_PIN22_INIT_VALUE   DIO_PIN_INIT_PIN_DEF
                    #endif
            
           #if (DIO_PIN22_INIT_DIRECTION == DIO_PIN22_INIT_DIRECTION_INPUT) && (DIO_PIN22_INIT_VALUE == DIO_PIN_INIT_VALUE_HIGH)
            #warning "PIN 3 is configured as input and defualt value is high, Corrected to LOW"
                      #undef  DIO_PIN22_INIT_VALUE
                      #define DIO_PIN22_INIT_VALUE   DIO_PIN_INIT_PIN_DEF
                    #endif
            
           #if (DIO_PIN23_INIT_DIRECTION == DIO_PIN23_INIT_DIRECTION_INPUT) && (DIO_PIN23_INIT_VALUE == DIO_PIN_INIT_VALUE_HIGH)
            #warning "PIN 3 is configured as input and defualt value is high, Corrected to LOW"
                      #undef  DIO_PIN23_INIT_VALUE
                      #define DIO_PIN23_INIT_VALUE   DIO_PIN_INIT_PIN_DEF
                    #endif
            
           #if (DIO_PIN24_INIT_DIRECTION == DIO_PIN24_INIT_DIRECTION_INPUT) && (DIO_PIN24_INIT_VALUE == DIO_PIN_INIT_VALUE_HIGH)
            #warning "PIN 3 is configured as input and defualt value is high, Corrected to LOW"
          #undef  DIO_PIN24_INIT_VALUE
          #define DIO_PIN24_INIT_VALUE   DIO_PIN_INIT_PIN_DEF
        #endif

		#if (DIO_PIN25_INIT_DIRECTION == DIO_PIN24_INIT_DIRECTION_INPUT) && (DIO_PIN24_INIT_VALUE == DIO_PIN_INIT_VALUE_HIGH)
           #warning "PIN 3 is configured as input and defualt value is high, Corrected to LOW"
         #undef  DIO_PIN24_INIT_VALUE
         #define DIO_PIN24_INIT_VALUE   DIO_PIN_INIT_PIN_DEF
       #endif

		#if (DIO_PIN26_INIT_DIRECTION == DIO_PIN24_INIT_DIRECTION_INPUT) && (DIO_PIN24_INIT_VALUE == DIO_PIN_INIT_VALUE_HIGH)
           #warning "PIN 3 is configured as input and defualt value is high, Corrected to LOW"
         #undef  DIO_PIN24_INIT_VALUE
         #define DIO_PIN24_INIT_VALUE   DIO_PIN_INIT_PIN_DEF
       #endif

		#if (DIO_PIN27_INIT_DIRECTION == DIO_PIN24_INIT_DIRECTION_INPUT) && (DIO_PIN24_INIT_VALUE == DIO_PIN_INIT_VALUE_HIGH)
           #warning "PIN 3 is configured as input and defualt value is high, Corrected to LOW"
         #undef  DIO_PIN24_INIT_VALUE
         #define DIO_PIN24_INIT_VALUE   DIO_PIN_INIT_PIN_DEF
       #endif

		#if (DIO_PIN28_INIT_DIRECTION == DIO_PIN24_INIT_DIRECTION_INPUT) && (DIO_PIN24_INIT_VALUE == DIO_PIN_INIT_VALUE_HIGH)
           #warning "PIN 3 is configured as input and defualt value is high, Corrected to LOW"
         #undef  DIO_PIN24_INIT_VALUE
         #define DIO_PIN24_INIT_VALUE   DIO_PIN_INIT_PIN_DEF
       #endif

		#if (DIO_PIN29_INIT_DIRECTION == DIO_PIN24_INIT_DIRECTION_INPUT) && (DIO_PIN24_INIT_VALUE == DIO_PIN_INIT_VALUE_HIGH)
           #warning "PIN 3 is configured as input and defualt value is high, Corrected to LOW"
         #undef  DIO_PIN24_INIT_VALUE
         #define DIO_PIN24_INIT_VALUE   DIO_PIN_INIT_PIN_DEF
       #endif

		#if (DIO_PIN30_INIT_DIRECTION == DIO_PIN24_INIT_DIRECTION_INPUT) && (DIO_PIN24_INIT_VALUE == DIO_PIN_INIT_VALUE_HIGH)
           #warning "PIN 3 is configured as input and defualt value is high, Corrected to LOW"
			#undef  DIO_PIN24_INIT_VALUE
         #define DIO_PIN24_INIT_VALUE   DIO_PIN_INIT_PIN_DEF
       #endif

		#if (DIO_PIN24_INIT_DIRECTION == DIO_PIN24_INIT_DIRECTION_INPUT) && (DIO_PIN24_INIT_VALUE == DIO_PIN_INIT_VALUE_HIGH)
           #warning "PIN 3 is configured as input and defualt value is high, Corrected to LOW"
			#undef  DIO_PIN24_INIT_VALUE
			#define DIO_PIN24_INIT_VALUE   DIO_PIN_INIT_PIN_DEF
       #endif

    
}



/* Description : Setting The direction of the DIO Component */
u8 DIO_u8SetPinDirection(u8 u8PinNb, u8 u8Direction)
{
	/* Local Variable */
	u8 u8ErrorState;
	u8 u8PortID;
	u8 u8PinID;

	if (
			(u8PinNb >= DIO_MAXNB) ||
			(	(u8Direction != DIO_OUTPUT_PIN) && (u8Direction != DIO_INPUT_PIN)
			)
		)
	{	u8ErrorState = ERROR_NOK; }

	else
	{
/* To Detect the PORT and the Pin form the input numper of PIn{0->32} :
 *  PORT Num = u8PinNB/8
 *  PIN  Num = u8PinNB%8
 *  EX :  u8PinNB=18 -> PORT Num = 2 , PIN Num = 2 -> SO its PORTC PIN 2
 */
		u8PortID = u8PinNb/NUMBER_PINS_IN_PORT;
		u8PinID  = u8PinNb%NUMBER_PINS_IN_PORT;

	/* Choose Specific Ports To SET the PIN DIRECTION IN */
		switch(u8PortID)
		{

			case DIO_PORTA:		ASSIGN_BIT(DIO_DDRA_Register,u8PinID,u8Direction);
					break;
			case DIO_PORTB:		ASSIGN_BIT(DIO_DDRB_Register,u8PinID,u8Direction);
					break;
			case DIO_PORTC:		ASSIGN_BIT(DIO_DDRC_Register,u8PinID,u8Direction);
					break;
			case DIO_PORTD:		ASSIGN_BIT(DIO_DDRD_Register,u8PinID,u8Direction);
					break;
		}
		u8ErrorState = ERROR_OK;
	}

	/* Function Return */
	return u8ErrorState;
}


/*********************************************************************************/
/*********************************************************************************/

/* Description : Setting The PINs of the DIO Component */
u8 DIO_u8SetPinValue(u8 u8PinNB, u8 u8Direction)
{
	/* Local Variable */
	u8 u8ErrorState;
	u8 u8PortID;
	u8 u8PinID;

	if (
			(u8PinNB >= DIO_MAXNB) ||
			(	(u8Direction != DIO_OUTPUT_PIN) && (u8Direction != DIO_INPUT_PIN)
			)
		)
	{	u8ErrorState = ERROR_NOK; }

	else
	{
/* To Detect the PORT and the Pin form the input numper of PIn{0->32} :
 *  PORT Num = u8PinNB/8
 *  PIN  Num = u8PinNB%8
 *  EX :  u8PinNB=18 -> PORT Num = 2 , PIN Num = 2 -> SO its PORTC PIN 2
 */
		u8PortID = u8PinNB/NUMBER_PINS_IN_PORT;
		u8PinID  = u8PinNB%NUMBER_PINS_IN_PORT;

	/* Choose Specific Ports To SET the PIN DIRECTION IN */
		switch(u8PortID)
		{

			case DIO_PORTA:		ASSIGN_BIT(DIO_PORTA_Register,u8PinID,u8Direction);
					break;
			case DIO_PORTB:		ASSIGN_BIT(DIO_PORTB_Register,u8PinID,u8Direction);
					break;
			case DIO_PORTC:		ASSIGN_BIT(DIO_PORTC_Register,u8PinID,u8Direction);
					break;
			case DIO_PORTD:		ASSIGN_BIT(DIO_PORTD_Register,u8PinID,u8Direction);
					break;
		}
		u8ErrorState = ERROR_OK;
	}

	/* Function Return */
	return u8ErrorState;
}


/*********************************************************************************/
/*********************************************************************************/

/* Description : Getting The Values of the DIO PINs Component */

u8 DIO_u8GetPinValue(u8 u8PinNB, u8* u8PinValue)
{
	/* Local Variable */
	u8 u8ErrorState;
	u8 u8PortID;
	u8 u8PinID;

	if (	(u8PinNB >= DIO_MAXNB) || (u8PinValue == NULL) 	)
	{	u8ErrorState = ERROR_NOK; }

	else
	{
		/* To Detect the PORT and the Pin form the input numper of PIn{0->32} :
		 *  PORT Num = u8PinNB/8
		 *  PIN  Num = u8PinNB%8
		 *  EX :  u8PinNB=18 -> PORT Num = 2 , PIN Num = 2 -> SO its PORTC PIN 2
		 */
		u8PortID = u8PinNB/NUMBER_PINS_IN_PORT;
		u8PinID  = u8PinNB%NUMBER_PINS_IN_PORT;

		switch(u8PortID)
				{

					case DIO_PORTA:		*(u8PinValue)=GET_BIT(DIO_PINA_Register,u8PinID);
							break;
					case DIO_PORTB:		*(u8PinValue)=GET_BIT(DIO_PINB_Register,u8PinID);
							break;
					case DIO_PORTC:		*(u8PinValue)=GET_BIT(DIO_PINC_Register,u8PinID);
							break;
					case DIO_PORTD:		*(u8PinValue)=GET_BIT(DIO_PIND_Register,u8PinID);
							break;
				}
		u8ErrorState = ERROR_OK;
	}

	/* Function Return */
	return u8ErrorState;

}


u8 DIO_u8SetPortDirection(u8 Copy_u8PortNB, u8 Copy_PortDirection)
{
	u8 u8ErrorState;

	if ( (Copy_u8PortNB >= DIO_u8MaxPortNB) || ( (Copy_PortDirection != DIO_U8_PORT_OUTPUT) && (Copy_PortDirection != DIO_U8_PORT_INPUT) ) )
		{
		u8ErrorState = ERROR_NOK;
		}
	else {
		switch(Copy_u8PortNB)
			{
			case DIO_PORTA:		ASSIGN_PORT(DIO_DDRA_Register, Copy_PortDirection);
								break;
			case DIO_PORTB:		ASSIGN_PORT(DIO_DDRB_Register, Copy_PortDirection);
								break;
			case DIO_PORTC:		ASSIGN_PORT(DIO_DDRC_Register, Copy_PortDirection);
								break;
			case DIO_PORTD:		ASSIGN_PORT(DIO_DDRD_Register, Copy_PortDirection);
								break;
			}
		u8ErrorState = ERROR_OK;
		}

	/* Function Return */
	return u8ErrorState;
}


u8 DIO_u8SetPortValue(u8 Copy_u8PortNB, u8 Copy_u8PortValue)
{
	u8 u8ErrorState;

	if ( (Copy_u8PortNB >= DIO_u8MaxPortNB) ||  (Copy_u8PortValue >= DIO_SIZE_U8)   )
		{
		u8ErrorState = ERROR_NOK;
		}
	else {
		switch(Copy_u8PortNB)
			{
			case DIO_PORTA:		ASSIGN_PORT(DIO_PORTA_Register, Copy_u8PortValue);
							break;
			case DIO_PORTB:		ASSIGN_PORT(DIO_PORTB_Register, Copy_u8PortValue);
							break;
			case DIO_PORTC:		ASSIGN_PORT(DIO_PORTC_Register, Copy_u8PortValue);
								break;
			case DIO_PORTD:		ASSIGN_PORT(DIO_PORTD_Register, Copy_u8PortValue);
							break;
			}
		u8ErrorState = ERROR_OK;
		}
	/* Function Return */
	return u8ErrorState;
}



u8 DIO_u8GetPortValue(u8 Copy_u8PortNB,u8* Copy_PortValue)
{
	u8 u8ErrorState;

	if (Copy_u8PortNB >= DIO_u8MaxPortNB)
	{
		u8ErrorState = ERROR_NOK;
	}
	else {

		switch(Copy_u8PortNB)
			{
			case DIO_PORTA:		*(Copy_PortValue) = GET_PORT(DIO_PINA_Register);
								break;
			case DIO_PORTB:		*(Copy_PortValue) = GET_PORT(DIO_PINB_Register);
								break;
			case DIO_PORTC:		*(Copy_PortValue) = GET_PORT(DIO_PINC_Register);
								break;
			case DIO_PORTD:		*(Copy_PortValue) = GET_PORT(DIO_PORTD_Register);
								break;
			}
		u8ErrorState = ERROR_OK;
	}

	return u8ErrorState;
}
