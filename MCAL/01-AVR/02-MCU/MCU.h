/*
 * MCU.h
 *
 *  Created on: Mar 20, 2019
 *      Author: EsSss
 */

#ifndef MCU_H_
#define MCU_H_

/*		 Status Register Address			*/
#define INT_SREG 	((Register*) 0x5F)->ByteAccess

/* Enabled SREG */
#define SREG_ENABLE		(u8)1
/* Disable SREG */
#define SREG_DISABLE	(u8)0


u8 MCU_u8EnableGlobalINT(void);

u8 MCU_u8DisableGlobalINT(void);


#endif /* MCU_H_ */
