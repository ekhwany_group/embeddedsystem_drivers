/*
 * MCU.c
 *
 *  Created on: Mar 20, 2019
 *      Author: EsSss
 */

/* LIBs Include */
#include "STD_TYPES.h"
#include "BIT_CALC.h"
/* Include MCU Header File */
#include "MCU.h"

u8 MCU_u8EnableGlobalINT(void){
	/* Locale Variable Definition */
	u8 Local_u8ErrorState=ERROR_OK;

	if (GET_BIT(INT_SREG,BIT7) != SREG_ENABLE  ){
		/* Enable The Global Interrupt PIN */
		SET_BIT(INT_SREG,BIT7);
	}
	else {
		Local_u8ErrorState=ERROR_NOK;
	}
	/* Return Error Level State */
	return Local_u8ErrorState;
}

u8 MCU_u8DisableGlobalINT(void){
	/* Locale Variable Definition */
	u8 Local_u8ErrorState=ERROR_OK;

	if (GET_BIT(INT_SREG,BIT7) != SREG_DISABLE  ){
		/* Enable The Global Interrupt PIN */
		CLR_BIT(INT_SREG,BIT7);
	}
	else {
		Local_u8ErrorState=ERROR_NOK;
	}
	/* Return Error Level State */
	return Local_u8ErrorState;
}
