
/*************************************ARM_DIO_prog.c********************************************/
/* Author  : Islam Gamal Abdel Naser                                                     					     */
/* Date    : 7 MAR, 2019                                                        					 */
/* Version : v01                                                                  					 */
/*****************************************************************************************************/
/*Description												                                 		  */
/*-----------												                               			  */
/*ARM_DIO_prog.c is a file from DIO Deriver include the header of function which 				  */
/*use to initiate all pins ,set pin direction , set pin value ,get pin value ,    					  */
/*set port direction , set port value, get port value		                          			      */
/******************************************************************************************************/
/* LIBS   Include */
#include "STD_TYPES.h"
#include "BIT_CALC.h"
/* Driver Includes */
#include "DIO_interface.h"
#include "DIO_private.h"
#include "DIO_config.h"


/***************************************************************/
/*Description: Initialization all pins direction and value
 * Inputs : void
 * Outputs: void
 * */
void DIO_voidIntialize(void)
{
	DIO_u8SetPinDirection(DIO_PIN0,DIO_INIT_MODE_PIN0);
	DIO_u8SetPinDirection(DIO_PIN1,DIO_INIT_MODE_PIN1);
	DIO_u8SetPinDirection(DIO_PIN2,DIO_INIT_MODE_PIN2);
	DIO_u8SetPinDirection(DIO_PIN3,DIO_INIT_MODE_PIN3);
	DIO_u8SetPinDirection(DIO_PIN4,DIO_INIT_MODE_PIN4);
	DIO_u8SetPinDirection(DIO_PIN5,DIO_INIT_MODE_PIN5);
	DIO_u8SetPinDirection(DIO_PIN6,DIO_INIT_MODE_PIN6);
	DIO_u8SetPinDirection(DIO_PIN7,DIO_INIT_MODE_PIN7);
	DIO_u8SetPinDirection(DIO_PIN8,DIO_INIT_MODE_PIN8);
	DIO_u8SetPinDirection(DIO_PIN9,DIO_INIT_MODE_PIN9);
	DIO_u8SetPinDirection(DIO_PIN11,DIO_INIT_MODE_PIN11);
	DIO_u8SetPinDirection(DIO_PIN12,DIO_INIT_MODE_PIN12);
	DIO_u8SetPinDirection(DIO_PIN13,DIO_INIT_MODE_PIN13);
	DIO_u8SetPinDirection(DIO_PIN14,DIO_INIT_MODE_PIN14);
	DIO_u8SetPinDirection(DIO_PIN15,DIO_INIT_MODE_PIN15);
	DIO_u8SetPinDirection(DIO_PIN16,DIO_INIT_MODE_PIN16);
	DIO_u8SetPinDirection(DIO_PIN17,DIO_INIT_MODE_PIN17);
	DIO_u8SetPinDirection(DIO_PIN18,DIO_INIT_MODE_PIN18);
	DIO_u8SetPinDirection(DIO_PIN19,DIO_INIT_MODE_PIN19);
	DIO_u8SetPinDirection(DIO_PIN20,DIO_INIT_MODE_PIN20);
	DIO_u8SetPinDirection(DIO_PIN21,DIO_INIT_MODE_PIN21);
	DIO_u8SetPinDirection(DIO_PIN22,DIO_INIT_MODE_PIN22);
	DIO_u8SetPinDirection(DIO_PIN23,DIO_INIT_MODE_PIN23);
	DIO_u8SetPinDirection(DIO_PIN24,DIO_INIT_MODE_PIN24);
	DIO_u8SetPinDirection(DIO_PIN25,DIO_INIT_MODE_PIN25);
	DIO_u8SetPinDirection(DIO_PIN26,DIO_INIT_MODE_PIN26);
	DIO_u8SetPinDirection(DIO_PIN27,DIO_INIT_MODE_PIN27);
	DIO_u8SetPinDirection(DIO_PIN28,DIO_INIT_MODE_PIN28);
	DIO_u8SetPinDirection(DIO_PIN29,DIO_INIT_MODE_PIN29);
	DIO_u8SetPinDirection(DIO_PIN30,DIO_INIT_MODE_PIN30);
	DIO_u8SetPinDirection(DIO_PIN31,DIO_INIT_MODE_PIN31);
	DIO_u8SetPinDirection(DIO_PIN32,DIO_INIT_MODE_PIN32);
	DIO_u8SetPinDirection(DIO_PIN33,DIO_INIT_MODE_PIN33);
	DIO_u8SetPinDirection(DIO_PIN34,DIO_INIT_MODE_PIN34);
	DIO_u8SetPinDirection(DIO_PIN35,DIO_INIT_MODE_PIN35);
	DIO_u8SetPinDirection(DIO_PIN36,DIO_INIT_MODE_PIN36);

	/***************************************************************************/
	/* 	PORTA Initialization Values */
	DIO_u8SetPinValue( DIO_PIN0 ,  DIO_PIN0_INIT_VAL  );
	DIO_u8SetPinValue( DIO_PIN1 ,  DIO_PIN1_INIT_VAL  );
	DIO_u8SetPinValue( DIO_PIN2 ,  DIO_PIN2_INIT_VAL  );
	DIO_u8SetPinValue( DIO_PIN3 ,  DIO_PIN3_INIT_VAL  );
	DIO_u8SetPinValue( DIO_PIN4 ,  DIO_PIN4_INIT_VAL  );
	DIO_u8SetPinValue( DIO_PIN5 ,  DIO_PIN5_INIT_VAL  );
	DIO_u8SetPinValue( DIO_PIN6 ,  DIO_PIN6_INIT_VAL  );
	DIO_u8SetPinValue( DIO_PIN7 ,  DIO_PIN7_INIT_VAL  );
	DIO_u8SetPinValue( DIO_PIN8 ,  DIO_PIN8_INIT_VAL  );
	DIO_u8SetPinValue( DIO_PIN9 ,  DIO_PIN9_INIT_VAL  );
	DIO_u8SetPinValue( DIO_PIN10 , DIO_PIN10_INIT_VAL );
	DIO_u8SetPinValue( DIO_PIN11 , DIO_PIN11_INIT_VAL );
	DIO_u8SetPinValue( DIO_PIN12 , DIO_PIN12_INIT_VAL );
	DIO_u8SetPinValue( DIO_PIN13 , DIO_PIN13_INIT_VAL );
	DIO_u8SetPinValue( DIO_PIN14 , DIO_PIN14_INIT_VAL );
	DIO_u8SetPinValue( DIO_PIN15 , DIO_PIN15_INIT_VAL );
	/* 	PORTB Initialization Values */
	DIO_u8SetPinValue( DIO_PIN16 , DIO_PIN16_INIT_VAL );
	DIO_u8SetPinValue( DIO_PIN17 , DIO_PIN17_INIT_VAL );
	DIO_u8SetPinValue( DIO_PIN18 , DIO_PIN18_INIT_VAL );
	DIO_u8SetPinValue( DIO_PIN19 , DIO_PIN19_INIT_VAL );
	DIO_u8SetPinValue( DIO_PIN20 , DIO_PIN20_INIT_VAL );
	DIO_u8SetPinValue( DIO_PIN21 , DIO_PIN21_INIT_VAL );
	DIO_u8SetPinValue( DIO_PIN22 , DIO_PIN22_INIT_VAL );
	DIO_u8SetPinValue( DIO_PIN23 , DIO_PIN23_INIT_VAL );
	DIO_u8SetPinValue( DIO_PIN24 , DIO_PIN24_INIT_VAL );
	DIO_u8SetPinValue( DIO_PIN25 , DIO_PIN25_INIT_VAL );
	DIO_u8SetPinValue( DIO_PIN26 , DIO_PIN26_INIT_VAL );
	DIO_u8SetPinValue( DIO_PIN27 , DIO_PIN27_INIT_VAL );
	DIO_u8SetPinValue( DIO_PIN28 , DIO_PIN28_INIT_VAL );
	DIO_u8SetPinValue( DIO_PIN29 , DIO_PIN29_INIT_VAL );
	DIO_u8SetPinValue( DIO_PIN30 , DIO_PIN30_INIT_VAL );
	DIO_u8SetPinValue( DIO_PIN31 , DIO_PIN31_INIT_VAL );
	/* 	PORTC Initialization Values */
	DIO_u8SetPinValue( DIO_PIN32 , DIO_PIN32_INIT_VAL );
	DIO_u8SetPinValue( DIO_PIN33 , DIO_PIN33_INIT_VAL );
	DIO_u8SetPinValue( DIO_PIN34 , DIO_PIN34_INIT_VAL );
	/* 	PORTD Initialization Values */
	DIO_u8SetPinValue( DIO_PIN35 , DIO_PIN35_INIT_VAL );
	DIO_u8SetPinValue( DIO_PIN36 , DIO_PIN36_INIT_VAL );
}



/********************************DIO_u8SetPinDirection********************************/

/*Description: set pin Direction
 * Inputs : u8  Copy_u8PinNB     : pin Number
                                  Range PIN0 TO PIN31
 * 		  : u8  Copy_u8Direction : if it DIO_u8_PIN_INIT_INPUT mean that this pin is input ,
                                if it DIO_u8_PIN_INIT_OUTPUT mean that this pin is output
 * Outputs: Error state
 *
 * */
u8 DIO_u8SetPinDirection(u8 Copy_u8PinNB,u8 Copy_u8Mode)
{
	/*Local variables*/
	u8 Local_u8OperationStatus = ERROR_OK;
	u8 Local_u8ActualPortNb = Copy_u8PinNB/DIO_u8_PORT_SIZE;
	u8 Local_u8ActualPinNb  = Copy_u8PinNB %DIO_u8_PORT_SIZE ;


	if(Copy_u8Mode == DIO_PIN_DIR_INPUT_PULL_UP)
	{
		switch(Local_u8ActualPortNb)
		{
		case 	DIO_u8_PORTA:	ASSIGN_BIT(DIO_GPIOA->ODR.RegisterAccess,Local_u8ActualPinNb,DIO_u8_HIGH); break;
    	case    DIO_u8_PORTB:	ASSIGN_BIT(DIO_GPIOB->ODR.RegisterAccess,Local_u8ActualPinNb,DIO_u8_HIGH); break;
    	case  	DIO_u8_PORTC:	ASSIGN_BIT(DIO_GPIOC->ODR.RegisterAccess,Local_u8ActualPinNb,DIO_u8_HIGH); break;
    	case    DIO_u8_PORTD:	ASSIGN_BIT(DIO_GPIOD->ODR.RegisterAccess,Local_u8ActualPinNb,DIO_u8_HIGH); break;
		}
	}
	else if(Copy_u8Mode == DIO_PIN_DIR_INPUT_PULL_DOWN)
	{
		switch(Local_u8ActualPortNb)
		{
		case 	DIO_u8_PORTA:	ASSIGN_BIT(DIO_GPIOA->ODR.RegisterAccess,Local_u8ActualPinNb,DIO_u8_LOW); break;
    	case    DIO_u8_PORTB:	ASSIGN_BIT(DIO_GPIOB->ODR.RegisterAccess,Local_u8ActualPinNb,DIO_u8_LOW); break;
    	case  	DIO_u8_PORTC:	ASSIGN_BIT(DIO_GPIOC->ODR.RegisterAccess,Local_u8ActualPinNb,DIO_u8_LOW); break;
    	case    DIO_u8_PORTD:	ASSIGN_BIT(DIO_GPIOD->ODR.RegisterAccess,Local_u8ActualPinNb,DIO_u8_LOW); break;
		}

		Copy_u8Mode = DIO_PIN_DIR_INPUT_PULL_UP;
	}

	if(Local_u8ActualPinNb < PRIV_CRL_LIMIT )
	{
		switch(Local_u8ActualPortNb)
		{
		case DIO_u8_PORTA:	ASSIGN_NIBBLE(DIO_GPIOA->CRL.RegisterAccess,Local_u8ActualPinNb,Copy_u8Mode); break;
		case DIO_u8_PORTB:	ASSIGN_NIBBLE(DIO_GPIOB->CRL.RegisterAccess,Local_u8ActualPinNb,Copy_u8Mode); break;
		case DIO_u8_PORTC:	ASSIGN_NIBBLE(DIO_GPIOC->CRL.RegisterAccess,Local_u8ActualPinNb,Copy_u8Mode); break;
		case DIO_u8_PORTD:	ASSIGN_NIBBLE(DIO_GPIOD->CRL.RegisterAccess,Local_u8ActualPinNb,Copy_u8Mode); break;
		default:  Local_u8OperationStatus = ERROR_NOK;
		}
	}
	else
	{
		Local_u8ActualPinNb -=PRIV_CRL_LIMIT;
		switch(Local_u8ActualPortNb)
		{
		case DIO_u8_PORTA:	ASSIGN_NIBBLE(DIO_GPIOA->CRH.RegisterAccess,Local_u8ActualPinNb,Copy_u8Mode); break;
		case DIO_u8_PORTB:	ASSIGN_NIBBLE(DIO_GPIOB->CRH.RegisterAccess,Local_u8ActualPinNb,Copy_u8Mode); break;
		case DIO_u8_PORTC:	ASSIGN_NIBBLE(DIO_GPIOC->CRH.RegisterAccess,Local_u8ActualPinNb,Copy_u8Mode); break;
		case DIO_u8_PORTD:	ASSIGN_NIBBLE(DIO_GPIOD->CRH.RegisterAccess,Local_u8ActualPinNb,Copy_u8Mode); break;
		default:  Local_u8OperationStatus = ERROR_NOK;
		}
	}
	/*Function Return*/
	return Local_u8OperationStatus;
}

/*****************************************************************************/
/****************************************************************/
/*Description: Set pin value
 * Inputs : u8 u8PinNB   : pin Number
                           Range PIN0 TO PIN31
 * 		  : u8 pu8value :  value which it may be high (1) or low (0)
 * Outputs: Error state
 * */
u8 DIO_u8SetPinValue(u8 Copy_u8PinNB,u8 Copy_pu8value)
{
	/*Local variables*/
	u8 Local_u8OperationStatus = ERROR_OK;
	u8 Local_u8PortNb 		   = Copy_u8PinNB/DIO_u8_PORT_SIZE;
	u8 Local_u8PinNb 		   = Copy_u8PinNB %DIO_u8_PORT_SIZE ;

	switch(Copy_pu8value)
	{
	case DIO_u8_HIGH: Local_u8OperationStatus = u8SetPin(Local_u8PortNb,Local_u8PinNb);   break;
	case DIO_u8_LOW : Local_u8OperationStatus = u8ResetPin(Local_u8PortNb,Local_u8PinNb); break;
	default: Local_u8OperationStatus= ERROR_NOK;
	}
	return Local_u8OperationStatus;
}

/*u8SetPin : private function use to */
static u8 u8SetPin(u8 Copy_u8Port , u8 Copy_u8Pin)
{
	u8 Local_u8Result = ERROR_OK;

	switch(Copy_u8Port)
	{
	case DIO_u8_PORTA:	DIO_GPIOA->BSRR.RegisterAccess = (1<<Copy_u8Pin); break;
	case DIO_u8_PORTB:	DIO_GPIOB->BSRR.RegisterAccess = (1<<Copy_u8Pin); break;
	case DIO_u8_PORTC:	DIO_GPIOC->BSRR.RegisterAccess = (1<<Copy_u8Pin); break;
	case DIO_u8_PORTD:	DIO_GPIOD->BSRR.RegisterAccess = (1<<Copy_u8Pin); break;
	default 		 :  Local_u8Result = ERROR_NOK;
	}
	return Local_u8Result;
}

/*****************************************************************************/
static u8 u8ResetPin(u8 Copy_u8Port , u8 Copy_u8Pin)
{
	u8 Local_u8Result = ERROR_OK;

	switch(Copy_u8Port)
	{
	case DIO_u8_PORTA:	DIO_GPIOA->BRR.RegisterAccess = (1<<Copy_u8Pin); break;
	case DIO_u8_PORTB:	DIO_GPIOB->BRR.RegisterAccess = (1<<Copy_u8Pin); break;
	case DIO_u8_PORTC:	DIO_GPIOC->BRR.RegisterAccess = (1<<Copy_u8Pin); break;
	case DIO_u8_PORTD:	DIO_GPIOD->BRR.RegisterAccess = (1<<Copy_u8Pin); break;
	default 		 :  Local_u8Result = ERROR_NOK;
	}
	return Local_u8Result;
}

/***************************************************************************/
/****************************************************************/

/*Description: get pin value
 * Inputs : u8 u8PinNB   : pin Number
                           Range PIN0 TO PIN31
 * 		  : u8* pu8value : pointer to value which it may be high (1) or low (0)
 * Outputs: Error state
 *
 * */
u8 DIO_u8GetPinValue(u8 Copy_u8PinNB,u8* Copy_pu8value)
{
	/*Local variables*/
	u8 Local_u8OperationStatus = ERROR_OK;
	u8 Local_u8ActualPortNb    = Copy_u8PinNB /DIO_u8_PORT_SIZE;
	u8 Local_u8ActualPinNb 	   = Copy_u8PinNB %DIO_u8_PORT_SIZE;

	switch(Local_u8ActualPortNb)
	{
	case DIO_u8_PORTA:	*(Copy_pu8value)= GET_BIT(DIO_GPIOA->IDR.RegisterAccess, Local_u8ActualPinNb); break;
	case DIO_u8_PORTB:	*(Copy_pu8value)= GET_BIT(DIO_GPIOB->IDR.RegisterAccess, Local_u8ActualPinNb); break;
    case DIO_u8_PORTC:	*(Copy_pu8value)= GET_BIT(DIO_GPIOC->IDR.RegisterAccess, Local_u8ActualPinNb); break;
	case DIO_u8_PORTD:	*(Copy_pu8value)= GET_BIT(DIO_GPIOD->IDR.RegisterAccess, Local_u8ActualPinNb); break;
	default : Local_u8OperationStatus = ERROR_NOK;
	}
	return Local_u8OperationStatus;
}
/****************************************************************/

/*Description: set port value
 * Inputs : u8 u8PortNB     : port Number
                             Range PORTA TO PORTD
 * 		  : u8 u8Direction : if it DIO_u8_PORT_INIT_LOW mean that all pins of this port are zero ,
 * 		                    if it DIO_u8_PORT_INIT_HIGH mean that all pins of this port are one
 * Outputs: Error state
 *
 * */
u8 DIO_u8SetPortValue(u8 Copy_u8PortNB,u8 Copy_u8Value){
	u8 Local_u8OperationStatus     = ERROR_OK;

	switch(Copy_u8PortNB){
		case DIO_u8_PORTA:	ASSIGN_PORT(DIO_GPIOA->ODR.RegisterAccess,Copy_u8Value); break;
		case DIO_u8_PORTB:	ASSIGN_PORT(DIO_GPIOB->ODR.RegisterAccess,Copy_u8Value); break;
		case DIO_u8_PORTC:	ASSIGN_PORT(DIO_GPIOC->ODR.RegisterAccess,Copy_u8Value); break;
		case DIO_u8_PORTD:	ASSIGN_PORT(DIO_GPIOD->ODR.RegisterAccess,Copy_u8Value); break;
		default : Local_u8OperationStatus = ERROR_NOK;
	}

	return Local_u8OperationStatus;
}
