/***************************************************/
/* Author   : Ahmed Assaf                          */
/* Version  : V01                                  */
/* Date     : 27 FEB 2019                          */
/***************************************************/
#ifndef _DIO_INTERFACE_H
#define _DIO_INTERFACE_H


#define DIO_u8_PIN0            (u8)0
#define DIO_u8_PIN1            (u8)1        
#define DIO_u8_PIN2            (u8)2
#define DIO_u8_PIN3            (u8)3
#define DIO_u8_PIN4            (u8)4
#define DIO_u8_PIN5            (u8)5
#define DIO_u8_PIN6            (u8)6
#define DIO_u8_PIN7            (u8)7
#define DIO_u8_PIN8            (u8)8
#define DIO_u8_PIN9            (u8)9
#define DIO_u8_PIN10           (u8)10
#define DIO_u8_PIN11           (u8)11
#define DIO_u8_PIN12           (u8)12
#define DIO_u8_PIN13           (u8)13
#define DIO_u8_PIN14           (u8)14
#define DIO_u8_PIN15           (u8)15
#define DIO_u8_PIN16           (u8)16
#define DIO_u8_PIN17           (u8)17
#define DIO_u8_PIN18           (u8)18
#define DIO_u8_PIN19           (u8)19
#define DIO_u8_PIN20           (u8)20
#define DIO_u8_PIN21           (u8)21
#define DIO_u8_PIN22           (u8)22
#define DIO_u8_PIN23           (u8)23
#define DIO_u8_PIN24           (u8)24
#define DIO_u8_PIN25           (u8)25
#define DIO_u8_PIN26           (u8)26
#define DIO_u8_PIN27           (u8)27
#define DIO_u8_PIN28           (u8)28
#define DIO_u8_PIN29           (u8)29
#define DIO_u8_PIN30           (u8)30
#define DIO_u8_PIN31           (u8)31
#define DIO_u8_PIN32           (u8)45
#define DIO_u8_PIN33           (u8)46
#define DIO_u8_PIN34           (u8)47
#define DIO_u8_PIN35           (u8)48
#define DIO_u8_PIN36           (u8)49


#define DIO_u8_HIGH            (u8)1
#define DIO_u8_LOW             (u8)0

#define DIO_u8_PORTA           (u8)0
#define DIO_u8_PORTB           (u8)1
#define DIO_u8_PORTC           (u8)2
#define DIO_u8_PORTD           (u8)3 

#define DIO_u8_INPUT_ANALOG          0b0000
#define DIO_u8_INPUT_FLOATING        0b0100
#define DIO_u8_INPUT_PULL_UP         0b1000
#define DIO_u8_INPUT_PULL_DOWN       0b1100

#define DIO_u8_OUTPUT_2M_PP          0b0010
#define DIO_u8_OUTPUT_2M_OD          0b0110
#define DIO_u8_OUTPUT_2M_AFPP        0b1010
#define DIO_u8_OUTPUT_2M_AFOD        0b1110

#define DIO_u8_OUTPUT_10M_PP         0b0001
#define DIO_u8_OUTPUT_10M_OD         0b0101
#define DIO_u8_OUTPUT_10M_AFPP       0b1001
#define DIO_u8_OUTPUT_10M_AFOD       0b1101

#define DIO_u8_OUTPUT_50M_PP         0b0011
#define DIO_u8_OUTPUT_50M_OD         0b0111
#define DIO_u8_OUTPUT_50M_AFPP       0b1011
#define DIO_u8_OUTPUT_50M_AFOD       0b1111




void DIO_voidInitialize(void);

u8   DIO_u8SetPinValue (u8 Copy_u8PinNb, u8  Copy_u8Value );
u8   DIO_u8GetPinValue (u8 Copy_u8PinNb, u8* Copy_Pu8Value);
u8   DIO_u8SetPinDir   (u8 Copy_u8PinNb, u8 Copy_u8Mode   );

#endif