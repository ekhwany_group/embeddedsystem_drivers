/***************************************************/
/* Author   : Ahmed Assaf                          */
/* Version  : V01                                  */
/* Date     : 27 FEB 2019                          */
/***************************************************/
#ifndef _DIO_PRIVATE_H
#define _DIO_PRIVATE_H

typedef struct
{
	Register_32Bit   CRL ;
	Register_32Bit   CRH ;
	Register_32Bit   IDR ;
	Register_32Bit   ODR ;
	Register_32Bit   BSRR;
	Register_32Bit   BRR ;
	Register_32Bit   LCKR;
}GPIO;

#define DIO_GPIOA     ((GPIO*)0x40010800)
#define DIO_GPIOB     ((GPIO*)0x40010C00)
#define DIO_GPIOC     ((GPIO*)0x40011000)
#define DIO_GPIOD     ((GPIO*)0x40011400)

static u8 u8SetPin(u8 Copy_u8Port, u8 Copy_u8Pin);
static u8 u8RstPin(u8 Copy_u8Port, u8 Copy_u8Pin);

#endif 