#ifndef STD_TYPES_H
#define STD_TYPES_H


/******************** DATA_TYPES ************************/
/********************************************************/
/* Author: ISLAM GMAL ABDEL NASSER					          	*/
/* DATE: 31 JAN 2019						                  			*/
/* VERSION : V1.0								                    		*/
/********************************************************/
/* DESCRIPTION: 							                    			*/
/*	This File Is Used To Define The Standard Used Types	*/
/********************************************************/




/**************************************/
/* UnSigned Character : 			        */
typedef unsigned char 		   	u8	;
/* UnSigned Short Integer : 		      */
typedef unsigned short int 	  u16 ;
/* UnSigned Long Integer : 			      */
typedef unsigned long  int		u32 ;
/* Signed Character : 				        */
typedef signed   char 			  s8  ;
/* Signed Short Integer : 		        */
typedef signed   short int		s16 ;
/* Signed Long Integer : 			        */
typedef signed   long  int  	s32 ;
/*  Floating Numbers : 			    	    */
typedef 		 float 			      f32 ;
/*  Double Numbers :				          */
typedef 		 double 		      f64 ;
/**************************************/


/* ERROR STATE     					   */
#define ERROR_NOK 1

/* NO ERROR STATE   			 	   */
#define ERROR_OK  0

/* NULL Pointer     			  	 */
#define NULL 	 ((void *)0)




/* Accessing ANY MEMORY Using BYTEACCESS or BIT ACCESS */
typedef union {
	struct {
		u8 Bit0 : 1;
		u8 Bit1 : 1;
		u8 Bit2 : 1;
		u8 Bit3 : 1;
		u8 Bit4 : 1;
		u8 Bit5 : 1;
		u8 Bit6 : 1;
		u8 Bit7 : 1;
	}BitAccess;
	u8 ByteAccess;
}Register;


typedef union {

	struct {
		u32 Bit0 : 1;
		u32 Bit1 : 1;
		u32 Bit2 : 1;
		u32 Bit3 : 1;
		u32 Bit4 : 1;
		u32 Bit5 : 1;
		u32 Bit6 : 1;
		u32 Bit7 : 1;
        u32 Bit8 : 1;
	 	u32 Bit9 : 1;
		u32 Bit10 : 1;
		u32 Bit11 : 1;
		u32 Bit12 : 1;
		u32 Bit13 : 1;
		u32 Bit14 : 1;
		u32 Bit15 : 1;
		u32 Bit16 : 1;
		u32 Bit17 : 1;
		u32 Bit18 : 1;
		u32 Bit19 : 1;
		u32 Bit20 : 1;
		u32 Bit21 : 1;
		u32 Bit22 : 1;
		u32 Bit23 : 1;
		u32 Bit24 : 1;
		u32 Bit25 : 1;
		u32 Bit26 : 1;
		u32 Bit27 : 1;
        u32 Bit28 : 1;
		u32 Bit29 : 1;
		u32 Bit30 : 1;
		u32 Bit31 : 1;
	}BitAccess;

	u32 RegisterAccess;
}Register_32Bit;


#endif
