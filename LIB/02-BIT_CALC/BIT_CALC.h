#ifndef BIT_CALC_H
#define BIT_CALC_H

/**********************BIT MANAPULATION LIBRARY**************************/

/************************************************************************/

/********************************************************/
/* Author: ISLAM GMAL ABDEL NASSER						*/
/* DATE: 31 JAN 2019									*/
/* VERSION : V1.0										*/
/********************************************************/
/* DESCRIPTION: 										*/
/*This file is used to configure the software Component	*/
/*by Initializing each bin wither OUTPUT or INPUT		*/
/********************************************************/


    /**********************CONC_8BIT*************************************/
   /*THIS MACRO LIKE FUNCTION USED TO CONCATINATE 8 BITs               */
  /*********INPUT ARGUMENT:********************************************/
 /*			b : The 8 Bits Needed To Concatinate   				     */
/********************************************************************/
#define CONC_8BIT(b0,b1,b2,b3,b4,b5,b6,b7)    CONC_HELPER(b0,b1,b2,b3,b4,b5,b6,b7)
#define CONC_HELPER(b0,b1,b2,b3,b4,b5,b6,b7)  0b##b7##b6##b5##b4##b3##b2##b1##b0


	 /************************ASSIGN_BIT MACRO*****************************/
    /*THIS MACRO LIKE FUNCTION USED TO ASSIGN ANY BIT IN VARIABLE BY 1/0 */
   /*********INPUT ARGUMENT:*********************************************/
  /*		VAR   : THE VARIABLE NEEDE TO SET BIT IN			       */
 /*			BITNO : THE NUMBER OF BIT TO BE SETTED				      */
/*********************************************************************/
#define ASSIGN_BIT(VAR,BITNO,VALUE)		(VAR) = ( (VAR) & (~(1<<BITNO) ) ) | (VALUE<<BITNO)

	 /********************ASSIGN_PORT MACRO*******************************/
    /*THIS MACRO LIKE FUNCTION USED TO ASSIGN ANY PORT IN DIO PRIPHIERAL*/
   /*********INPUT ARGUMENT:********************************************/
  /*		PORTNB   : THE VARIABLE NEEDE TO SET BIT IN				  */
 /*			DIRECTION : THE NUMBER OF BIT TO BE SETTED				 */
/********************************************************************/
#define	ASSIGN_PORT(PORTNB,DIRECTION) 	(PORTNB) = (DIRECTION)


	 /**********************SET_BIT MACRO********************************/
    /*THIS MACRO LIKE FUNCTION USED TO SET ANY BIT IN AN VARIABLE BY 1 */
   /*********INPUT ARGUMENT:*******************************************/
  /*		VAR   : THE VARIABLE NEEDE TO SET BIT IN				 */
 /*			BITNO : THE NUMBER OF BIT TO BE SETTED					*/
/*******************************************************************/
#define	SET_BIT(VAR,BITNO) 	(VAR) |= (1 << (BITNO))


	  /**********************SET_H_NIBBLE MACRO*******************/
	 /*THIS MACRO LIKE FUNCTION USED TO SET THE HIGH NIBBLE    **/ 
	/* IN A VARIABLE TO(1).                                    */
   /*********INPUT ARGUMENT:***********************************/
  /*	VAR  : THE VARIABLE NEEDED TO SET THE HIGH NIBBLE IN */
 /***********************************************************/
#define SET_H_NIBBLE(VAR)	 	(VAR) |= (0X F0)

	  /**********************SET_L_NIBBLE MACRO*******************/
	 /* THIS MACRO LIKE FUNCTION USED TO SET THE LOW NIBBLE     */ 
	/*  IN A VARIABLE TO(1).                                   */
   /*********INPUT ARGUMENT:***********************************/
  /*		VAR  : THE VARIABLE NEEDE TO SET BIT IN			 */
 /*			BITNO: THE NUMBER OF BIT TO BE SETTED			*/
/***********************************************************/
#define SET_L_NIBBLE(VAR)	 	(VAR) |= (0X 0F)


/*************************************************************************/
/*************************************************************************/


	 /**********************CLR_BIT MACRO********************************/
    /*THIS MACRO LIKE FUNCTION USED TO SET EXACT BIT IN A VARIABLE BY 0*/
   /*********INPUT ARGUMENT:*******************************************/
  /*		VAR   : THE VARIABLE NEEDE TO SET BIT IN				 */
 /*			BITNO : THE NUMBER OF BIT TO BE SETTED					*/
/*******************************************************************/
#define	CLR_BIT(VAR,BITNO) 	(VAR) &= ~(1 << (BITNO))

   	 /**********************CLR_H_NIBBLE MACRO******************/
    /* THIS MACRO LIKE FUNCTION USED TO SET THE HIGH NIBBLE   */ 
   /*  IN A VARIABLE TO(0).                                  */   
  /*********INPUT ARGUMENT:**********************************/
 /*		VAR  : THE VARIABLE NEEDE TO CLEAR HIGH NIBBLE IN  */
/**********************************************************/
#define CLR_H_NIBBLE(VAR)	 	(VAR) &= (0X 0F)


   	 /**********************CLR_L_NIBBLE MACRO****************/
    /* THIS MACRO LIKE FUNCTION USED TO SET THE LOW NIBBLE  */ 
   /*  IN ANY VARIABLE TO(0).                              */   
  /*********INPUT ARGUMENT:********************************/
 /*		VAR  : THE VARIABLE NEEDE TO CLEAR LOW NIBBLE IN */
/********************************************************/
#define CLR_L_NIBBLE(VAR)	 	(VAR) &= (0X F0)


/*************************************************************************/
/*************************************************************************/


   	 /**********************GET_BIT MACRO     ****************/
    /* THIS MACRO LIKE FUNCTION USED TO SET THE LOW NIBBLE  */
   /*  IN ANY VARIABLE TO(0).                              */
  /*********INPUT ARGUMENT:********************************/
 /*		VAR  : THE VARIABLE NEEDE TO CLEAR LOW NIBBLE IN */
/********************************************************/
#define GET_BIT(VAR,BITNO) 		(((VAR) >> (BITNO)) & 0x01)


   	 /**********************GET_PORT MACRO     ***************/
    /* THIS MACRO LIKE FUNCTION USED TO GET THE VALUE       */
   /*  OF ANY VARIABLE.		                               */
  /*********INPUT ARGUMENT:********************************/
 /*		VAR  : THE VARIABLE NEEDE TO CLEAR LOW NIBBLE IN */
/********************************************************/
#define GET_PORT(PORT_REG) 		PORT_REG



/*************************************************************************/
/*************************************************************************/

	  /**********************TOGGLE_BIT MACRO***************/
     /* THIS MACRO LIKE FUNCTION USED TO TOGGLE EXACT BIT */ 
    /*  IN A VARIABLE FROM(1) TO (0) AND VICEVERSA.      */   
   /*********INPUT ARGUMENT:*****************************/
  /*		VAR  : THE VARIABLE NEEDE TO SET BIT IN    */
 /*         BITNO : THE NUMBER OF BIT TO BE SETTED    */
/*****************************************************/
#define	TOGGLE_BIT(VAR,BITNO) 	(VAR) ^= (1 << (BITNO))


   	 /**********************TOGGLE_H_NIBBLE MACRO****************/
    /* THIS MACRO LIKE FUNCTION USED TO TOGGLE THE HIGH NIBBLE */ 
   /*  IN ANY VARIABLE FROM(1) TO (0) AND VICEVERSA.          */                                
  /*********INPUT ARGUMENT:***********************************/
 /*		VAR  : THE VARIABLE NEEDE TO TOGGLE HIGH NIBBLE IN  */
/***********************************************************/
#define TOGGLE_H_NIBBLE(VAR)	 	(VAR) ^= (0X F0)

   	 /**********************TOGGLE_L_NIBBLE MACRO****************/
    /* THIS MACRO LIKE FUNCTION USED TO TOGGLE THE LOW NIBBLE  */ 
   /*  IN ANY VARIABLE FROM(1) TO (0) AND VICEVERSA.          */                                
  /*********INPUT ARGUMENT:***********************************/
 /*		VAR  : THE VARIABLE NEEDE TO TOGGLE LOW NIBBLE IN   */
/***********************************************************/
#define TOGGLE_L_NIBBLE(VAR)	 	(VAR) ^= (0X 0F)

   	   /**********************ASSIGN_NIBBLE MACRO  *******************/
      /* THIS MACRO LIKE FUNCTION USED TO ASSIGN SPECIFIC NIBBLE    */
     /*  IN ANY REGISTER.								           */
    /*********INPUT ARGUMENT:**************************************/
   /*		REG    : THE REGISTER NEEDE TO ASSIGN NIBBLE IN.     */
  /*		NIBLE  : THE NIBLE NEEDE TO BE ASSIGNED.            */
 /*		VALUE  : THE VALUE THAT WILL BE ASSIGNED IN THE NIBBLE.*/
/**************************************************************/
#define ASSIGN_NIBBLE(REG,NIBLE_NO,VALUE)  REG = (REG & (~((u32)0xF<<(NIBLE_NO*4)))) | (VALUE<<(NIBLE_NO*4))


/***********	 BIT MAP *********/
#define BIT0	(u8)0
#define BIT1	(u8)1
#define BIT2	(u8)2
#define BIT3	(u8)3
#define BIT4	(u8)4
#define BIT5	(u8)5
#define BIT6	(u8)6
#define BIT7	(u8)7
#define BIT8	(u8)8
#define BIT9	(u8)9
#define BIT10	(u8)10
#define BIT11	(u8)11
#define BIT12	(u8)12
#define BIT13	(u8)13
#define BIT14	(u8)14
#define BIT15	(u8)15
#define BIT16	(u8)16
#define BIT17	(u8)17
#define BIT18	(u8)18
#define BIT19	(u8)19
#define BIT20	(u8)20
#define BIT21	(u8)21
#define BIT22	(u8)22
#define BIT23	(u8)23
#define BIT24	(u8)24
#define BIT25	(u8)25
#define BIT26	(u8)26
#define BIT27	(u8)27
#define BIT28	(u8)28
#define BIT29	(u8)29
#define BIT30	(u8)30
#define BIT31	(u8)31

#endif
