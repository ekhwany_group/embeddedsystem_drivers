/*
 * SevenSegment_interface.h
 *
 *  Created on: Feb 1, 2019
 *      Author: EsSss
 */





/********************************************************/
/* Author: ISLAM GMAL ABDEL NASSER						*/
/* DATE: 31 JAN 2019									*/
/* VERSION : V1.0										*/
/* LAYER : HAL											*/
/********************************************************/
/* DESCRIPTION: 										*/
/*This file is used to Interface the software Component	*/
/*With the used utilities and parameters				*/
/********************************************************/





#ifndef SEVENSEGMENT_INTERFACE_H_
#define SEVENSEGMENT_INTERFACE_H_


#define SSD_PINS 	(u8)7



/* The Used Installed SSDs In The System */
#define SSD0	(u8)0
#define SSD1	(u8)1
#define SSD2	(u8)2
#define SSD3	(u8)3

/* The Maximum Value That Can Be Displayed On The SSD */
#define SEV_SEG_MAX_DISPLAY_PATTERN 	(u8)10




/* The 9 Numbers For The Cathode 7Segment */
#define SEV_SEG_CATH_NUM0	{1,1,1,1,1,1,0,0}
#define SEV_SEG_CATH_NUM1	{0,1,1,0,0,0,0,0}
#define SEV_SEG_CATH_NUM2	{1,1,0,1,1,0,1,0}
#define SEV_SEG_CATH_NUM3	{1,1,1,1,0,0,1,0}
#define SEV_SEG_CATH_NUM4	{0,1,1,0,0,1,1,0}
#define SEV_SEG_CATH_NUM5	{1,0,1,1,0,1,1,0}
#define SEV_SEG_CATH_NUM6	{1,0,1,1,1,1,1,0}
#define SEV_SEG_CATH_NUM7	{1,1,1,0,0,0,0,0}
#define SEV_SEG_CATH_NUM8	{1,1,1,1,1,1,1,0}
#define SEV_SEG_CATH_NUM9	{1,1,1,1,0,1,1,0}


/********************************************************/
/********************************************************/

/* The 9 Numbers For The Anode 7Segment */
#define SEV_SEG_ANODE_NUM0	{0,0,1,0,0,0,0,0}
#define SEV_SEG_ANODE_NUM1	{0,1,1,1,1,0,0,1}
#define SEV_SEG_ANODE_NUM2	{0,1,0,0,0,1,0,0}
#define SEV_SEG_ANODE_NUM3	{0,1,0,1,0,0,0,0}
#define SEV_SEG_ANODE_NUM4	{0,0,0,1,1,0,0,1}
#define SEV_SEG_ANODE_NUM5	{0,0,0,1,0,0,1,0}
#define SEV_SEG_ANODE_NUM6	{0,0,0,0,0,0,1,0}
#define SEV_SEG_ANODE_NUM7	{0,0,1,1,1,0,0,0}
#define SEV_SEG_ANODE_NUM8	{0,0,0,0,0,0,0,0}
#define SEV_SEG_ANODE_NUM9	{0,0,0,1,0,0,0,0}


/********************************************************/
/********************************************************/

/* The 9 Numbers That can be Displayed */
#define SSD_NUM0	(u8)0
#define SSD_NUM1	(u8)1
#define SSD_NUM2	(u8)2
#define SSD_NUM3	(u8)3
#define SSD_NUM4	(u8)4
#define SSD_NUM5	(u8)5
#define SSD_NUM6	(u8)6
#define SSD_NUM7	(u8)7
#define SSD_NUM8	(u8)8
#define SSD_NUM9	(u8)9




/* Description: initialization of the SSD Component */
void SSD_VidInit(void);

/*Description: This Function used To Display 				*/
/* 				Specific Pattern On the SSD					*/
/* 	RANGE : 	1 -----> 9									*/
/* INPUTS: -The Number Of the SSD							*/
/* 		   -The Required Display number 					*/
/* OUTPUTS: The Error STATE									*/
u8 SSD_u8Display(u8 Copy_u8SSD_NUM,u8 Copy_u8DisplayPattern);


/* Description: This Function Implemented To Set The SSD ON */
/* INPUTS: The Number Of the SSD 							*/
/* OUTPUTS: The Error STATE									*/
u8 SSD_u8On(u8 Copy_u8SSD_NUM);

/* Description: This Function Implemented To Set The SSD OFF */
/* INPUTS: The Number Of the SSD 							 */
/* OUTPUTS: The Error STATE									 */

u8 SSD_u8Off(u8 Copy_u8SSD_NUM);



#endif /* SEVENSEGMENT_INTERFACE_H_ */
