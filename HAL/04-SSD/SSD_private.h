/*
 * SevenSegment_private.h
 *
 *  Created on: Feb 1, 2019
 *      Author: EsSss
 */

#ifndef SEVENSEGMENT_PRIVATE_H_
#define SEVENSEGMENT_PRIVATE_H_



/********************************************************/
/* Author: ISLAM GMAL ABDEL NASSER						*/
/* DATE: 31 JAN 2019									*/
/* VERSION : V1.0										*/
/* LAYER : HAL											*/
/********************************************************/
/* DESCRIPTION: 										*/
/*This file is used to Define Some Private Macros		*/
/********************************************************/




/* Description: The Defined Types Of SEven Segments
 * Range :		- Common Cathode
 * 				- Common Anode
 */
#define SSD_TYPE_COMMON_CATHODE			(u8)0
#define SSD_TYPE_COMMON_ANODE			(u8)1


/* Description: Defining The Corresponding Values for SSD Types
 * RANGE: 	Common Ground,
 * 			Common Source
 */
#define SEV_SEG_COMMON_GROUND			(u8)0
#define SEV_SEG_COMMON_SOURCE			(u8)1

/******************************************************************************/



#define INIT		0
#endif /* SEVENSEGMENT_PRIVATE_H_ */
