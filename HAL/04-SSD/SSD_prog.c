/*
 * SevenSegment_prog.c
 *
 *  Created on: Feb 1, 2019
 *      Author: EsSss
 */



/********************************************************/
/* Author: ISLAM GMAL ABDEL NASSER						*/
/* DATE: 31 JAN 2019									*/
/* VERSION : V1.0										*/
/* LAYER : HAL											*/
/********************************************************/
/* DESCRIPTION: 										*/
/*This file is used to Explain the implementation 		*/
/* of the SSD APIs 										*/
/********************************************************/

/* INCLUDING THE LIBRARIES NEEDED   */
#include "STD_TYPES.h"
#include "BIT_CALC.h"
/* INCLUDING THE LOWER LAYER NEEDED	*/
#include "DIO_interface.h"
/* INCLUDING THE OWNER FILES        */
#include "SevenSegment_interface.h"
#include "SevenSegment_private.h"
#include "SevenSegment_config.h"





/* Description: initialization of the SSD Component */
void SSD_VidInit(void)
{
	/* DO NOTHING */
}
/*****************************************************/


/*************************************************************/
/* Description: This Function Implemented To Set The SSD ON */
/* INPUTS: The Number Of the SSD 							*/
/* OUTPUTS: The Error STATE									*/
/*************************************************************/
u8 SSD_u8On(u8 Copy_u8SSD_NUM)
{	/* Local Variable Definition : 	*/
	/*			- Error State       */
	u8 Locale_u8ErrorState;

	/* Error State Validation	*/
	if ( Copy_u8SSD_NUM >= SSD_MAX_CONNECTED_NUM )
	{
		Locale_u8ErrorState= ERROR_NOK;
	}
	else {
		/* Validate The Type Of The Desired SSD */
		switch (SSD_Au8ConnectedSSDTypes[Copy_u8SSD_NUM])
		{
		case SSD_TYPE_COMMON_CATHODE :	DIO_u8SetPinValue(SSD_Au8EnablePins[Copy_u8SSD_NUM],DIO_U8_PIN_LOW);
										break;
		case SSD_TYPE_COMMON_ANODE   :	DIO_u8SetPinValue(SSD_Au8EnablePins[Copy_u8SSD_NUM],DIO_U8_PIN_HIGH);
										break;
		}
		Locale_u8ErrorState = ERROR_OK;
	}

	/* Return Error State: */
	return Locale_u8ErrorState;
}


/*************************************************************/
/* Description: This Function Implemented To Set The SSD OFF */
/* INPUTS: The Number Of the SSD 							 */
/* OUTPUTS: The Error STATE									 */
/*************************************************************/
u8 SSD_u8Off(u8 Copy_u8SSD_NUM)
{
	/* Local Variable Definition : 	*/
	/*			- Error State       */
		u8 Locale_u8ErrorState;
		/* Error State Validation	*/
		if ( Copy_u8SSD_NUM >= SSD_MAX_CONNECTED_NUM )
		{
			Locale_u8ErrorState= ERROR_NOK;
		}
		else {
			/* Validate The Type Of The Desired SSD */
			switch (SSD_Au8ConnectedSSDTypes[Copy_u8SSD_NUM])
			{
			case SSD_TYPE_COMMON_CATHODE :	DIO_u8SetPinValue(SSD_Au8EnablePins[Copy_u8SSD_NUM],DIO_U8_PIN_HIGH);
											break;
			case SSD_TYPE_COMMON_ANODE   :	DIO_u8SetPinValue(SSD_Au8EnablePins[Copy_u8SSD_NUM],DIO_U8_PIN_LOW);
											break;
			}
			Locale_u8ErrorState = ERROR_OK;
		}
		/* Return Error State: */
		return Locale_u8ErrorState;
}




/*************************************************************/
/* Description: Implementing the way of Displaying Patterns  */
/* 				on more than one seven segment				 */
/* 	RANGE : 	1 -----> 9									 */
/*************************************************************/
u8 SSD_u8Display(u8 Copy_u8SSD_NUM,u8 Copy_u8DisplayPattern)
{
	/* Local Variable Definition : 	*/
	/*			- Error State       */
	/*          - PinNum			*/

	u8 Locale_u8ErrorState;
	u8 Locale_u8PinNum;

	/* Validation TEST of Correct Parameters: */
	if (
			(Copy_u8DisplayPattern >= SEV_SEG_MAX_DISPLAY_PATTERN)
			||
			(Copy_u8SSD_NUM >= SSD_MAX_CONNECTED_NUM)
		)
		{	Locale_u8ErrorState = ERROR_NOK; }

	/* Code Implementation In the Case Of Valid Inputs */
	else {
		/* Configure Specific SSD with Reference to Desired one by the USER */
		switch( Copy_u8SSD_NUM )
		{
			/* The First SSD Configuration To Display Number */
			case SSD0: 	if (SSD_Au8ConnectedSSDTypes[SSD0] == SSD_TYPE_COMMON_CATHODE)
						{	for (Locale_u8PinNum=INIT; Locale_u8PinNum < SSD_PINS; Locale_u8PinNum++)
							{	DIO_u8SetPinValue(SSD_Au8ConnectedPins[Locale_u8PinNum],
											  SSD_u8Displayed_Cathode_Values[Copy_u8DisplayPattern][Locale_u8PinNum]);
							}
						}
						else if (SSD_Au8ConnectedSSDTypes[SSD0] == SSD_TYPE_COMMON_ANODE){
							for (Locale_u8PinNum=INIT; Locale_u8PinNum < SSD_PINS; Locale_u8PinNum++)
								{	DIO_u8SetPinValue(SSD_Au8ConnectedPins[Locale_u8PinNum],
									  SSD_u8Displayed_Anode_Values[Copy_u8DisplayPattern][Locale_u8PinNum]);
								}
						}
						else {/* DO NOTHING */ }
						break;

			/* The Second SSD Configuration To Display Number */
			case SSD1: 	if (SSD_Au8ConnectedSSDTypes[SSD1] == SSD_TYPE_COMMON_CATHODE)
						{	for (Locale_u8PinNum=INIT; Locale_u8PinNum < SSD_PINS; Locale_u8PinNum++)
							{	DIO_u8SetPinValue(SSD_Au8ConnectedPins[Locale_u8PinNum],
								  SSD_u8Displayed_Cathode_Values[Copy_u8DisplayPattern][Locale_u8PinNum]);
							}
						}
						else if (SSD_Au8ConnectedSSDTypes[SSD1] == SSD_TYPE_COMMON_ANODE){
							for (Locale_u8PinNum=INIT; Locale_u8PinNum < SSD_PINS; Locale_u8PinNum++)
							{	DIO_u8SetPinValue(SSD_Au8ConnectedPins[Locale_u8PinNum],
												  SSD_u8Displayed_Anode_Values[Copy_u8DisplayPattern][Locale_u8PinNum]);
							}
						}
						else {/* DO NOTHING */ }
						break;

			/* The Third SSD Configuration To Display Number */
			case SSD2: 	if (SSD_Au8ConnectedSSDTypes[SSD2] == SSD_TYPE_COMMON_CATHODE)
						{	for (Locale_u8PinNum=INIT; Locale_u8PinNum < SSD_PINS; Locale_u8PinNum++)
							{	DIO_u8SetPinValue(SSD_Au8ConnectedPins[Locale_u8PinNum],
								  SSD_u8Displayed_Cathode_Values[Copy_u8DisplayPattern][Locale_u8PinNum]);
							}
						}
						else if (SSD_Au8ConnectedSSDTypes[SSD2] == SSD_TYPE_COMMON_ANODE){
							for (Locale_u8PinNum=INIT; Locale_u8PinNum < SSD_PINS; Locale_u8PinNum++)
							{	DIO_u8SetPinValue(SSD_Au8ConnectedPins[Locale_u8PinNum],
												  SSD_u8Displayed_Anode_Values[Copy_u8DisplayPattern][Locale_u8PinNum]);
							}
						}
						else {/* DO NOTHING */ }
						break;

			/* The Fourth SSD Configuration To Display Number */
			case SSD3:	if (SSD_Au8ConnectedSSDTypes[SSD3] == SSD_TYPE_COMMON_CATHODE)
						{	for (Locale_u8PinNum=INIT; Locale_u8PinNum < SSD_PINS; Locale_u8PinNum++)
							{	DIO_u8SetPinValue(SSD_Au8ConnectedPins[Locale_u8PinNum],
									  SSD_u8Displayed_Cathode_Values[Copy_u8DisplayPattern][Locale_u8PinNum]);
							}
						}
						else if (SSD_Au8ConnectedSSDTypes[SSD3] == SSD_TYPE_COMMON_ANODE){
							for (Locale_u8PinNum=INIT; Locale_u8PinNum < SSD_PINS; Locale_u8PinNum++)
							{	DIO_u8SetPinValue(SSD_Au8ConnectedPins[Locale_u8PinNum],
												  SSD_u8Displayed_Anode_Values[Copy_u8DisplayPattern][Locale_u8PinNum]);
							}
						}
						else {/* DO NOTHING */ }
						break;

			default: 	/* DO NOTHING */
						break;
		}

		/* Error STATE Is OKAY */
		Locale_u8ErrorState = ERROR_OK;
	}
	/* Return The ERROR STATE VALUE */
	return Locale_u8ErrorState;
}


