/*
 * SevenSegment_config.h
 *
 *  Created on: Feb 1, 2019
 *      Author: EsSss
 */

#ifndef SEVENSEGMENT_CONFIG_H_
#define SEVENSEGMENT_CONFIG_H_


/********************************************************/
/* Author: ISLAM GMAL ABDEL NASSER						*/
/* DATE: 31 JAN 2019									*/
/* VERSION : V1.0										*/
/* LAYER : HAL											*/
/************************************************************************************/
/* DESCRIPTION: 																	*/
/*This file is used to Configure The Seven Segment 									*/
/* Driver.																			*/
/* CONFIGURATION STEPS: 															*/
/*	Step 1: Setup The Number Of Connected SSDs  (SSD_MAX_CONNECTED_NUM)				*/
/*  Step 2: Configure The Associated Enable PINS of You SSD (Enable_SSD)			*/
/*  Step 3: Connect You SSD Pins To DIO PINS  (SSD_CHA,B,C,.....)					*/
/* 	Step 4: Edit The Enable Array by the Updated Pins (SSD_Au8EnablePins)	     	*/
/* 	step 5: Edit The Array of Connected Channels of YOur SSD (SSD_Au8ConnectedPins)	*/
/* 	Step 6: Update The Array Of SSD TYPES (SSD_Au8ConnectedSSDTypes) 				*/
/************************************************************************************/


/* Description :
 * 		Maximum Number of Connected Seven Segments
 * 		RANGE: 0 ---> 3 ( SSD0,SSD1,SSD2,SSD3)
 */
#define SSD_MAX_CONNECTED_NUM 	4

#define  SSD_7PINS 	(u8)8

/* Defining The Enables Of the Connected SSDs */
#define Enable_SSD1		DIO_PIN16
#define Enable_SSD2		DIO_PIN17
#define Enable_SSD3		DIO_PIN18
#define Enable_SSD4		DIO_PIN19


/* Description: Seven Segment Channels */
#define SSD_CHA		DIO_PIN24
#define SSD_CHB		DIO_PIN25
#define SSD_CHC		DIO_PIN26
#define SSD_CHD		DIO_PIN27
#define SSD_CHE		DIO_PIN28
#define SSD_CHF		DIO_PIN29
#define SSD_CHG		DIO_PIN30
#define SSD_CHDOT	DIO_PIN31


#define SSD_TYPE1	SSD_TYPE_COMMON_CATHODE
#define SSD_TYPE2	SSD_TYPE_COMMON_CATHODE
#define SSD_TYPE3	SSD_TYPE_COMMON_CATHODE
#define SSD_TYPE4	SSD_TYPE_COMMON_CATHODE



/********************************************************/
/********************************************************/

/* ARRAY OF SSD CATHODE VALUES THAT CAN BE DISPLAYED */
/* Range : 1--->9 							 */
u8 SSD_u8Displayed_Cathode_Values[SEV_SEG_MAX_DISPLAY_PATTERN][SSD_7PINS] = {
		SEV_SEG_CATH_NUM0,
		SEV_SEG_CATH_NUM1,
		SEV_SEG_CATH_NUM2,
		SEV_SEG_CATH_NUM3,
		SEV_SEG_CATH_NUM4,
		SEV_SEG_CATH_NUM5,
		SEV_SEG_CATH_NUM6,
		SEV_SEG_CATH_NUM7,
		SEV_SEG_CATH_NUM8,
		SEV_SEG_CATH_NUM9
};


/* ARRAY OF SSD ANODE VALUES THAT CAN BE DISPLAYED */
/* Range : 1--->9 							 */
u8 SSD_u8Displayed_Anode_Values[SEV_SEG_MAX_DISPLAY_PATTERN][SSD_7PINS] = {
		SEV_SEG_ANODE_NUM0,
		SEV_SEG_ANODE_NUM1,
		SEV_SEG_ANODE_NUM2,
		SEV_SEG_ANODE_NUM3,
		SEV_SEG_ANODE_NUM4,
		SEV_SEG_ANODE_NUM5,
		SEV_SEG_ANODE_NUM6,
		SEV_SEG_ANODE_NUM7,
		SEV_SEG_ANODE_NUM8,
		SEV_SEG_ANODE_NUM9
};
/********************************************************/
/********************************************************/


/* Initialization Of SEVEN SEGMENT    */
u8 SSD_Au8ConnectedPins[SSD_7PINS]={
				SSD_CHA,
				SSD_CHB,
				SSD_CHC,
				SSD_CHD,
				SSD_CHE,
				SSD_CHF,
				SSD_CHG,
				SSD_CHDOT
							};


/* Description: Initializing An Array of the Connected SevenSegments'Enables */
u8 SSD_Au8EnablePins[SSD_MAX_CONNECTED_NUM] = {	Enable_SSD1,
												Enable_SSD2,
												Enable_SSD3,
												Enable_SSD4
											   };

/******************************************************************************/
/* Initialization Of Seven Segments Types */
u8 SSD_Au8ConnectedSSDTypes[SSD_MAX_CONNECTED_NUM] = {  SSD_TYPE1,
														SSD_TYPE2,
														SSD_TYPE3,
														SSD_TYPE4
													 };
/******************************************************************************/

#endif /* SEVENSEGMENT_CONFIG_H_ */
