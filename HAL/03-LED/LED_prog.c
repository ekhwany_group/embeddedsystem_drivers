/*
 * LED_prog.c
 *
 *  Created on: Jan 29, 2019
 *      Author: EsSss
 */



/********************************************************/
/* Author: ISLAM GMAL ABDEL NASSER						*/
/* DATE: 31 JAN 2019									*/
/* VERSION : V1.0										*/
/********************************************************/
/* DESCRIPTION: 										*/
/*This file is used to Contain the implementation 		*/
/* of the SWC APIs 										*/
/********************************************************/

/* INCLUDING THE LIBRARIES NEEDED   */
#include "STD_TYPES.h"
#include "BIT_CALC.h"
/* INCLUDING THE LOWER LAYER NEEDED	*/
#include "DIO_private.h"
#include "DIO_interface.h"
#include "DIO_config.h"
/* INCLUDING THE OWNER FILES        */
#include "LED_interface.h"
#include "LED_config.h"
#include "LED_private.h"



u8 LED_Au8LedCH[LED_u8_MAXNUM_OF_LEDS]={
		LED_Au8LedCH0,
		LED_Au8LedCH1,
		LED_Au8LedCH2,
		LED_Au8LedCH3,
		LED_Au8LedCH4
};


u8 LED_Au8LedType[LED_u8_MAXNUM_OF_LEDS]={
		Au8Led0Type,
		Au8Led1Type,
		Au8Led2Type,
		Au8Led3Type,
		Au8Led4Type
};




/********************************************************/
/* DESCRIPTION: 										*/
/*  This file is used to Initialize the Led Component	*/
/********************************************************/
void LED_VidLedInit(void)
{
	u8 LEDS_u8_Nb;

	/* Initialization Loop: That Initialize Each LED PIN as OUTPUT PIN */
	for (LEDS_u8_Nb=LED_u8_INIT; LEDS_u8_Nb<LED_u8_MAXNUM_OF_LEDS; LEDS_u8_Nb++ )
		{DIO_u8SetPinDirection(LED_Au8LedCH[LEDS_u8_Nb],DIO_U8_PIN_OUTPUT);}

	/* Return Value Of The API */
	return ;
}



/********************************************************/
/* DESCRIPTION: 										*/
/*  This file is used Configure Any LED ON				*/
/********************************************************/
u8 LED_u8LedOn(u8 Copy_u8_LEDNb)
{	/* Local Variables Definition */
	u8 LEDS_u8_Nb,u8ErrorState;

	/* Error State Validation */
	if (Copy_u8_LEDNb >= DIO_MAXNB)
		{u8ErrorState = ERROR_NOK;
		}

	else {
		/* SeTTing The Values Of The LED ARRAY ON */
		for (LEDS_u8_Nb=LED_u8_INIT; LEDS_u8_Nb<LED_u8_MAXNUM_OF_LEDS; LEDS_u8_Nb++ )
		{
			/* Validation for The Type Of The LED */
			if	(LED_Au8LedType[LEDS_u8_Nb]==LED_Forward)
				{DIO_u8SetPinValue(LED_Au8LedCH[LEDS_u8_Nb],DIO_U8_PIN_HIGH);}
			else
			{
				DIO_u8SetPinValue(LED_Au8LedCH[LEDS_u8_Nb],DIO_U8_PIN_LOW);
			}
		}
	}
	/* Return Value of the API */
	return u8ErrorState;
}


/********************************************************/
/* DESCRIPTION: 										*/
/*  This file is used Configure Any LED OFF				*/
/********************************************************/
u8 LED_u8LedOff(u8 Copy_u8_LEDNb)
{
	/* Local Variables Definition */
		u8 LEDS_u8_Nb,u8ErrorState;

		/* Error State Validation */
		if (Copy_u8_LEDNb >= DIO_MAXNB)
			{u8ErrorState = ERROR_NOK;
			}

		else {
			/* SeTTing The Values Of The LED ARRAY ON */
			for (LEDS_u8_Nb=LED_u8_INIT; LEDS_u8_Nb<LED_u8_MAXNUM_OF_LEDS; LEDS_u8_Nb++ )
			{
				/* Validation for The Type Of The LED */
				if	(LED_Au8LedType[LEDS_u8_Nb]==LED_Forward)
					{DIO_u8SetPinValue(LED_Au8LedCH[LEDS_u8_Nb],DIO_U8_PIN_LOW);}
				else
				{
					DIO_u8SetPinValue(LED_Au8LedCH[LEDS_u8_Nb],DIO_U8_PIN_HIGH);
				}
			}
		}
		/* Return Value of the API */
		return u8ErrorState;


}
