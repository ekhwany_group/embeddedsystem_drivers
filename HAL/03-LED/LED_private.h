/*
 * LED_private.h
 *
 *  Created on: Feb 5, 2019
 *      Author: EsSss
 */

#ifndef LED_PRIVATE_H_
#define LED_PRIVATE_H_

/********************************************************/
/* Author: ISLAM GMAL ABDEL NASSER						*/
/* DATE: 31 JAN 2019									*/
/* VERSION : V1.0										*/
/********************************************************/
/* DESCRIPTION: 										*/
/*This file is used to Contain the implementation 		*/
/* of the SWC APIs 										*/
/********************************************************/

#define   LED_Forward	1

#define   LED_Reversed	0


#endif /* LED_PRIVATE_H_ */
