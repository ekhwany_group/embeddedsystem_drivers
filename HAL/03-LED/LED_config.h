/*
 * LED_config.h
 *
 *  Created on: Feb 5, 2019
 *      Author: EsSss
 */


#ifndef LED_CONFIG_H_
#define LED_CONFIG_H_


/********************************************************/
/* Author: ISLAM GMAL ABDEL NASSER						*/
/* DATE: 31 JAN 2019									*/
/* VERSION : V1.0										*/
/********************************************************/
/* DESCRIPTION: 										*/
/*This file is used to	Configure the LED Component		*/
/********************************************************/


/* Define The Number Of Led Array */
#define LED_u8_MAXNUM_OF_LEDS  (u8)5



/* Define The Channels Of Your LEDs */
#define		LED_Au8LedCH0		DIO_PIN0
#define		LED_Au8LedCH1		DIO_PIN1
#define		LED_Au8LedCH2		DIO_PIN2
#define		LED_Au8LedCH3		DIO_PIN3
#define		LED_Au8LedCH4		DIO_PIN4


/* Define The Type Of Your LEDs */
/* Range: LED_Forward           */
/*        LED_Reversed          */
#define	Au8Led0Type	 	 LED_Forward
#define	Au8Led1Type	 	 LED_Forward
#define	Au8Led2Type	 	 LED_Reversed
#define	Au8Led3Type	 	 LED_Reversed
#define	Au8Led4Type	 	 LED_Reversed



//definition
#endif /* LED_CONFIG_H_ */
