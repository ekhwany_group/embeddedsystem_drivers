/*
 * LMD_private.h
 *
 *  Created on: Feb 11, 2019
 *      Author: EsSss
 */

#ifndef LMD_PRIVATE_H_
#define LMD_PRIVATE_H_


/********************************************************/
/* Author: ISLAM GMAL ABDEL NASSER						*/
/* DATE: 11 Feb 2019									*/
/* VERSION : V1.0										*/
/* LAYER   : HAL 										*/
/********************************************************/
/* DESCRIPTION: 										*/
/*This file is used to Store The Private Attributes		*/
/* 	Of LMD Component			 						*/
/********************************************************/

#define INIT 				(u8)0

/* This Private Function Implemented To set The LMD Pins */
/* By Corresponding Data 								 */
 static u8	u8SetDataPins(u8 Copy_u8RowsNumber, u8 *u8Image, u8 Copy_u8Color);

#endif /* LMD_PRIVATE_H_ */
