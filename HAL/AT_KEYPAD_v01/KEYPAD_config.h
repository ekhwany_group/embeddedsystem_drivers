/*
 * KEYPAD_config.h
 *
 *  Created on: Feb 15, 2019
 *      Author: Ahmed Tarek
 */

#ifdef KEYPAD_PRIVATE_H_
#ifndef KEYPAD_CONFIG_PRIVATE_H_
#define KEYPAD_CONFIG_PRIVATE_H_


#define MAX_NUM_OF_COLS (u8)4
#define MAX_NUM_OF_ROWS (u8)4

#define PULL_UP			   	   1


u8 KEYPAD_TYPE[4] = {PULL_UP,PULL_UP,PULL_UP,PULL_UP};

/*KEYPAD_Au8ArrayOfRowsPins : this array define the pins which will connect with the rows */
/*this must configure as input pins and its value must be high by default */
u8 KEYPAD_Au8ArrayOfRowPins [MAX_NUM_OF_ROWS]=
											{
													DIO_PIN0,
													DIO_PIN1,
													DIO_PIN2,
													DIO_PIN3

											};

/*KEYPAD_Au8ArrayOfColPins : this array define the pins which will connect with the cols */
/*this must configure as output pins and its value must be high by default */
u8 KEYPAD_Au8ArrayOfColPins [MAX_NUM_OF_COLS]=
											{
													DIO_PIN4,
													DIO_PIN5,
													DIO_PIN6,
													DIO_PIN7

											};



#endif /* KEYPAD_CONFIG_PRIVATE_H_ */
#endif /* KEYPAD_PRIVATE_H_ */




#ifdef KEYPAD_INTERFACE_H_
#ifndef KEYPAD_CONFIG_INTERFACE_H_
#define KEYPAD_CONFIG_INTERFACE_H_





#endif /* KEYPAD_CONFIG_INTERFACE_H_ */
#endif /* KEYPAD_INTERFACE_H_ */
