/*
 * KEYPAD_private.h
 *
 *  Created on: Feb 15, 2019
 *      Author: Ahmed Tarek
 */

#ifndef KEYPAD_PRIVATE_H_
#define KEYPAD_PRIVATE_H_
#include "KEYPAD_config.h"




#define KEY_ACTIVE_COL_LOW 	   (u8)0
#define KEY_ACTIVE_COL_HIGH    (u8)1
#define FIRST_COL			   (u8)0
#define FIRST_ROW			   (u8)0

#define PULL_DOWN			   0

#define KEY_VALUE_HIGH 		   (u8)1
#define KEY_VALUE_LOW		   (u8)0


#endif /* KEYPAD_PRIVATE_H_ */
