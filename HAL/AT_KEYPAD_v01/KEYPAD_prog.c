/*
 * KEYPAD_prog.c
 *
 *  Created on: Feb 15, 2019
 *      Author: Ahmed Tarek
 */
/*********************************KEYPAD_prog.C****************************/
/* Author  : Ahmed Tarek                                                  */
/* Date    : 15 feb 2019                                                  */
/* Version : v01                                                          */
/**************************************************************************/
/*Description															  */
/*-----------															  */
/*KEYPAD_prog.C is a file from KEYPAD Deriver include the definition      */
/* of function which will be used to get pressed keys				      */
/**************************************************************************/

#include "STD_TYPES.h"
#include "BIT_CALC.h"


#include "DIO_interface.h"


#include "KEYPAD_interface.h"
#include "KEYPAD_private.h"






/********************************************KEYPAD_u8GetPressedKeys*****************************/

/*Description: used to set motor speed and direction
 * Inputs :u8* Copy_Au8KeysStatues :pointer to array which include  the kyes of kyepad
 * 			Range				   : form 0 to 15
 * Outputs: Error state
 *
 * */

u8 KETPAD_u8GetPressedKeys(u8* Copy_Au8KeysStatues )
{


	u8 Local_u8IndexOfActiveCol , Local_u8IndexOfActiveRow ;

	/*Local_u8KeyStatue : local variable save in it the value of the key which get it from GetPinValue*/
	u8 Local_u8KeyStatue;

	/*Local_u8OpreationStatue :Local variables is used to check the error */
	u8 Local_u8OpreationStatue = STD_u8_OK;
	if(	Copy_Au8KeysStatues == NULL	)
	{
		Local_u8OpreationStatue = STD_u8_ERROR;
	}
	else
	{
		/*this for loop loops on colons set value for one of them to zero then set it one again */
		for(    Local_u8IndexOfActiveCol = FIRST_COL ;
				Local_u8IndexOfActiveCol < MAX_NUM_OF_COLS ;
				Local_u8IndexOfActiveCol ++
		)
		{
			/*set colon to zero to active it */
			DIO_u8SetPinValue(KEYPAD_Au8ArrayOfColPins[Local_u8IndexOfActiveCol],KEY_ACTIVE_COL_LOW);

			/*this for loop loops on rows get the value of the row to know which key is pressed  */
			for(    Local_u8IndexOfActiveRow = FIRST_ROW ;
					Local_u8IndexOfActiveRow < MAX_NUM_OF_ROWS ;
					Local_u8IndexOfActiveRow ++
			)
			{
				/*DIO_u8GetPinValue : is used to get the value of the row which connect to this pin */
				DIO_u8GetPinValue(KEYPAD_Au8ArrayOfRowPins[Local_u8IndexOfActiveRow],&Local_u8KeyStatue);

				switch(KEYPAD_TYPE[Local_u8IndexOfActiveRow])
				{
				/*in case of your keypad is pull up*/
				case PULL_UP:
					if (Local_u8KeyStatue == KEY_VALUE_LOW)
					{
						Copy_Au8KeysStatues [(Local_u8IndexOfActiveCol*MAX_NUM_OF_ROWS) +Local_u8IndexOfActiveRow] = KEY_VALUE_HIGH;

					}
					else
					{
						Copy_Au8KeysStatues [(Local_u8IndexOfActiveCol*MAX_NUM_OF_ROWS) +Local_u8IndexOfActiveRow] = KEY_VALUE_LOW;
					}
					break;

					/*in case of your keypad is pull down*/

				case PULL_DOWN:
					if (Local_u8KeyStatue == KEY_VALUE_HIGH)
					{
						(Copy_Au8KeysStatues [(Local_u8IndexOfActiveCol*MAX_NUM_OF_ROWS) +Local_u8IndexOfActiveRow]) = KEY_VALUE_HIGH;

					}
					else
					{
						(Copy_Au8KeysStatues [(Local_u8IndexOfActiveCol*MAX_NUM_OF_ROWS) +Local_u8IndexOfActiveRow]) = KEY_VALUE_LOW;
					}
					break;
				}

			}


			/*set column to one to deactive it */
			DIO_u8SetPinValue(KEYPAD_Au8ArrayOfColPins[Local_u8IndexOfActiveCol],KEY_ACTIVE_COL_HIGH);
		}

	}




	return Local_u8OpreationStatue;

}

