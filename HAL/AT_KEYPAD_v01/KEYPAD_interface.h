
/****************************KEYPAD_interface.h****************************/
/* Author  : Ahmed Tarek                                                  */
/* Date    : 15 feb 2019                                                  */
/* Version : v01                                                          */
/**************************************************************************/
/*Description															  */
/*-----------															  */
/*KEYPAD_interface.h is a file from KEYPAD Deriver include the declaration*/
/* of function which will be used to get pressed keys				      */
/**************************************************************************/

#ifndef KEYPAD_INTERFACE_H_
#define KEYPAD_INTERFACE_H_

#include "KEYPAD_config.h"

/********************************************KEYPAD_u8GetPressedKeys*****************************/

/*Description: used to set motor speed and direction
 * Inputs :u8* Copy_Au8KeysStatues :pointer to array which include  the kyes of kyepad
 * 			Range				   : form 0 to 15
 * Outputs: Error state
 *
 * */

u8 KETPAD_u8GetPressedKeys(u8* Copy_Au8KeysStatues );


#endif /* KEYPAD_INTERFACE_H_ */
