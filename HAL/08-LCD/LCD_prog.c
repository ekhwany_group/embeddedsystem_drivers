/*
 * LCD_prog.c
 *
 *  Created on: Mar 4, 2019
 *      Author: EsSss
 */

#include "STD_TYPES.h"
#include "BIT_CALC.h"
#include "SYSTICK_interface.h"

#include "DIO_interface.h"
#include "LCD_interface.h"
#include "LCD_config.h"
#include "LCD_private.h"


/******************************************************/
/* API: LCD_Initialization 							  */
/* Description : Setup The LCD Module By Sending Some */
/* 				 Fixed Commands. 					  */
/******************************************************/
/* Inputs : No Inputs 								  */
/* Outputs: No Outputs 								  */
/******************************************************/
void LCD_Initialization(void){
	SysTick_Wait10ms(3);
	LCD_u8WriteCommand(LCD_SET_COMMAND);
	SysTick_Wait10us(4);
	LCD_u8WriteCommand(LCD_DISPLAY_ON_OFF);
	SysTick_Wait10us(4);
	LCD_u8WriteCommand(LCD_DISPLAY_CLEAR);
	SysTick_Wait10us(160);
	return;
}

/******************************************************/
/* API: LCD_u8WriteData 							  */
/* Description : Write Data On The LCD				  */
/******************************************************/
/* Inputs : Data To Display 						  */
/* Outputs: Error State								  */
/******************************************************/
static void LCD_u8WriteData(u8 Copy_u8Data){

	DIO_u8SetPinValue(LCD_U8_RS,LCD_RS_DATA);
	PrivWrite((Register)Copy_u8Data);
	return;
}


/******************************************************/
/* API: LCD_u8WriteCommand 							  */
/* Description : Sending commands To LCD Controller   */
/******************************************************/
/* Inputs : Desired Command							  */
/* Outputs: Error State								  */
/******************************************************/
static void LCD_u8WriteCommand( u8 Copy_u8Command){

	DIO_u8SetPinValue(LCD_U8_RS,LCD_RS_COMMAND);
	PrivWrite((Register)Copy_u8Command);
	return;
}


u8 LCD_voidWriteStr(u8 *Copy_u8Data, u8 copy_u8XPos, u8 Copy_u8YPos){
	u8 locale_u8Result= ERROR_OK;
	u8 Locale_u8Addresss;
	if(	(copy_u8XPos < 16) && (	Copy_u8YPos < 2)	){

		Locale_u8Addresss = copy_u8XPos + (Copy_u8YPos* 0x40);
		Locale_u8Addresss |= 0x80;
		LCD_u8WriteCommand(Locale_u8Addresss);

		while(*Copy_u8Data != '\0'){

			LCD_u8WriteData(*Copy_u8Data);
			Copy_u8Data++;
			Locale_u8Addresss++;
			/* End Of First Line Validation */
			if(Locale_u8Addresss == 0x90){
				/* Return To Address of Second Row Begining */
						Locale_u8Addresss = 0xc0;
						LCD_u8WriteCommand(Locale_u8Addresss);
			}
			else if (Locale_u8Addresss == 0xD0){
				Locale_u8Addresss = 0x80;
				LCD_u8WriteCommand(Locale_u8Addresss);
			}
		}
	}

	else {
		locale_u8Result = ERROR_NOK;
	}

	return locale_u8Result;
}

static void PrivWrite(Register Data){

	DIO_u8SetPinValue(LCD_U8_RW,LCD_RW_WRITE);
	DIO_u8SetPinValue(LCD_U8_D0,Data.BitAccess.Bit0);
	DIO_u8SetPinValue(LCD_U8_D1,Data.BitAccess.Bit1);
	DIO_u8SetPinValue(LCD_U8_D2,Data.BitAccess.Bit2);
	DIO_u8SetPinValue(LCD_U8_D3,Data.BitAccess.Bit3);
	DIO_u8SetPinValue(LCD_U8_D4,Data.BitAccess.Bit4);
	DIO_u8SetPinValue(LCD_U8_D5,Data.BitAccess.Bit5);
	DIO_u8SetPinValue(LCD_U8_D6,Data.BitAccess.Bit6);
	DIO_u8SetPinValue(LCD_U8_D7,Data.BitAccess.Bit7);
	/* Triggered The Enable Pin to apply Changes */
	DIO_u8SetPinValue(LCD_U8_E,LCD_E_HIGH);
	SysTick_Wait10us(100);
	DIO_u8SetPinValue(LCD_U8_E,LCD_E_LOW);
}
