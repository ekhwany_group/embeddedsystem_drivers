/*
 * SWI.interface.h
 *
 *  Created on: Feb 7, 2019
 *      Author: EsSss
 */

#ifndef SWI_INTERFACE_H_
#define SWI_INTERFACE_H_


/********************************************************/
/* Author: ISLAM GMAL ABDEL NASSER						*/
/* DATE: 7 Feb 2019										*/
/* VERSION : V1.0										*/
/* COMPONENT : SWI										*/
/********************************************************/
/* DESCRIPTION: 										*/
/* 	This file is used to list the interfaces of SWI		*/
/********************************************************/



/* Description: Returning The Value of the switch pin */
u8 SWI_u8GetSwitchState(u8 SWI_u8SwitchNb, u8 * SWI_SwitchValue);





#endif /* SWI_INTERFACE_H_ */
