/*
 * SWI_config.h
 *
 *  Created on: Feb 7, 2019
 *      Author: EsSss
 */

#ifndef SWI_CONFIG_H_
#define SWI_CONFIG_H_


/********************************************************/
/* Author: ISLAM GMAL ABDEL NASSER						*/
/* DATE: 31 JAN 2019									*/
/* VERSION : V1.0										*/
/********************************************************/
/* DESCRIPTION: 										*/
/* 	This file is used to Define the Interfaces 			*/
/* 	Of The software Component							*/
/********************************************************/



/* The Maximum Number Of Available Switches */
#define SWI_MAX_SWITCHES_NUM 		(u8)3


/* SWI Structure */
typedef struct {
	u8 SWI_u8Pin;
	u8 SWI_u8Type;
}SWI_CH;


/* Initialization Of SWI Channels in the form of Structures With reference to SWI_MAX_SWITCHES_NUM */
SWI_CH  SWI_Ch0 = { DIO_PIN0, PULL_UP };
SWI_CH	SWI_Ch1 = { DIO_PIN1, PULL_UP };
SWI_CH  SWI_Ch2 = { DIO_PIN2, PULL_UP };


/* Configuration of the Array of SWI Channels and its Types */
SWI_CH SWI_AS_Switches[SWI_MAX_SWITCHES_NUM]={
		{ DIO_PIN0, PULL_UP },
		{ DIO_PIN1, PULL_UP },
		{ DIO_PIN2, PULL_UP }
};

/*SWI_CH SWI_AS_Switches[SWI_MAX_SWITCHES_NUM]={
		SWI_Ch0,
		SWI_Ch1,
		SWI_Ch2
};*/


#endif /* SWI_CONFIG_H_ */
