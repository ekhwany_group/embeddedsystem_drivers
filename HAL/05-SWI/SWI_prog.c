/*
 * SWI.prog.c
 *
 *  Created on: Feb 7, 2019
 *      Author: EsSss
 */

#ifndef SWI_PROG_C_
#define SWI_PROG_C_


/********************************************************/
/* Author: ISLAM GMAL ABDEL NASSER						*/
/* DATE: 31 JAN 2019									*/
/* VERSION : V1.0										*/
/********************************************************/
/* DESCRIPTION: 										*/
/* 	This file is used to Define the Interfaces 			*/
/* 	Of The software Component							*/
/********************************************************/



/* INCLUDING THE LIBRARIES NEEDED   */
#include "STD_TYPES.h"
#include "BIT_CALC.h"
/* INCLUDING THE LOWER LAYER NEEDED	*/
#include "DIO_interface.h"
/* INCLUDING THE OWNER FILES        */
#include "SWI_interface.h"
#include "SWI_private.h"
#include "SWI_config.h"




/* Description: Returning The Value of the switch pin */

/*  *********************************************************************************************************************************
 * Implementation: 																													*
 * 			1st step: Validate The Inputs To Determine The Error State Return Value.												*
 * 			2nd Step: Search For the input PIN Channel in the Array OF SWI Structures 												*
 * 					  Then If the Type of the Corresponding Channel in the Array equals Pull UP 									*
 * 					  parse to the Input POINTER the Desired value of PUll UP Resistance System,									*
 * 					  else IF the type was Pull Down, Then Parse to the Input POINTER <- the Desired value of PULL DOWN System. 	*
 ***********************************************************************************************************************************/
u8 SWI_u8GetSwitchState(u8 SWI_u8SwitchNb, u8 * SWI_SwitchValue)
{
	u8 	Error_State;
	u8	Locale_CH;
	u8 	Locale_Error_Validation = SWI_U8_INIT;
	u8  Locale_u8Passed_Typed;
  u8  Locale_u8SwitchState;
	/* Validation for SWI Input Channel	through Looping on the Input channel is it equal to one of the Configured SWI Channels or not
	 * 	If The Incremented VALUE equals to THe number of the MAX SWI CHANNELS */
	for (Locale_CH = SWI_U8_INIT;
		Locale_CH<SWI_MAX_SWITCHES_NUM;
		Locale_CH++)
	{
		if(	SWI_u8SwitchNb != SWI_AS_Switches[Locale_CH].SWI_u8Pin )
		{
			Locale_Error_Validation++;
		}
		else { /* Do NOTHING */ }
	}


	if (Locale_Error_Validation == SWI_MAX_SWITCHES_NUM)
		{
			Error_State = ERROR_NOK;
		}
	else
		{

		/* Search For The Corresponding Type For the required Switch Channel */
			for (   Locale_CH = SWI_U8_INIT;	Locale_CH<SWI_MAX_SWITCHES_NUM;	Locale_CH++ )	{
				if(	SWI_AS_Switches[Locale_CH].SWI_u8Pin == SWI_u8SwitchNb){
					Locale_u8Passed_Typed = SWI_AS_Switches[Locale_CH].SWI_u8Type;
				}
				else {
				/* Do NoThing */
				}
			}

			DIO_u8GetPinValue(SWI_u8SwitchNb,&Locale_u8SwitchState);
			/* *******************************************
			 * Determining The Type Of The switch :		**
			 * 	Range: PULL_UP							**
			 * 		   PULL_DOWN						**
			 * *******************************************/
			switch(Locale_u8Passed_Typed){
				case PULL_UP:	if (Locale_u8SwitchState == SWI_u8State_HIGH )
									{*(SWI_SwitchValue)=SWI_u8State_LOW; }
								else if (Locale_u8SwitchState == SWI_u8State_LOW )
									{*(SWI_SwitchValue)=SWI_u8State_HIGH; }
								else { /* Do Nothing */ }
						break;

				case PULL_DOWN:	if (Locale_u8SwitchState) == SWI_u8State_HIGH )
										{*(SWI_SwitchValue)=SWI_u8State_HIGH; }
								else if (Locale_u8SwitchState) == SWI_u8State_LOW )
										{*(SWI_SwitchValue)=SWI_u8State_LOW; }
								else {	/* Do NoThing */ }
						break;
				default:	/* Do Nothing */
					    break;
			}


		}



	/* Return Value */
	return Error_State;
}



#endif /* SWI_PROG_C_ */
