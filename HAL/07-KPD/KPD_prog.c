/*
 * KPD_prog.c
 *
 *  Created on: Feb 16, 2019
 *      Author: EsSss
 */

#ifndef KPD_PROG_C_
#define KPD_PROG_C_


/********************************************************/
/* Author: ISLAM GMAL ABDEL NASSER						*/
/* DATE: 16 JAN 2019									*/
/* VERSION : V1.0										*/
/* COMPONENT : KeyPad									*/
/********************************************************/
/* DESCRIPTION: 										*/
/* 	This file is used to Include the Implementation		*/
/* 	Of KeyPad Component APIs							*/
/********************************************************/

#include <stdio.h>

/* INCLUDING THE LIBRARIES NEEDED   */
#include "STD_TYPES.h"
#include "BIT_CALC.h"
/* INCLUDING THE LOWER LAYER NEEDED	*/
#include "DIO_interface.h"
/* INCLUDING THE OWNER FILES        */
#include "KPD_interface.h"
#include "KPD_config.h"
#include "KPD_private.h"




/* Description:												*/
/* This API Is used to Return the status of the KeyPad 		*/
/* in an array that describes which button is pressed and   */
/* which one is not 										*/
/************************************************************/
u8 KPD_Au8GetState(u8   Copy_Au8KeyPadStatus[4][4]){


	/* Local Variables Definition   */
	/* - Error State 				*/
	/* - Column Index				*/
	/* - Row Index 					*/
	u8 Locale_ErrorState;
	u8 Locale_ColumnIndex;
	u8 Locale_RowIndex;
	u8 Locale_TempSwiState;

	/* Error Validation Case */
	if ( Copy_Au8KeyPadStatus == NULL){

		Locale_ErrorState = ERROR_NOK;
	}
	else {
		/* Error State Is Okay */
		Locale_ErrorState = ERROR_OK;
		/* Activate the Columns one by one through this loop */
		for (Locale_ColumnIndex=PRIV_INIT_INDEX;
				Locale_ColumnIndex < KPD_COLUMNS_NUM;
					Locale_ColumnIndex++){

			DIO_u8SetPinValue(KPD_Au8ConnectedPins[PRIV_COL_INDEX][Locale_ColumnIndex] , PRIV_COL_ACTIVE);

			/* check on the state of The row key if its switched on or off */
			for (Locale_RowIndex=PRIV_INIT_INDEX;
							Locale_RowIndex < KPD_ROWS_NUM;
								Locale_RowIndex++){

				DIO_u8GetPinValue( KPD_Au8ConnectedPins[PRIV_ROW_INDEX][Locale_RowIndex],&Locale_TempSwiState);

				switch (KPD_Au8KPDTypes[Locale_RowIndex]){
					case DIO_PIN_DIR_INPUT_PULL_UP :
												if ( Locale_TempSwiState == KPD_PRESSED){
												/* In Case Of Switched On Key */
													Copy_Au8KeyPadStatus[Locale_ColumnIndex][Locale_RowIndex] = KPD_KeyOn;
												}
												else {
												/* In Case Of Switched OFF Key */
													(Copy_Au8KeyPadStatus[Locale_ColumnIndex][Locale_RowIndex]) = KPD_KeyOff;
												}
												break;
					case DIO_PIN_DIR_INPUT_PULL_DOWN :
												if (Locale_TempSwiState == KPD_PRESSED){
												/* In Case Of Switched On Key  */
													(Copy_Au8KeyPadStatus[Locale_ColumnIndex][Locale_RowIndex]) = KPD_KeyOff;
												}
												else {
												/* In Case Of Switched OFF Key  */
													(Copy_Au8KeyPadStatus[Locale_ColumnIndex][Locale_RowIndex]) = KPD_KeyOn;
												}
												break;
				}
			}

			DIO_u8SetPinValue(KPD_Au8ConnectedPins[PRIV_COL_INDEX][Locale_ColumnIndex] , PRIV_COL_DEACTIVE);

		}
	}
	return Locale_ErrorState;
}

#endif /* KPD_PROG_C_ */
