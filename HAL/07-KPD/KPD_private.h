/*
 * KPD_private.h
 *
 *  Created on: Feb 16, 2019
 *      Author: EsSss
 */

#ifndef KPD_PRIVATE_H_
#define KPD_PRIVATE_H_


/********************************************************/
/* Author: ISLAM GMAL ABDEL NASSER						*/
/* DATE: 16 JAN 2019									*/
/* VERSION : V1.0										*/
/* COMPONENT : KeyPad									*/
/********************************************************/
/* DESCRIPTION: 										*/
/* 	This file is used to Include The private	 		*/
/* 	parameters Of KeyPad Component						*/
/********************************************************/


/* Activation of Keypad Column */
#define PRIV_COL_ACTIVE	        (u8)0
#define PRIV_COL_DEACTIVE	    (u8)1
/* Parameters Of KetPad ( Rows, Columns ) */
#define KPD_PARAMETERS			(u8)2
/* Initialization Value of columns */
#define PRIV_COL_INDEX  		(u8)0
/* Initialization Value of ROWs */
#define PRIV_ROW_INDEX  		(u8)1
/* Value of First Index */
#define PRIV_INIT_INDEX			(u8)0
/* Value Of Pressed KeyPad  */
#define KPD_PRESSED		        (u8)0
/* Value Of Released KeyPad */
#define KPD_REALESED            (u8)1


/* Connected Pin Of KeyPad Array Implementation */
u8 KPD_Au8ConnectedPins[KPD_PARAMETERS][KPD_ROWS_NUM] = KPD_CONNECTED_PINS;

/* Connected Pin Of KeyPad Types Array Implementation */
u8 KPD_Au8KPDTypes[KPD_ROWS_NUM] = KPD_CONNECTION_TYPE;


#endif /* KPD_PRIVATE_H_ */
