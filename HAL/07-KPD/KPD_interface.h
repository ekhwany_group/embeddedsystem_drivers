/*
 * KPD_interface.h
 *
 *  Created on: Feb 16, 2019
 *      Author: EsSss
 */

#ifndef KPD_INTERFACE_H_
#define KPD_INTERFACE_H_

/********************************************************/
/* Author: ISLAM GMAL ABDEL NASSER						*/
/* DATE: 16 Feb 2019									*/
/* VERSION : V1.0										*/
/* COMPONENT : KeyPad									*/
/********************************************************/
/* DESCRIPTION: 										*/
/* 	This file is used to list the interfaces of KPD		*/
/********************************************************/


/* Indication For the Pressed Key */
#define 	KPD_KeyOn 	(u8)1

/* Indication For the Released Key */
#define     KPD_KeyOff	(u8)0


/* Description:												*/
/* This API Is used to Return the status of the KeyPad 		*/
/* in an array that describes which button is pressed and   */
/* which one is not 										*/
/************************************************************/
u8 KPD_Au8GetState(u8  Copy_Au8KeyPadStatus[4][4]);


#endif /* KPD_INTERFACE_H_ */
