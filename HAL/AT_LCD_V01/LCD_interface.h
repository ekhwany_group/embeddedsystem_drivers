/*
 * LCD_interface.h
 *
 *  Created on: Mar 4, 2019
 *      Author: EsSss
 */

#ifndef LCD_INTERFACE_H_
#define LCD_INTERFACE_H_

/* Register Selector Pin Modes */
#define LCD_RS_COMMAND			(u8)0
#define LCD_RS_DATA				(u8)1

/* R/W Pin Modes */
#define LCD_RW_WRITE			(u8)0
#define LCD_RW_READ				(u8)1

/* Values Of The Enable PIN */
#define LCD_E_HIGH				(u8)1
#define LCD_E_LOW				(u8)0

/* Maximum Value Can be Written On the LCD Controller */
#define LCD_MAX_COMMAND_DATA	(255)
#define ROW_NO_0				(u8)0
#define ROW_NO_1				(u8)1


/******************************************************/
/* API: LCD_Initialization 							  */
/* Description : Setup The LCD Module By Sending Some */
/* 				 Fixed Commands. 					  */
/******************************************************/
/* Inputs : No Inputs 								  */
/* Outputs: No Outputs 								  */
/******************************************************/
void LCD_Initialization(void);

/******************************************************/
/* API: LCD_u8WriteChar								  */
/* Description : Write Data On The LCD				  */
/******************************************************/
/* Inputs : Data To Display written char-by-char	  */
/* Outputs: Error State								  */
/******************************************************/
u8 LCD_u8WriteChar(u8 Copy_u8Data);
/******************************************************/
/* API: LCD_u8WriteCommand 							  */
/* Description : Sending commands To LCD Controller   */
/******************************************************/
/* Inputs : Desired Command							  */
/* Outputs: Error State								  */
/******************************************************/
u8 LCD_u8WriteCommand(u8 Copy_u8Command);



u8 LCD_u8WriteString( u8 *Copy_u8Data, u8 Copy_u8xPos, u8 Copy_u8yPos);


u8 LCD_u8WriteCharCGRAM(u8 *Copy_Au8CharPattern);
u8 LCD_u8DisplaySpecialChar(u8 Copy_u8CharNo, u8 Copy_u8Xpos, u8 Copy_u8Ypos);


#endif /* LCD_INTERFACE_H_ */
