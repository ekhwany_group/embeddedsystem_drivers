/*
 * LCD_prog.c
 *
 *  Created on: Mar 4, 2019
 *      Author: EsSss
 */

#include "STD_TYPES.h"
#include "BIT_CALC.h"
#include "DIO_interface.h"
#include <util\delay.h>
#include "LCD_interface.h"
#include "LCD_config.h"
#include "LCD_private.h"

#define ERROR_NOK 								(u8)1
#define ERROR_OK 								(u8)0

/******************************************************/
/* API: LCD_Initialization 							  */
/* Description : Setup The LCD Module By Sending Some */
/* 				 Fixed Commands. 					  */
/******************************************************/
/* Inputs : No Inputs 								  */
/* Outputs: No Outputs 								  */
/******************************************************/
static u8 Local_u8_LCD_MOVE =0;


static void PrivWrite(Register_8bit Copy_u8Data) {

	DIO_u8SetPinValue(LCD_U8_RW, LCD_RW_WRITE);
	DIO_u8SetPinValue(LCD_U8_D0, Copy_u8Data.BitAccess.BIT0);
	DIO_u8SetPinValue(LCD_U8_D1, Copy_u8Data.BitAccess.BIT1);
	DIO_u8SetPinValue(LCD_U8_D2, Copy_u8Data.BitAccess.BIT2);
	DIO_u8SetPinValue(LCD_U8_D3, Copy_u8Data.BitAccess.BIT3);
	DIO_u8SetPinValue(LCD_U8_D4, Copy_u8Data.BitAccess.BIT4);
	DIO_u8SetPinValue(LCD_U8_D5, Copy_u8Data.BitAccess.BIT5);
	DIO_u8SetPinValue(LCD_U8_D6, Copy_u8Data.BitAccess.BIT6);
	DIO_u8SetPinValue(LCD_U8_D7, Copy_u8Data.BitAccess.BIT7);

	/* Triggered The Enable Pin to apply Changes */
	DIO_u8SetPinValue(LCD_U8_E, LCD_E_HIGH);
	_delay_ms(1);
	DIO_u8SetPinValue(LCD_U8_E, LCD_E_LOW);
}

void LCD_Initialization(void) {
	_delay_ms(30);
	LCD_u8WriteCommand(LCD_SET_COMMAND);
	_delay_us(39);
	LCD_u8WriteCommand(LCD_DISPLAY_ON_OFF);
	_delay_us(39);
	LCD_u8WriteCommand(LCD_DISPLAY_CLEAR);
	_delay_ms(1.53);

}

/******************************************************/
/* API: LCD_u8WriteData 							  */
/* Description : Write Data On The LCD				  */
/******************************************************/
/* Inputs : Data To Display 						  */
/* Outputs: Error State								  */
/******************************************************/
u8 LCD_u8WriteChar(u8 Copy_u8Data) {

	/* locale Variables definitions */
	u8 Local_u8ErrorState;
	/* Error State Validation */
	if (Copy_u8Data > LCD_MAX_COMMAND_DATA) {
		/* Not Okay Parsed Inputs resulting An Error */
		Local_u8ErrorState = ERROR_NOK;
	} else {
		/* There Is No Error and Inputs Are Valid */
		Local_u8ErrorState = ERROR_OK;
		DIO_u8SetPinValue(LCD_U8_RS, LCD_RS_DATA);
		PrivWrite((Register_8bit) Copy_u8Data);
	}
	/* Return Value */
	return Local_u8ErrorState;
}

u8 LCD_u8WriteString(u8 *Copy_u8Data, u8 Copy_u8xPos, u8 Copy_u8yPos) {
	u8 Local_Result = ERROR_OK;
	u8 Local_u8Address;

	if ((Copy_u8xPos < 16) && (Copy_u8yPos < 2)) {
		switch(Copy_u8yPos)
		{
		case 0:
			Local_u8Address = 0x80;
			break;
		case 1:
			Local_u8Address = 0xc0;
			break;
		}


		Local_u8Address=Local_u8Address+Copy_u8xPos;
		LCD_u8WriteCommand(Local_u8Address);

		while (*Copy_u8Data != '\0') {
			LCD_u8WriteChar(*Copy_u8Data);
			Copy_u8Data++;
			Local_u8Address++;

			/* End of line 1 */
			if (Local_u8Address == 0x90) {
				Local_u8Address = 0xc0;
				LCD_u8WriteCommand(Local_u8Address);
			}

			/* End of line 2 */
			if (Local_u8Address == 0xd0) {
				Local_u8Address = 0x80;
				LCD_u8WriteCommand(Local_u8Address);
			}

		}

	}

	else {
		Local_Result = ERROR_NOK;
	}

	return Local_Result;
}

/******************************************************/
/* API: LCD_u8WriteCommand 							  */
/* Description : Sending commands To LCD Controller   */
/******************************************************/
/* Inputs : Desired Command							  */
/* Outputs: Error State								  */
/******************************************************/
u8 LCD_u8WriteCommand(u8 Copy_u8Command) {

	/* locale Variables definitions */
	u8 Locale_u8ErrorState;
	/* Error State Validation */
	if (Copy_u8Command > LCD_MAX_COMMAND_DATA) {
		/* Not Okay Error State */
		Locale_u8ErrorState = ERROR_NOK;
	} else {
		/* Error State Okay */
		Locale_u8ErrorState = ERROR_OK;
		DIO_u8SetPinValue(LCD_U8_RS, LCD_RS_COMMAND);
		PrivWrite((Register_8bit) Copy_u8Command);
	}
	/* Return Value */
	return Locale_u8ErrorState;
}

u8 LCD_u8WriteCharCGRAM(u8 *Copy_Au8CharPattern) {
	u8 Local_Error = ERROR_OK;
	/*LCD_u8_CHAR_HEIGHT : this macro is used to jump from char's location  to another in CGRAM*/
	u8 Local_u8_CGRAM = 0x40+(LCD_u8_CHAR_HEIGHT*Local_u8_LCD_MOVE);


	u8 Local_u8_count;
	LCD_u8WriteCommand(Local_u8_CGRAM);

	for (Local_u8_count = 0; Local_u8_count < 8; Local_u8_count++)
	{
		LCD_u8WriteChar(Copy_Au8CharPattern[Local_u8_count]);
	}

		Local_u8_LCD_MOVE++;
		if(Local_u8_LCD_MOVE > 7)
		{
			Local_u8_LCD_MOVE= 0;
		}


	return Local_Error;
}

u8 LCD_u8DisplaySpecialChar(u8 Copy_u8CharNo, u8 Copy_u8Xpos, u8 Copy_u8Ypos)
{
	u8 Local_Error = ERROR_OK;
	u8 Local_u8_ROW0_Begin = 0x80;
	u8 Local_u8_ROW1_Begin = 0xc0;
	u8 Temp_Location=0;
	if( (Copy_u8Xpos >15) || (Copy_u8Ypos >1))
	{
		Local_Error = ERROR_NOK;
	}

	else
	{
		switch(Copy_u8Ypos)
		{
		case ROW_NO_0:
		    LCD_u8WriteCommand((Local_u8_ROW0_Begin+Copy_u8Xpos));
			LCD_u8WriteChar(Copy_u8CharNo);
			break;

		case ROW_NO_1:

			LCD_u8WriteCommand((Local_u8_ROW1_Begin+Copy_u8Xpos));
			LCD_u8WriteChar(Copy_u8CharNo);
			break;
		}
	}

	return Local_Error;
}

