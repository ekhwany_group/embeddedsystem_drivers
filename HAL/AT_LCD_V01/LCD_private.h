/*
 * LCD_private.h
 *
 *  Created on: Mar 4, 2019
 *      Author: EsSss
 */

#ifndef LCD_PRIVATE_H_
#define LCD_PRIVATE_H_


#define LCD_u8_CHAR_HEIGHT     (u8)8

static void PrivWrite(Register_8bit  Copy_u8Data);

#endif /* LCD_PRIVATE_H_ */
