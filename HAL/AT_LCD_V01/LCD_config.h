/*
 * LCD_config.h
 *
 *  Created on: Mar 4, 2019
 *      Author: EsSss
 */

#ifndef LCD_CONFIG_H_
#define LCD_CONFIG_H_

/****************		 Initialization Configuration	***************/
/* 					Delay For 30 MS									  */
/*	SetCommand : 0011NFXX	(D0,...,D7)					 			  */
/*				N: Number Of Rows ( 0---> 1 Row, 1---> 2Rows)  		  */
/*				F: FONT Size 	  ( 0---> 8*5  , 1---> 8*11 )  		  */
/* 					Delay For 39 uS									  */
/*	Display ON/OFF : 00001DCB								   		  */
/*				D:	For Display ON/OFF (0 ---> OFF, 1---> ON ) 		  */
/*				C:	For Cursor (0--> Disabled, 1--> Enabled)   		  */
/*				B:	For Blinking Cursor (0--> Disabled, 1--> Enabled) */
/* 					Delay For 39 uS									  */
/*	Display Clear : 00000001										  */
/* 					Delay For  1.53 MS								  */
/**********************************************************************/
/* Configured Set Command For 2 Rows And FONT 8*11 	*/
#define LCD_SET_COMMAND 	(u8)0b00111000
/* Configured Display ON/OFF As ON,No Cursor, Blinking OFF */
#define LCD_DISPLAY_ON_OFF	(u8)0b00001100
/* Configured DISPLAY Clear as Clear */
#define LCD_DISPLAY_CLEAR	(u8)0b00000001

/* LCD PINS Configuration */
#define LCD_U8_RS	DIO_PIN24
#define LCD_U8_RW	DIO_PIN25
#define LCD_U8_E	DIO_PIN26
#define LCD_U8_D0	DIO_PIN0
#define LCD_U8_D1	DIO_PIN1
#define LCD_U8_D2	DIO_PIN2
#define LCD_U8_D3	DIO_PIN3
#define LCD_U8_D4	DIO_PIN4
#define LCD_U8_D5	DIO_PIN5
#define LCD_U8_D6	DIO_PIN6
#define LCD_U8_D7	DIO_PIN7

#define LCD_8BIT_MODE		(u8)0x30
#define LCD_4BIT_MODE		(u8)0x20


#endif /* LCD_CONFIG_H_ */
