/*
 * SSD_private.h
 *
 *  Created on: Feb 8, 2019
 *      Author: Ahmed Tarek
 */

#ifndef SSD_PRIVATE_H_
#define SSD_PRIVATE_H_

#define SSD_PIN__VALUE_HIGH          (u8)1
#define SSD_PIN__VALUE_LOW           (u8)0

#define SSD_COMMON_CATHODE			 (u8)0
#define SSD_COMMON_ANODE			 (u8)1

#define SSD_MAX_NUMBER				(u8)10



#define SSD_U8_NUMBER_ZERO        (u8)0
#define SSD_U8_NUMBER_ONE         (u8)1
#define SSD_U8_NUMBER_TWO         (u8)2
#define SSD_U8_NUMBER_THREE       (u8)3
#define SSD_U8_NUMBER_FOUR        (u8)4
#define SSD_U8_NUMBER_FIVE        (u8)5
#define SSD_U8_NUMBER_SIX         (u8)6
#define SSD_U8_NUMBER_SEVEN       (u8)7
#define SSD_U8_NUMBER_EIGHT       (u8)8
#define SSD_U8_NUMBER_NINE        (u8)9



#endif /* SSD_PRIVATE_H_ */
