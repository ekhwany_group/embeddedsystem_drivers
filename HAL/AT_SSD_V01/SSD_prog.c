/*
 * SSD_prog.c
 *
 *  Created on: Feb 8, 2019
 *      Author: Ahmed Tarek
 */

/***********************************SSD_interface.h********************************/
/* Author  : Ahmed Tarek                                                          */
/* Date    : 8 feb 2019                                                           */
/* Version : v01                                                                  */
/**********************************************************************************/
/*Description										          	        		  */
/*-----------										                 			  */
/*SSD_prog.c is a file from SSD Deriver include the definition of function which  */
/*use to initiate all SSD pins ,set SSD value , set SSD on and set SSD off        */
/**********************************************************************************/


#include "STD_TYPES.h"
#include "DIO_interface.h"
#include "SSD_interface.h"
#include "SSD_config.h"
#include "SSD_private.h"




/****************************************************************/

/*Description: initiate the seven segment direction
 * Inputs : void
 *
 * Outputs: void
 *
 * */

void SSD_VoidInitiatlization(void)
{
	/*local variables*************************************************/
		/* Local_u8ErrorChecker: this variable is used to check the error*/
		/*                                                                */





}


/****************************************************************/

/*Description: set seven segment value
 * Inputs : u8  Copy_u8SsdValue  : pin Number
                                  Range SSD_ZERO TO SSD_NINE
 *
 * Outputs: Error state
 *
 * */

u8 SSD_u8SetValue(u8 Copy_u8SsdValue)
{

	/*local variables */
		u8 Local_u8ErrorChecker = STD_u8_OK;

		/*this if condition to check the error if the input number excess
		 * SEVSEG_MAX_NUMBER or the type of seven segment is not in a range
		 * */
		if(
				(Copy_u8SsdValue >= SSD_MAX_NUMBER)
			)
		{

			Local_u8ErrorChecker = STD_u8_ERROR;
		}

		/*switch to choose if the seven segment is common cathode or com.Anode */
		switch(SSD_TYPE)
		{
			case SSD_COMMON_CATHODE :

		 		/*DIO_u8SetPion : set the pin which connect to the seven segment pin EN */
		 		DIO_u8SetPinValue(SSD_PIN_EN,SSD_PIN__VALUE_LOW);
			 switch(Copy_u8SsdValue)
			 {
/**************************************NUMBER_ZERO*********************************************/
			 case  SSD_U8_NUMBER_ZERO:
		       /*DIO_u8SetPortDirection : set the pin which will connect to the seven segment pin a */
		 		DIO_u8SetPinValue(SSD_PIN_A,SSD_PIN__VALUE_HIGH);

		 		/*DIO_u8SetPion : set the pin which connect to the seven segment pin b*/
		 		DIO_u8SetPinValue(SSD_PIN_B,SSD_PIN__VALUE_HIGH);

		 		/*DIO_u8SetPion : set the pin which connect to the seven segment pin c */
		 		DIO_u8SetPinValue(SSD_PIN_C,SSD_PIN__VALUE_HIGH);

		 		/*DIO_u8SetPion : set the pin which connect to the seven segment pin d */
		 		DIO_u8SetPinValue(SSD_PIN_D,SSD_PIN__VALUE_HIGH);

		 		/*DIO_u8SetPion : set the pin which connect to the seven segment pin e */
		 		DIO_u8SetPinValue(SSD_PIN_E,SSD_PIN__VALUE_HIGH);

		 		/*DIO_u8SetPion : set the pin which connect to the seven segment pin f */
		 		DIO_u8SetPinValue(SSD_PIN_F,SSD_PIN__VALUE_HIGH);

		 		/*DIO_u8SetPion : set the pin which connect to the seven segment pin g */
		 		DIO_u8SetPinValue(SSD_PIN_G,SSD_PIN__VALUE_LOW);
		 		break;
/**************************************NUMBER_ONE*********************************************/

			 case  SSD_U8_NUMBER_ONE:
		       /*DIO_u8SetPortDirection : set the pin which will connect to the seven segment pin a */
		 		DIO_u8SetPinValue(SSD_PIN_A,SSD_PIN__VALUE_LOW);

		 		/*DIO_u8SetPion : set the pin which connect to the seven segment pin b*/
		 		DIO_u8SetPinValue(SSD_PIN_B,SSD_PIN__VALUE_HIGH);

		 		/*DIO_u8SetPion : set the pin which connect to the seven segment pin c */
		 		DIO_u8SetPinValue(SSD_PIN_C,SSD_PIN__VALUE_HIGH);

		 		/*DIO_u8SetPion : set the pin which connect to the seven segment pin d */
		 		DIO_u8SetPinValue(SSD_PIN_D,SSD_PIN__VALUE_LOW);

		 		/*DIO_u8SetPion : set the pin which connect to the seven segment pin e */
		 		DIO_u8SetPinValue(SSD_PIN_E,SSD_PIN__VALUE_LOW);

		 		/*DIO_u8SetPion : set the pin which connect to the seven segment pin f */
		 		DIO_u8SetPinValue(SSD_PIN_F,SSD_PIN__VALUE_LOW);

		 		/*DIO_u8SetPion : set the pin which connect to the seven segment pin g */
		 		DIO_u8SetPinValue(SSD_PIN_G,SSD_PIN__VALUE_LOW);
		 		break;
/**************************************NUMBER_TWO*********************************************/

			 case  SSD_U8_NUMBER_TWO:
		       /*DIO_u8SetPortDirection : set the pin which will connect to the seven segment pin a */
		 		DIO_u8SetPinValue(SSD_PIN_A,SSD_PIN__VALUE_HIGH);

		 		/*DIO_u8SetPion : set the pin which connect to the seven segment pin b*/
		 		DIO_u8SetPinValue(SSD_PIN_B,SSD_PIN__VALUE_HIGH);

		 		/*DIO_u8SetPion : set the pin which connect to the seven segment pin c */
		 		DIO_u8SetPinValue(SSD_PIN_C,SSD_PIN__VALUE_LOW);

		 		/*DIO_u8SetPion : set the pin which connect to the seven segment pin d */
		 		DIO_u8SetPinValue(SSD_PIN_D,SSD_PIN__VALUE_HIGH);

		 		/*DIO_u8SetPion : set the pin which connect to the seven segment pin e */
		 		DIO_u8SetPinValue(SSD_PIN_E,SSD_PIN__VALUE_HIGH);

		 		/*DIO_u8SetPion : set the pin which connect to the seven segment pin f */
		 		DIO_u8SetPinValue(SSD_PIN_F,SSD_PIN__VALUE_LOW);

		 		/*DIO_u8SetPion : set the pin which connect to the seven segment pin g */
		 		DIO_u8SetPinValue(SSD_PIN_G,SSD_PIN__VALUE_HIGH);
		 		break;
/**************************************NUMBER_THREE*********************************************/
			 case  SSD_U8_NUMBER_THREE:
		       /*DIO_u8SetPortDirection : set the pin which will connect to the seven segment pin a */
		 		DIO_u8SetPinValue(SSD_PIN_A,SSD_PIN__VALUE_HIGH);

		 		/*DIO_u8SetPion : set the pin which connect to the seven segment pin b*/
		 		DIO_u8SetPinValue(SSD_PIN_B,SSD_PIN__VALUE_HIGH);

		 		/*DIO_u8SetPion : set the pin which connect to the seven segment pin c */
		 		DIO_u8SetPinValue(SSD_PIN_C,SSD_PIN__VALUE_HIGH);

		 		/*DIO_u8SetPion : set the pin which connect to the seven segment pin d */
		 		DIO_u8SetPinValue(SSD_PIN_D,SSD_PIN__VALUE_HIGH);

		 		/*DIO_u8SetPion : set the pin which connect to the seven segment pin e */
		 		DIO_u8SetPinValue(SSD_PIN_E,SSD_PIN__VALUE_LOW);

		 		/*DIO_u8SetPion : set the pin which connect to the seven segment pin f */
		 		DIO_u8SetPinValue(SSD_PIN_F,SSD_PIN__VALUE_LOW);

		 		/*DIO_u8SetPion : set the pin which connect to the seven segment pin g */
		 		DIO_u8SetPinValue(SSD_PIN_G,SSD_PIN__VALUE_HIGH);
		 		break;
/**************************************NUMBER_FOUR*********************************************/

			 case  SSD_U8_NUMBER_FOUR:
		       /*DIO_u8SetPortDirection : set the pin which will connect to the seven segment pin a */
		 		DIO_u8SetPinValue(SSD_PIN_A,SSD_PIN__VALUE_LOW);

		 		/*DIO_u8SetPion : set the pin which connect to the seven segment pin b*/
		 		DIO_u8SetPinValue(SSD_PIN_B,SSD_PIN__VALUE_HIGH);

		 		/*DIO_u8SetPion : set the pin which connect to the seven segment pin c */
		 		DIO_u8SetPinValue(SSD_PIN_C,SSD_PIN__VALUE_HIGH);

		 		/*DIO_u8SetPion : set the pin which connect to the seven segment pin d */
		 		DIO_u8SetPinValue(SSD_PIN_D,SSD_PIN__VALUE_LOW);

		 		/*DIO_u8SetPion : set the pin which connect to the seven segment pin e */
		 		DIO_u8SetPinValue(SSD_PIN_E,SSD_PIN__VALUE_LOW);

		 		/*DIO_u8SetPion : set the pin which connect to the seven segment pin f */
		 		DIO_u8SetPinValue(SSD_PIN_F,SSD_PIN__VALUE_HIGH);

		 		/*DIO_u8SetPion : set the pin which connect to the seven segment pin g */
		 		DIO_u8SetPinValue(SSD_PIN_G,SSD_PIN__VALUE_HIGH);
		 		break;
/**************************************NUMBER_FIVE*********************************************/

			 case  SSD_U8_NUMBER_FIVE:
		       /*DIO_u8SetPortDirection : set the pin which will connect to the seven segment pin a */
		 		DIO_u8SetPinValue(SSD_PIN_A,SSD_PIN__VALUE_HIGH);

		 		/*DIO_u8SetPion : set the pin which connect to the seven segment pin b*/
		 		DIO_u8SetPinValue(SSD_PIN_B,SSD_PIN__VALUE_LOW);

		 		/*DIO_u8SetPion : set the pin which connect to the seven segment pin c */
		 		DIO_u8SetPinValue(SSD_PIN_C,SSD_PIN__VALUE_HIGH);

		 		/*DIO_u8SetPion : set the pin which connect to the seven segment pin d */
		 		DIO_u8SetPinValue(SSD_PIN_D,SSD_PIN__VALUE_HIGH);

		 		/*DIO_u8SetPion : set the pin which connect to the seven segment pin e */
		 		DIO_u8SetPinValue(SSD_PIN_E,SSD_PIN__VALUE_LOW);

		 		/*DIO_u8SetPion : set the pin which connect to the seven segment pin f */
		 		DIO_u8SetPinValue(SSD_PIN_F,SSD_PIN__VALUE_HIGH);

		 		/*DIO_u8SetPion : set the pin which connect to the seven segment pin g */
		 		DIO_u8SetPinValue(SSD_PIN_G,SSD_PIN__VALUE_HIGH);
		 		break;
/**************************************NUMBER_SIX*********************************************/

			 case  SSD_U8_NUMBER_SIX:
		       /*DIO_u8SetPortDirection : set the pin which will connect to the seven segment pin a */
		 		DIO_u8SetPinValue(SSD_PIN_A,SSD_PIN__VALUE_HIGH);

		 		/*DIO_u8SetPion : set the pin which connect to the seven segment pin b*/
		 		DIO_u8SetPinValue(SSD_PIN_B,SSD_PIN__VALUE_LOW);

		 		/*DIO_u8SetPion : set the pin which connect to the seven segment pin c */
		 		DIO_u8SetPinValue(SSD_PIN_C,SSD_PIN__VALUE_HIGH);

		 		/*DIO_u8SetPion : set the pin which connect to the seven segment pin d */
		 		DIO_u8SetPinValue(SSD_PIN_D,SSD_PIN__VALUE_HIGH);

		 		/*DIO_u8SetPion : set the pin which connect to the seven segment pin e */
		 		DIO_u8SetPinValue(SSD_PIN_E,SSD_PIN__VALUE_HIGH);

		 		/*DIO_u8SetPion : set the pin which connect to the seven segment pin f */
		 		DIO_u8SetPinValue(SSD_PIN_F,SSD_PIN__VALUE_HIGH);

		 		/*DIO_u8SetPion : set the pin which connect to the seven segment pin g */
		 		DIO_u8SetPinValue(SSD_PIN_G,SSD_PIN__VALUE_HIGH);
		 		break;
/**************************************NUMBER_SEVEN*********************************************/

			 case  SSD_U8_NUMBER_SEVEN:
		       /*DIO_u8SetPortDirection : set the pin which will connect to the seven segment pin a */
		 		DIO_u8SetPinValue(SSD_PIN_A,SSD_PIN__VALUE_HIGH);

		 		/*DIO_u8SetPion : set the pin which connect to the seven segment pin b*/
		 		DIO_u8SetPinValue(SSD_PIN_B,SSD_PIN__VALUE_HIGH);

		 		/*DIO_u8SetPion : set the pin which connect to the seven segment pin c */
		 		DIO_u8SetPinValue(SSD_PIN_C,SSD_PIN__VALUE_HIGH);

		 		/*DIO_u8SetPion : set the pin which connect to the seven segment pin d */
		 		DIO_u8SetPinValue(SSD_PIN_D,SSD_PIN__VALUE_LOW);

		 		/*DIO_u8SetPion : set the pin which connect to the seven segment pin e */
		 		DIO_u8SetPinValue(SSD_PIN_E,SSD_PIN__VALUE_LOW);

		 		/*DIO_u8SetPion : set the pin which connect to the seven segment pin f */
		 		DIO_u8SetPinValue(SSD_PIN_F,SSD_PIN__VALUE_LOW);

		 		/*DIO_u8SetPion : set the pin which connect to the seven segment pin g */
		 		DIO_u8SetPinValue(SSD_PIN_G,SSD_PIN__VALUE_LOW);
		 		break;
/**************************************NUMBER_EIGHT*********************************************/
			 case  SSD_U8_NUMBER_EIGHT:
		       /*DIO_u8SetPortDirection : set the pin which will connect to the seven segment pin a */
		 		DIO_u8SetPinValue(SSD_PIN_A,SSD_PIN__VALUE_HIGH);

		 		/*DIO_u8SetPion : set the pin which connect to the seven segment pin b*/
		 		DIO_u8SetPinValue(SSD_PIN_B,SSD_PIN__VALUE_HIGH);

		 		/*DIO_u8SetPion : set the pin which connect to the seven segment pin c */
		 		DIO_u8SetPinValue(SSD_PIN_C,SSD_PIN__VALUE_HIGH);

		 		/*DIO_u8SetPion : set the pin which connect to the seven segment pin d */
		 		DIO_u8SetPinValue(SSD_PIN_D,SSD_PIN__VALUE_HIGH);

		 		/*DIO_u8SetPion : set the pin which connect to the seven segment pin e */
		 		DIO_u8SetPinValue(SSD_PIN_E,SSD_PIN__VALUE_HIGH);

		 		/*DIO_u8SetPion : set the pin which connect to the seven segment pin f */
		 		DIO_u8SetPinValue(SSD_PIN_F,SSD_PIN__VALUE_HIGH);

		 		/*DIO_u8SetPion : set the pin which connect to the seven segment pin g */
		 		DIO_u8SetPinValue(SSD_PIN_G,SSD_PIN__VALUE_HIGH);
		 		break;
/**************************************NUMBER_NINE*********************************************/
			 case  SSD_U8_NUMBER_NINE:
		       /*DIO_u8SetPortDirection : set the pin which will connect to the seven segment pin a */
		 		DIO_u8SetPinValue(SSD_PIN_A,SSD_PIN__VALUE_HIGH);

		 		/*DIO_u8SetPion : set the pin which connect to the seven segment pin b*/
		 		DIO_u8SetPinValue(SSD_PIN_B,SSD_PIN__VALUE_HIGH);

		 		/*DIO_u8SetPion : set the pin which connect to the seven segment pin c */
		 		DIO_u8SetPinValue(SSD_PIN_C,SSD_PIN__VALUE_HIGH);

		 		/*DIO_u8SetPion : set the pin which connect to the seven segment pin d */
		 		DIO_u8SetPinValue(SSD_PIN_D,SSD_PIN__VALUE_HIGH);

		 		/*DIO_u8SetPion : set the pin which connect to the seven segment pin e */
		 		DIO_u8SetPinValue(SSD_PIN_E,SSD_PIN__VALUE_LOW);

		 		/*DIO_u8SetPion : set the pin which connect to the seven segment pin f */
		 		DIO_u8SetPinValue(SSD_PIN_F,SSD_PIN__VALUE_HIGH);

		 		/*DIO_u8SetPion : set the pin which connect to the seven segment pin g */
		 		DIO_u8SetPinValue(SSD_PIN_G,SSD_PIN__VALUE_HIGH);


		 		break;


			 }
				break;
			case SSD_COMMON_ANODE  :

				/*DIO_u8SetPion : set the pin which connect to the seven segment pin EN */
						 		DIO_u8SetPinValue(SSD_PIN_EN,SSD_PIN__VALUE_HIGH);
							 switch(Copy_u8SsdValue)
							 {
				/**************************************NUMBER_ZERO*********************************************/
							 case  SSD_U8_NUMBER_ZERO:
						       /*DIO_u8SetPortDirection : set the pin which will connect to the seven segment pin a */
						 		DIO_u8SetPinValue(SSD_PIN_A,SSD_PIN__VALUE_LOW);

						 		/*DIO_u8SetPion : set the pin which connect to the seven segment pin b*/
						 		DIO_u8SetPinValue(SSD_PIN_B,SSD_PIN__VALUE_LOW);

						 		/*DIO_u8SetPion : set the pin which connect to the seven segment pin c */
						 		DIO_u8SetPinValue(SSD_PIN_C,SSD_PIN__VALUE_LOW);

						 		/*DIO_u8SetPion : set the pin which connect to the seven segment pin d */
						 		DIO_u8SetPinValue(SSD_PIN_D,SSD_PIN__VALUE_LOW);

						 		/*DIO_u8SetPion : set the pin which connect to the seven segment pin e */
						 		DIO_u8SetPinValue(SSD_PIN_E,SSD_PIN__VALUE_LOW);

						 		/*DIO_u8SetPion : set the pin which connect to the seven segment pin f */
						 		DIO_u8SetPinValue(SSD_PIN_F,SSD_PIN__VALUE_LOW);

						 		/*DIO_u8SetPion : set the pin which connect to the seven segment pin g */
						 		DIO_u8SetPinValue(SSD_PIN_G,SSD_PIN__VALUE_HIGH);
						 		break;
				/**************************************NUMBER_ONE*********************************************/

							 case  SSD_U8_NUMBER_ONE:
						       /*DIO_u8SetPortDirection : set the pin which will connect to the seven segment pin a */
						 		DIO_u8SetPinValue(SSD_PIN_A,SSD_PIN__VALUE_HIGH);

						 		/*DIO_u8SetPion : set the pin which connect to the seven segment pin b*/
						 		DIO_u8SetPinValue(SSD_PIN_B,SSD_PIN__VALUE_LOW);

						 		/*DIO_u8SetPion : set the pin which connect to the seven segment pin c */
						 		DIO_u8SetPinValue(SSD_PIN_C,SSD_PIN__VALUE_LOW);

						 		/*DIO_u8SetPion : set the pin which connect to the seven segment pin d */
						 		DIO_u8SetPinValue(SSD_PIN_D,SSD_PIN__VALUE_HIGH);

						 		/*DIO_u8SetPion : set the pin which connect to the seven segment pin e */
						 		DIO_u8SetPinValue(SSD_PIN_E,SSD_PIN__VALUE_HIGH);

						 		/*DIO_u8SetPion : set the pin which connect to the seven segment pin f */
						 		DIO_u8SetPinValue(SSD_PIN_F,SSD_PIN__VALUE_HIGH);

						 		/*DIO_u8SetPion : set the pin which connect to the seven segment pin g */
						 		DIO_u8SetPinValue(SSD_PIN_G,SSD_PIN__VALUE_HIGH);
						 		break;
				/**************************************NUMBER_TWO*********************************************/

							 case  SSD_U8_NUMBER_TWO:
						       /*DIO_u8SetPortDirection : set the pin which will connect to the seven segment pin a */
						 		DIO_u8SetPinValue(SSD_PIN_A,SSD_PIN__VALUE_LOW);

						 		/*DIO_u8SetPion : set the pin which connect to the seven segment pin b*/
						 		DIO_u8SetPinValue(SSD_PIN_B,SSD_PIN__VALUE_LOW);

						 		/*DIO_u8SetPion : set the pin which connect to the seven segment pin c */
						 		DIO_u8SetPinValue(SSD_PIN_C,SSD_PIN__VALUE_HIGH);

						 		/*DIO_u8SetPion : set the pin which connect to the seven segment pin d */
						 		DIO_u8SetPinValue(SSD_PIN_D,SSD_PIN__VALUE_LOW);

						 		/*DIO_u8SetPion : set the pin which connect to the seven segment pin e */
						 		DIO_u8SetPinValue(SSD_PIN_E,SSD_PIN__VALUE_LOW);

						 		/*DIO_u8SetPion : set the pin which connect to the seven segment pin f */
						 		DIO_u8SetPinValue(SSD_PIN_F,SSD_PIN__VALUE_HIGH);

						 		/*DIO_u8SetPion : set the pin which connect to the seven segment pin g */
						 		DIO_u8SetPinValue(SSD_PIN_G,SSD_PIN__VALUE_LOW);
						 		break;
				/**************************************NUMBER_THREE*********************************************/
							 case  SSD_U8_NUMBER_THREE:
						       /*DIO_u8SetPortDirection : set the pin which will connect to the seven segment pin a */
						 		DIO_u8SetPinValue(SSD_PIN_A,SSD_PIN__VALUE_LOW);

						 		/*DIO_u8SetPion : set the pin which connect to the seven segment pin b*/
						 		DIO_u8SetPinValue(SSD_PIN_B,SSD_PIN__VALUE_LOW);

						 		/*DIO_u8SetPion : set the pin which connect to the seven segment pin c */
						 		DIO_u8SetPinValue(SSD_PIN_C,SSD_PIN__VALUE_LOW);

						 		/*DIO_u8SetPion : set the pin which connect to the seven segment pin d */
						 		DIO_u8SetPinValue(SSD_PIN_D,SSD_PIN__VALUE_LOW);

						 		/*DIO_u8SetPion : set the pin which connect to the seven segment pin e */
						 		DIO_u8SetPinValue(SSD_PIN_E,SSD_PIN__VALUE_HIGH);

						 		/*DIO_u8SetPion : set the pin which connect to the seven segment pin f */
						 		DIO_u8SetPinValue(SSD_PIN_F,SSD_PIN__VALUE_HIGH);

						 		/*DIO_u8SetPion : set the pin which connect to the seven segment pin g */
						 		DIO_u8SetPinValue(SSD_PIN_G,SSD_PIN__VALUE_LOW);
						 		break;
				/**************************************NUMBER_FOUR*********************************************/

							 case  SSD_U8_NUMBER_FOUR:
						       /*DIO_u8SetPortDirection : set the pin which will connect to the seven segment pin a */
						 		DIO_u8SetPinValue(SSD_PIN_A,SSD_PIN__VALUE_HIGH);

						 		/*DIO_u8SetPion : set the pin which connect to the seven segment pin b*/
						 		DIO_u8SetPinValue(SSD_PIN_B,SSD_PIN__VALUE_LOW);

						 		/*DIO_u8SetPion : set the pin which connect to the seven segment pin c */
						 		DIO_u8SetPinValue(SSD_PIN_C,SSD_PIN__VALUE_LOW);

						 		/*DIO_u8SetPion : set the pin which connect to the seven segment pin d */
						 		DIO_u8SetPinValue(SSD_PIN_D,SSD_PIN__VALUE_HIGH);

						 		/*DIO_u8SetPion : set the pin which connect to the seven segment pin e */
						 		DIO_u8SetPinValue(SSD_PIN_E,SSD_PIN__VALUE_HIGH);

						 		/*DIO_u8SetPion : set the pin which connect to the seven segment pin f */
						 		DIO_u8SetPinValue(SSD_PIN_F,SSD_PIN__VALUE_LOW);

						 		/*DIO_u8SetPion : set the pin which connect to the seven segment pin g */
						 		DIO_u8SetPinValue(SSD_PIN_G,SSD_PIN__VALUE_LOW);
						 		break;
				/**************************************NUMBER_FIVE*********************************************/

							 case  SSD_U8_NUMBER_FIVE:
						       /*DIO_u8SetPortDirection : set the pin which will connect to the seven segment pin a */
						 		DIO_u8SetPinValue(SSD_PIN_A,SSD_PIN__VALUE_LOW);

						 		/*DIO_u8SetPion : set the pin which connect to the seven segment pin b*/
						 		DIO_u8SetPinValue(SSD_PIN_B,SSD_PIN__VALUE_HIGH);

						 		/*DIO_u8SetPion : set the pin which connect to the seven segment pin c */
						 		DIO_u8SetPinValue(SSD_PIN_C,SSD_PIN__VALUE_LOW);

						 		/*DIO_u8SetPion : set the pin which connect to the seven segment pin d */
						 		DIO_u8SetPinValue(SSD_PIN_D,SSD_PIN__VALUE_LOW);

						 		/*DIO_u8SetPion : set the pin which connect to the seven segment pin e */
						 		DIO_u8SetPinValue(SSD_PIN_E,SSD_PIN__VALUE_HIGH);

						 		/*DIO_u8SetPion : set the pin which connect to the seven segment pin f */
						 		DIO_u8SetPinValue(SSD_PIN_F,SSD_PIN__VALUE_LOW);

						 		/*DIO_u8SetPion : set the pin which connect to the seven segment pin g */
						 		DIO_u8SetPinValue(SSD_PIN_G,SSD_PIN__VALUE_LOW);
						 		break;
				/**************************************NUMBER_SIX*********************************************/

							 case  SSD_U8_NUMBER_SIX:
						       /*DIO_u8SetPortDirection : set the pin which will connect to the seven segment pin a */
						 		DIO_u8SetPinValue(SSD_PIN_A,SSD_PIN__VALUE_LOW);

						 		/*DIO_u8SetPion : set the pin which connect to the seven segment pin b*/
						 		DIO_u8SetPinValue(SSD_PIN_B,SSD_PIN__VALUE_HIGH);

						 		/*DIO_u8SetPion : set the pin which connect to the seven segment pin c */
						 		DIO_u8SetPinValue(SSD_PIN_C,SSD_PIN__VALUE_LOW);

						 		/*DIO_u8SetPion : set the pin which connect to the seven segment pin d */
						 		DIO_u8SetPinValue(SSD_PIN_D,SSD_PIN__VALUE_LOW);

						 		/*DIO_u8SetPion : set the pin which connect to the seven segment pin e */
						 		DIO_u8SetPinValue(SSD_PIN_E,SSD_PIN__VALUE_LOW);

						 		/*DIO_u8SetPion : set the pin which connect to the seven segment pin f */
						 		DIO_u8SetPinValue(SSD_PIN_F,SSD_PIN__VALUE_LOW);

						 		/*DIO_u8SetPion : set the pin which connect to the seven segment pin g */
						 		DIO_u8SetPinValue(SSD_PIN_G,SSD_PIN__VALUE_LOW);
						 		break;
				/**************************************NUMBER_SEVEN*********************************************/

							 case  SSD_U8_NUMBER_SEVEN:
						       /*DIO_u8SetPortDirection : set the pin which will connect to the seven segment pin a */
						 		DIO_u8SetPinValue(SSD_PIN_A,SSD_PIN__VALUE_LOW);

						 		/*DIO_u8SetPion : set the pin which connect to the seven segment pin b*/
						 		DIO_u8SetPinValue(SSD_PIN_B,SSD_PIN__VALUE_LOW);

						 		/*DIO_u8SetPion : set the pin which connect to the seven segment pin c */
						 		DIO_u8SetPinValue(SSD_PIN_C,SSD_PIN__VALUE_LOW);

						 		/*DIO_u8SetPion : set the pin which connect to the seven segment pin d */
						 		DIO_u8SetPinValue(SSD_PIN_D,SSD_PIN__VALUE_HIGH);

						 		/*DIO_u8SetPion : set the pin which connect to the seven segment pin e */
						 		DIO_u8SetPinValue(SSD_PIN_E,SSD_PIN__VALUE_HIGH);

						 		/*DIO_u8SetPion : set the pin which connect to the seven segment pin f */
						 		DIO_u8SetPinValue(SSD_PIN_F,SSD_PIN__VALUE_HIGH);

						 		/*DIO_u8SetPion : set the pin which connect to the seven segment pin g */
						 		DIO_u8SetPinValue(SSD_PIN_G,SSD_PIN__VALUE_HIGH);
						 		break;
				/**************************************NUMBER_EIGHT*********************************************/
							 case  SSD_U8_NUMBER_EIGHT:
						       /*DIO_u8SetPortDirection : set the pin which will connect to the seven segment pin a */
						 		DIO_u8SetPinValue(SSD_PIN_A,SSD_PIN__VALUE_LOW);

						 		/*DIO_u8SetPion : set the pin which connect to the seven segment pin b*/
						 		DIO_u8SetPinValue(SSD_PIN_B,SSD_PIN__VALUE_LOW);

						 		/*DIO_u8SetPion : set the pin which connect to the seven segment pin c */
						 		DIO_u8SetPinValue(SSD_PIN_C,SSD_PIN__VALUE_LOW);

						 		/*DIO_u8SetPion : set the pin which connect to the seven segment pin d */
						 		DIO_u8SetPinValue(SSD_PIN_D,SSD_PIN__VALUE_LOW);

						 		/*DIO_u8SetPion : set the pin which connect to the seven segment pin e */
						 		DIO_u8SetPinValue(SSD_PIN_E,SSD_PIN__VALUE_LOW);

						 		/*DIO_u8SetPion : set the pin which connect to the seven segment pin f */
						 		DIO_u8SetPinValue(SSD_PIN_F,SSD_PIN__VALUE_LOW);

						 		/*DIO_u8SetPion : set the pin which connect to the seven segment pin g */
						 		DIO_u8SetPinValue(SSD_PIN_G,SSD_PIN__VALUE_LOW);
						 		break;
				/**************************************NUMBER_NINE*********************************************/
							 case  SSD_U8_NUMBER_NINE:
						       /*DIO_u8SetPortDirection : set the pin which will connect to the seven segment pin a */
						 		DIO_u8SetPinValue(SSD_PIN_A,SSD_PIN__VALUE_LOW);

						 		/*DIO_u8SetPion : set the pin which connect to the seven segment pin b*/
						 		DIO_u8SetPinValue(SSD_PIN_B,SSD_PIN__VALUE_LOW);

						 		/*DIO_u8SetPion : set the pin which connect to the seven segment pin c */
						 		DIO_u8SetPinValue(SSD_PIN_C,SSD_PIN__VALUE_LOW);

						 		/*DIO_u8SetPion : set the pin which connect to the seven segment pin d */
						 		DIO_u8SetPinValue(SSD_PIN_D,SSD_PIN__VALUE_LOW);

						 		/*DIO_u8SetPion : set the pin which connect to the seven segment pin e */
						 		DIO_u8SetPinValue(SSD_PIN_E,SSD_PIN__VALUE_HIGH);

						 		/*DIO_u8SetPion : set the pin which connect to the seven segment pin f */
						 		DIO_u8SetPinValue(SSD_PIN_F,SSD_PIN__VALUE_LOW);

						 		/*DIO_u8SetPion : set the pin which connect to the seven segment pin g */
						 		DIO_u8SetPinValue(SSD_PIN_G,SSD_PIN__VALUE_LOW);
						 		break;
							 }

				break;
		}

		/*return state of error*/
		return Local_u8ErrorChecker;

}
/****************************************************************/

/****************************************************************/

/*Description: set seven segment ON
 * Inputs : Void
 *
 * Outputs: void
 *
 * */
u8 SSD_u8SetOn(void)
{
	/*local variables */
			u8 Local_u8ErrorChecker = STD_u8_OK;

	switch(SSD_TYPE)
	{
	   case SSD_COMMON_CATHODE :

			/*DIO_u8SetPion : set the pin which connect to the seven segment pin EN */
		   Local_u8ErrorChecker = DIO_u8SetPinValue(SSD_PIN_EN,SSD_PIN__VALUE_LOW);
	 	break;
	   case SSD_COMMON_ANODE :

	  			/*DIO_u8SetPion : set the pin which connect to the seven segment pin EN */
		   Local_u8ErrorChecker  =	DIO_u8SetPinValue(SSD_PIN_EN,SSD_PIN__VALUE_HIGH);
	  	 	break;
	}
	return Local_u8ErrorChecker;
}
/****************************************************************/

/****************************************************************/

/*Description: set seven segment Off
 * Inputs : u8  void
 *
 * Outputs: void
 *
 * */


u8 SSD_u8SetOff(void)
{
	/*local variables */
	u8 Local_u8ErrorChecker = STD_u8_OK;

	switch(SSD_TYPE)
		{
		   case SSD_COMMON_CATHODE :

				/*DIO_u8SetPion : set the pin which connect to the seven segment pin EN */
			   Local_u8ErrorChecker  =	DIO_u8SetPinValue(SSD_PIN_EN,SSD_PIN__VALUE_HIGH);
		 	break;
		   case SSD_COMMON_ANODE :

		  			/*DIO_u8SetPion : set the pin which connect to the seven segment pin EN */
			   Local_u8ErrorChecker = DIO_u8SetPinValue(SSD_PIN_EN,SSD_PIN__VALUE_LOW);
		  	 	break;
		}
	return Local_u8ErrorChecker;
}

