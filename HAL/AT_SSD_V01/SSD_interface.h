
/***********************************SSD_interface.h********************************/
/* Author  : Ahmed Tarek                                                          */
/* Date    : 8 feb 2019                                                           */
/* Version : v01                                                                  */
/**********************************************************************************/
/*Description										          	        		  */
/*-----------										                 			  */
/*DIO_interface.h is a file from SSD Deriver include the header of function which */
/*use to initiate all SSD pinsset SSD value , set SSD on and set SSD off          */
/**********************************************************************************/
#ifndef SSD_INTERFACE_H_
#define SSD_INTERFACE_H_


/****************************************************************/

/*Description: initiate the seven segment direction
 * Inputs : void
 *
 * Outputs: void
 *
 * */

void SSD_VoidInitiatlization(void);


/****************************************************************/

/*Description: set seven segment value
 * Inputs : u8  Copy_u8SsdValue  : pin Number
                                  Range SSD_ZERO TO SSD_NINE
 *
 * Outputs: Error state
 *
 * */

u8 SSD_u8SetValue(u8 Copy_u8SsdValue);
/****************************************************************/

/****************************************************************/

/*Description: set seven segment ON
 * Inputs : Void
 *
 * Outputs: Error state
 *
 * */
u8 SSD_u8SetOn(void);
/****************************************************************/

/****************************************************************/

/*Description: set seven segment Off
 * Inputs : u8  void
 *
 * Outputs: Error state
 *
 * */


u8 SSD_u8SetOff(void);


#endif /* SSD_INTERFACE_H_ */
