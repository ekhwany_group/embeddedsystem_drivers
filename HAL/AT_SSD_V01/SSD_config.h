/*
 * SSD_config.h
 *
 *  Created on: Feb 8, 2019
 *      Author: Ahmed Tarek
 */

#ifndef SSD_CONFIG_H_
#define SSD_CONFIG_H_


#define SSD_TYPE		SSD_COMMON_CATHODE


/*configure the pinA of the seven segment    */
/*Range : DIO_PIN0 TO DIO_PIN31*/
#define SSD_PIN_A  	    	  	DIO_PIN0
/*configure the pinB of the seven segment    */
/*Range : DIO_PIN0 TO DIO_PIN31*/
#define SSD_PIN_B     		  	DIO_PIN1
/*configure the pinC of the seven segment    */
/*Range : DIO_PIN0 TO DIO_PIN31*/
#define SSD_PIN_C    	 	  	DIO_PIN2
/*configure the pinD of the seven segment    */
/*Range : DIO_PIN0 TO DIO_PIN31*/
#define SSD_PIN_D     		  	DIO_PIN3
/*configure the pinE of the seven segment    */
/*Range : DIO_PIN0 TO DIO_PIN31*/
#define SSD_PIN_E     	  		DIO_PIN4
/*configure the pinF of the seven segment 	 */
/*Range : DIO_PIN0 TO DIO_PIN31*/
#define SSD_PIN_F     		    DIO_PIN5
/*configure the pinG of the seven segment    */
/*Range : DIO_PIN0 TO DIO_PIN31*/
#define SSD_PIN_G     	  		DIO_PIN6
/*configure the Enable pin of the seven segment */
/*Range : DIO_PIN0 TO DIO_PIN31*/
#define SSD_PIN_EN	      		DIO_PIN25

#endif /* SSD_CONFIG_H_ */
