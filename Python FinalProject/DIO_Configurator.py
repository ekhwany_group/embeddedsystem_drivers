 # -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'FinalProject.ui'
#
# Created: Sun Mar 17 00:28:08 2019
#      by: pyside-uic 0.2.15 running on PySide 1.2.4
#
# WARNING! All changes made in this file will be lost!

from PySide import QtCore, QtGui

class Ui_MainWindow(object):
    def setupUi(self, PIN_Configuration_Tool):
        PIN_Configuration_Tool.setObjectName("PIN_Configuration_Tool")
        PIN_Configuration_Tool.resize(620, 360)
        self.centralwidget = QtGui.QWidget(PIN_Configuration_Tool)
        self.centralwidget.setObjectName("centralwidget")
        self.PinConfiguration = QtGui.QGroupBox(self.centralwidget)
        self.PinConfiguration.setGeometry(QtCore.QRect(5, 10, 691, 341))
        self.PinConfiguration.setObjectName("PinConfiguration")
        self.InputLevel = QtGui.QGroupBox(self.PinConfiguration)
        self.InputLevel.setGeometry(QtCore.QRect(420, 40, 181, 181))
        self.InputLevel.setObjectName("InputLevel")
        self.Analoge = QtGui.QRadioButton(self.InputLevel)
        self.Analoge.setGeometry(QtCore.QRect(10, 30, 82, 17))
        self.Analoge.setObjectName("Analoge")
        self.Floating = QtGui.QRadioButton(self.InputLevel)
        self.Floating.setGeometry(QtCore.QRect(10, 60, 82, 17))
        self.Floating.setObjectName("Floating")
        self.PullUp = QtGui.QRadioButton(self.InputLevel)
        self.PullUp.setGeometry(QtCore.QRect(10, 100, 82, 17))
        self.PullUp.setObjectName("PullUp")
        self.PullDown = QtGui.QRadioButton(self.InputLevel)
        self.PullDown.setGeometry(QtCore.QRect(10, 130, 82, 17))
        self.PullDown.setObjectName("PullDown")
        self.ModeBox = QtGui.QGroupBox(self.PinConfiguration)
        self.ModeBox.setGeometry(QtCore.QRect(30, 40, 181, 181))
        self.ModeBox.setObjectName("ModeBox")
        self.Output = QtGui.QRadioButton(self.ModeBox)
        self.Output.setGeometry(QtCore.QRect(10, 40, 82, 17))
        self.Output.setObjectName("Output")
        self.Input = QtGui.QRadioButton(self.ModeBox)
        self.Input.setGeometry(QtCore.QRect(10, 80, 82, 17))
        self.Input.setObjectName("Input")
        self.OutputLevel = QtGui.QGroupBox(self.PinConfiguration)
        self.OutputLevel.setGeometry(QtCore.QRect(220, 40, 191, 181))
        self.OutputLevel.setObjectName("OutputLevel")
        self.PushPull = QtGui.QRadioButton(self.OutputLevel)
        self.PushPull.setGeometry(QtCore.QRect(20, 30, 82, 17))
        self.PushPull.setObjectName("PushPull")
        self.OpenDrain = QtGui.QRadioButton(self.OutputLevel)
        self.OpenDrain.setGeometry(QtCore.QRect(20, 70, 82, 17))
        self.OpenDrain.setObjectName("OpenDrain")
        self.AFOpenDrain = QtGui.QRadioButton(self.OutputLevel)
        self.AFOpenDrain.setGeometry(QtCore.QRect(20, 150, 101, 17))
        self.AFOpenDrain.setObjectName("AFOpenDrain")
        self.AFPushPull = QtGui.QRadioButton(self.OutputLevel)
        self.AFPushPull.setGeometry(QtCore.QRect(20, 110, 82, 17))
        self.AFPushPull.setObjectName("AFPushPull")
        self.New = QtGui.QPushButton(self.PinConfiguration)
        self.New.setGeometry(QtCore.QRect(30, 10, 75, 23))
        self.New.setObjectName("New")
        self.Save = QtGui.QPushButton(self.PinConfiguration)
        self.Save.setGeometry(QtCore.QRect(110, 10, 75, 23))
        self.Save.setObjectName("Save")
        self.Load = QtGui.QPushButton(self.PinConfiguration)
        self.Load.setGeometry(QtCore.QRect(190, 10, 75, 23))
        self.Load.setObjectName("Load")
        self.Generate = QtGui.QPushButton(self.PinConfiguration)
        self.Generate.setGeometry(QtCore.QRect(420, 260, 181, 71))
        self.Generate.setObjectName("Generate")
        self.FilePath = QtGui.QLineEdit(self.PinConfiguration)
        self.FilePath.setGeometry(QtCore.QRect(30, 290, 361, 20))
        self.FilePath.setObjectName("FilePath")
        self.label_2 = QtGui.QLabel(self.PinConfiguration)
        self.label_2.setGeometry(QtCore.QRect(30, 270, 46, 13))
        self.label_2.setObjectName("label_2")
        self.PinNum = QtGui.QComboBox(self.PinConfiguration)
        self.PinNum.setGeometry(QtCore.QRect(30, 230, 69, 22))
        self.PinNum.setObjectName("PinNum")
        self.PinNum.addItem("")
        self.PinNum.addItem("")
        self.PinNum.addItem("")
        self.PinNum.addItem("")
        self.PinNum.addItem("")
        self.PinNum.addItem("")
        self.PinNum.addItem("")
        self.PinNum.addItem("")
        self.PinNum.addItem("")
        self.PinNum.addItem("")
        self.PinNum.addItem("")
        self.PinNum.addItem("")
        self.PinNum.addItem("")
        self.PinNum.addItem("")
        self.PinNum.addItem("")
        self.PinNum.addItem("")
        self.PinNum.addItem("")
        self.PinNum.addItem("")
        self.PinNum.addItem("")
        self.PinNum.addItem("")
        self.PinNum.addItem("")
        self.PinNum.addItem("")
        PIN_Configuration_Tool.setCentralWidget(self.centralwidget)
        self.menubar = QtGui.QMenuBar(PIN_Configuration_Tool)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 800, 21))
        self.menubar.setObjectName("menubar")
        PIN_Configuration_Tool.setMenuBar(self.menubar)
        self.statusbar = QtGui.QStatusBar(PIN_Configuration_Tool)
        self.statusbar.setObjectName("statusbar")
        PIN_Configuration_Tool.setStatusBar(self.statusbar)

        self.retranslateUi(PIN_Configuration_Tool)
        QtCore.QObject.connect(self.New, QtCore.SIGNAL("clicked(bool)"), self.ModeBox.setDisabled)
        QtCore.QObject.connect(self.New, QtCore.SIGNAL("clicked(bool)"), self.OutputLevel.setDisabled)
        QtCore.QObject.connect(self.New, QtCore.SIGNAL("clicked(bool)"), self.InputLevel.setDisabled)
        QtCore.QObject.connect(self.Input, QtCore.SIGNAL("clicked(bool)"), self.OutputLevel.setDisabled)
        QtCore.QObject.connect(self.Output, QtCore.SIGNAL("clicked(bool)"), self.InputLevel.setDisabled)
        QtCore.QObject.connect(self.Output, QtCore.SIGNAL("clicked(bool)"), self.OutputLevel.setEnabled)
        QtCore.QObject.connect(self.Input, QtCore.SIGNAL("clicked(bool)"), self.InputLevel.setEnabled)
        QtCore.QObject.connect(self.Generate, QtCore.SIGNAL("clicked()"), self.GenerateFunc)
        QtCore.QObject.connect(self.New, QtCore.SIGNAL("clicked()"), self.NewFunc)
        QtCore.QObject.connect(self.New, QtCore.SIGNAL("clicked()"), self.ModeBox.clicked)
        QtCore.QObject.connect(self.Load, QtCore.SIGNAL("clicked()"), self.LoadFunc)
        #QtCore.QObject.connect(self.Generate, QtCore.SIGNAL("clicked()"), self.SaveFunc)
        QtCore.QMetaObject.connectSlotsByName(PIN_Configuration_Tool)
        PinsList= []
    def GenerateFunc(self) :
        DIO_INIT_FILE=self.FilePath.text() + r'\DIO_Init.h'
        DIO_H_HANDLER=open(DIO_INIT_FILE,'a')
        if self.Output.isChecked():
          if self.PushPull.isChecked():
            PIN_INIT_MODE='		DIO_u8_OUTPUT_PUSH_PULL'
          elif self.OpenDrain.isChecked():
            PIN_INIT_MODE='		DIO_u8_OUTPUT_OPEN_DRAIN'
          elif self.AFPushPull.isChecked():
            PIN_INIT_MODE='		DIO_u8_OUTPUT_AF_PUSH_PULL'
          elif self.AFOpenDrain.isChecked():
              PIN_INIT_MODE='		DIO_u8_OUTPUT_AF_OPEN_DRAIN'
        elif self.Input.isChecked():
          if self.Analoge.isChecked():
              PIN_INIT_MODE='		DIO_u8_INPUT_ANALOGE'
          elif self.PullUp.isChecked():
              PIN_INIT_MODE='		DIO_u8_INPUT_PULLUP'
          elif self.PullDown.isChecked():
              PIN_INIT_MODE='		DIO_u8_INPUT_PULL_DOWN'
          elif self.Floating.isChecked():
              PIN_INIT_MODE='		DIO_u8_INPUT_Floating'							
          
        DIO_H_HANDLER.write('\n#define  ' + self.PinNum.currentText() + ' 	' + PIN_INIT_MODE)
        PinsList.append(self.PinNum.currentText())
        self.PinNum.removeItem(self.PinNum.currentIndex())
        DIO_H_HANDLER.close()

    def NewFunc(self) :
        DIO_INIT_FILE=self.FilePath.text() + r'\DIO_Init.h'
        DIO_H_HANDLER=open(DIO_INIT_FILE,'w')
        DIO_H_HANDLER.write('New File Mode')
				#for i in range(len(PinsList)):
        self.PinNum.addItems(PinsList)
        DIO_H_HANDLER.close()
				
    def LoadFunc(self) :
        DIO_INIT_FILE=self.FilePath.text() + r'\DIO_Init.h'
        DIO_H_HANDLER=open(DIO_INIT_FILE,'a')
        DIO_H_HANDLER.write('Opened In The Load Mode')				
        DIO_DIO_H_HANDLER.close()
				
    #def SaveFunc(self) :
				
        
    def retranslateUi(self, PIN_Configuration_Tool) :
        PIN_Configuration_Tool.setWindowTitle(QtGui.QApplication.translate("PIN_Configuration_Tool", "PIN_Configuration_Tool", None, QtGui.QApplication.UnicodeUTF8))
        self.PinConfiguration.setTitle(QtGui.QApplication.translate("PIN_Configuration_Tool", "PIN", None, QtGui.QApplication.UnicodeUTF8))
        self.InputLevel.setTitle(QtGui.QApplication.translate("PIN_Configuration_Tool", "Input Level", None, QtGui.QApplication.UnicodeUTF8))
        self.Analoge.setText(QtGui.QApplication.translate("PIN_Configuration_Tool", "Analoge", None, QtGui.QApplication.UnicodeUTF8))
        self.Floating.setText(QtGui.QApplication.translate("PIN_Configuration_Tool", "Floating ", None, QtGui.QApplication.UnicodeUTF8))
        self.PullUp.setText(QtGui.QApplication.translate("PIN_Configuration_Tool", "Pull Up", None, QtGui.QApplication.UnicodeUTF8))
        self.PullDown.setText(QtGui.QApplication.translate("PIN_Configuration_Tool", "Pull Down", None, QtGui.QApplication.UnicodeUTF8))
        self.ModeBox.setTitle(QtGui.QApplication.translate("PIN_Configuration_Tool", "ModeBox", None, QtGui.QApplication.UnicodeUTF8))
        self.Output.setText(QtGui.QApplication.translate("PIN_Configuration_Tool", "Output", None, QtGui.QApplication.UnicodeUTF8))
        self.Input.setText(QtGui.QApplication.translate("PIN_Configuration_Tool", "Input", None, QtGui.QApplication.UnicodeUTF8))
        self.OutputLevel.setTitle(QtGui.QApplication.translate("PIN_Configuration_Tool", "Output Level", None, QtGui.QApplication.UnicodeUTF8))
        self.PushPull.setText(QtGui.QApplication.translate("PIN_Configuration_Tool", "Push Pull", None, QtGui.QApplication.UnicodeUTF8))
        self.OpenDrain.setText(QtGui.QApplication.translate("PIN_Configuration_Tool", "Open Drain", None, QtGui.QApplication.UnicodeUTF8))
        self.AFOpenDrain.setText(QtGui.QApplication.translate("PIN_Configuration_Tool", "AF Open Drain", None, QtGui.QApplication.UnicodeUTF8))
        self.AFPushPull.setText(QtGui.QApplication.translate("PIN_Configuration_Tool", "AF Push Pull", None, QtGui.QApplication.UnicodeUTF8))
        self.New.setText(QtGui.QApplication.translate("PIN_Configuration_Tool", "New", None, QtGui.QApplication.UnicodeUTF8))
        self.Save.setText(QtGui.QApplication.translate("PIN_Configuration_Tool", "Save", None, QtGui.QApplication.UnicodeUTF8))
        self.Load.setText(QtGui.QApplication.translate("PIN_Configuration_Tool", "Load", None, QtGui.QApplication.UnicodeUTF8))
        self.Generate.setText(QtGui.QApplication.translate("PIN_Configuration_Tool", "Generate", None, QtGui.QApplication.UnicodeUTF8))
        self.label_2.setText(QtGui.QApplication.translate("PIN_Configuration_Tool", "Fle Path", None, QtGui.QApplication.UnicodeUTF8))
        self.PinNum.setItemText(0, QtGui.QApplication.translate("PIN_Configuration_Tool", "DIO_PIN0 ", None, QtGui.QApplication.UnicodeUTF8))
        self.PinNum.setItemText(1, QtGui.QApplication.translate("PIN_Configuration_Tool", "DIO_PIN1", None, QtGui.QApplication.UnicodeUTF8))
        self.PinNum.setItemText(2, QtGui.QApplication.translate("PIN_Configuration_Tool", "DIO_PIN2", None, QtGui.QApplication.UnicodeUTF8))
        self.PinNum.setItemText(3, QtGui.QApplication.translate("PIN_Configuration_Tool", "DIO_PIN3", None, QtGui.QApplication.UnicodeUTF8))
        self.PinNum.setItemText(4, QtGui.QApplication.translate("PIN_Configuration_Tool", "DIO_PIN4", None, QtGui.QApplication.UnicodeUTF8))
        self.PinNum.setItemText(5, QtGui.QApplication.translate("PIN_Configuration_Tool", "DIO_PIN5", None, QtGui.QApplication.UnicodeUTF8))
        self.PinNum.setItemText(6, QtGui.QApplication.translate("PIN_Configuration_Tool", "DIO_PIN6", None, QtGui.QApplication.UnicodeUTF8))
        self.PinNum.setItemText(7, QtGui.QApplication.translate("PIN_Configuration_Tool", "DIO_PIN7", None, QtGui.QApplication.UnicodeUTF8))
        self.PinNum.setItemText(8, QtGui.QApplication.translate("PIN_Configuration_Tool", "DIO_PIN8", None, QtGui.QApplication.UnicodeUTF8))
        self.PinNum.setItemText(9, QtGui.QApplication.translate("PIN_Configuration_Tool", "DIO_PIN9", None, QtGui.QApplication.UnicodeUTF8))
        self.PinNum.setItemText(10, QtGui.QApplication.translate("PIN_Configuration_Tool", "DIO_PIN10", None, QtGui.QApplication.UnicodeUTF8))
        self.PinNum.setItemText(11, QtGui.QApplication.translate("PIN_Configuration_Tool", "DIO_PIN11", None, QtGui.QApplication.UnicodeUTF8))
        self.PinNum.setItemText(12, QtGui.QApplication.translate("PIN_Configuration_Tool", "DIO_PIN12", None, QtGui.QApplication.UnicodeUTF8))
        self.PinNum.setItemText(13, QtGui.QApplication.translate("PIN_Configuration_Tool", "DIO_PIN13", None, QtGui.QApplication.UnicodeUTF8))
        self.PinNum.setItemText(14, QtGui.QApplication.translate("PIN_Configuration_Tool", "DIO_PIN14", None, QtGui.QApplication.UnicodeUTF8))
        self.PinNum.setItemText(15, QtGui.QApplication.translate("PIN_Configuration_Tool", "DIO_PIN15", None, QtGui.QApplication.UnicodeUTF8))
        self.PinNum.setItemText(16, QtGui.QApplication.translate("PIN_Configuration_Tool", "DIO_PIN16", None, QtGui.QApplication.UnicodeUTF8))
        self.PinNum.setItemText(17, QtGui.QApplication.translate("PIN_Configuration_Tool", "DIO_PIN17", None, QtGui.QApplication.UnicodeUTF8))
        self.PinNum.setItemText(18, QtGui.QApplication.translate("PIN_Configuration_Tool", "DIO_PIN18", None, QtGui.QApplication.UnicodeUTF8))
        self.PinNum.setItemText(19, QtGui.QApplication.translate("PIN_Configuration_Tool", "DIO_PIN19", None, QtGui.QApplication.UnicodeUTF8))
        self.PinNum.setItemText(20, QtGui.QApplication.translate("PIN_Configuration_Tool", "DIO_PIN20", None, QtGui.QApplication.UnicodeUTF8))
        self.PinNum.setItemText(21, QtGui.QApplication.translate("PIN_Configuration_Tool", "DIO_PIN21", None, QtGui.QApplication.UnicodeUTF8))
        self.PinNum.setItemText(22, QtGui.QApplication.translate("PIN_Configuration_Tool", "DIO_PIN22", None, QtGui.QApplication.UnicodeUTF8))
        self.PinNum.setItemText(23, QtGui.QApplication.translate("PIN_Configuration_Tool", "DIO_PIN23", None, QtGui.QApplication.UnicodeUTF8))
        self.PinNum.setItemText(24, QtGui.QApplication.translate("PIN_Configuration_Tool", "DIO_PIN24", None, QtGui.QApplication.UnicodeUTF8))
        self.PinNum.setItemText(25, QtGui.QApplication.translate("PIN_Configuration_Tool", "DIO_PIN25", None, QtGui.QApplication.UnicodeUTF8))
        self.PinNum.setItemText(26, QtGui.QApplication.translate("PIN_Configuration_Tool", "DIO_PIN26", None, QtGui.QApplication.UnicodeUTF8))
        self.PinNum.setItemText(27, QtGui.QApplication.translate("PIN_Configuration_Tool", "DIO_PIN27", None, QtGui.QApplication.UnicodeUTF8))
        self.PinNum.setItemText(28, QtGui.QApplication.translate("PIN_Configuration_Tool", "DIO_PIN28", None, QtGui.QApplication.UnicodeUTF8))
        self.PinNum.setItemText(29, QtGui.QApplication.translate("PIN_Configuration_Tool", "DIO_PIN29", None, QtGui.QApplication.UnicodeUTF8))
        self.PinNum.setItemText(30, QtGui.QApplication.translate("PIN_Configuration_Tool", "DIO_PIN30", None, QtGui.QApplication.UnicodeUTF8))
        self.PinNum.setItemText(31, QtGui.QApplication.translate("PIN_Configuration_Tool", "DIO_PIN31", None, QtGui.QApplication.UnicodeUTF8))
        self.PinNum.setItemText(32, QtGui.QApplication.translate("PIN_Configuration_Tool", "DIO_PIN32", None, QtGui.QApplication.UnicodeUTF8))
        self.PinNum.setItemText(33, QtGui.QApplication.translate("PIN_Configuration_Tool", "DIO_PIN33", None, QtGui.QApplication.UnicodeUTF8))
        self.PinNum.setItemText(34, QtGui.QApplication.translate("PIN_Configuration_Tool", "DIO_PIN34", None, QtGui.QApplication.UnicodeUTF8))
        self.PinNum.setItemText(35, QtGui.QApplication.translate("PIN_Configuration_Tool", "DIO_PIN35", None, QtGui.QApplication.UnicodeUTF8))
        self.PinNum.setItemText(36, QtGui.QApplication.translate("PIN_Configuration_Tool", "DIO_PIN36", None, QtGui.QApplication.UnicodeUTF8))
        self.PinNum.setItemText(37, QtGui.QApplication.translate("PIN_Configuration_Tool", "DIO_PIN37", None, QtGui.QApplication.UnicodeUTF8))


if __name__ == "__main__":
  import sys
  app = QtGui.QApplication(sys.argv)
  PIN_Configuration_Tool = QtGui.QMainWindow()
  ui = Ui_MainWindow()
  ui.setupUi(PIN_Configuration_Tool)
  PIN_Configuration_Tool.show()
  sys.exit(app.exec_())

